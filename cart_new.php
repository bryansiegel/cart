
<?php
//sessions and add to db
include('includes/cart_new_top.php');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <?php include('includes/header.php'); ?>

    <?php include('includes/show-image.php'); ?>
</head>

<body>

<div class="header">
    <div class="header-top-link">
        <div class="header-top-link-container">

        </div>
    </div>
    <div class="header-top-container">
        <div class="header-top">
            <div class="centerstuff">
            <img src="http://www.cibariasoapsupply.com/shop/skin/frontend/default/cibaria-supply/images/logo.png" alt="Wholesale Olive Oil Supplies">
        </div>
            </div>

    <div class="header-nav-container">
        <div id="menu1" style="margin: 5px auto;">

            <ul class="menu-top">
                <li><a href="/" class="menu-button"><span class="menu-label">Home</span></a></li>
                <li><a href="#instructions" class="menu-button"><span class="menu-label">Instructions</span></a></li>
                <li><a href="#bottles" class="menu-button"><span class="menu-label">Bottles</span></a></li>
            </ul>

        </div>

    </div>
</div>
    </div>
<!--            end header-->


<div class="products">

    <div class="container" style="width: 1100px;">
        <h1>Pre Packed Bottle Order Form</h1>
        <p><strong><span style="color:red">Before you begin</span></strong> please read through the instructions by clicking on the instructions link above. The Pre Packed order form is only a form. It is not connected to your Cibaria Store Supply account or shopping cart. It will not calculate shipping for you either.  The purpose of this form is to make it easier for you to order Pre Packed Bottle goods vs our old system. We will be updating this form as time permits. </p>
    <h1>Lead Times</h1>
 <p>Once your order is received by our customer service department lead times are anywhere from 10-15 days. You will receive a shipping quote from our customer service department</p>
<h1>Bottle Images</h1>
     <p>To see the images of bottles please click on the bottles link above. </p>
<!--        bottles-->
        <br />

                <div class="row">
<!--                    right-->

                    <div class="col-md-7" style="float:right;">
                        <?php include('cart_get.php'); ?>
                    </div>
                    <!--new-->

                    <form action="" method="post">
                    <!--        left-->
                    <div class="col-md-5">
                        <div class="panel panel-primary">
                            <div class="panel-heading">



                                        <?php if ($order->getId() != "") { ?>
                                            <input type="hidden" name="id" value="<?php echo $order->getId(); ?>"/>
                                        <?php } ?>

                                <h3 class="panel-title" style="font-size: 25px; font-weight:bold;">Pre Packed</h3>
                            </div>
                            <div class="panel-body">
                                <!--                    row-->
                                <div class="row">

                                    <!--                        left-->
                                    <div class="col-md-6">
                                        <h2>1. Bottle Style</h2>
                                        <p>Click on the button below to select your bottle.</p>
                                        <hr style="margin-top:-12px;"/>


                                        <!--                    bordolese-->
                                        <button type="button" id="bordolese_bottles" class="btn btn-warning" style="width:180px;text-align: left;">Green Bordolese&nbsp;<span class="glyphicon glyphicon-triangle-bottom" style="float:right;"></span></button>
                                        <br/>
                    <span id="bordolese_bottles">
                    <?php foreach ($bordolese_bottles as $bordolese_bottle) { ?>
                        <input type="radio" name="bottle" id="<?php echo $bordolese_bottle; ?>"
                               value="<?php echo $bordolese_bottle; ?>" onclick='check_value("bordolese")'/>
                        <label for="<?php echo $bordolese_bottle; ?>"><?php echo $bordolese_bottle; ?></label>
                        <br/>
                    <?php } ?>
                        </span>

                                        <div style="margin-top:2px;margin-bottom:2px;"></div>


                                        <!--                    bordeaux bottles-->
                                        <button type="button" id="bordeaux_bottles" class="btn btn-warning" style="width:180px;text-align: left;">Green Bordeaux<span class="glyphicon glyphicon-triangle-bottom" style="float:right;"></span></button>
                                        <br/>
                    <span id="bordeaux_bottles">
                        <?php foreach ($bordeaux_bottles as $bordeaux_bottle) { ?>
                            <input type="radio" name="bottle" id="<?php echo $bordeaux_bottle; ?>"
                                   value="<?php echo $bordeaux_bottle; ?>"
                                   onclick='check_value("<?php if ($bordeaux_bottle === "BORDEAUX - 12/375 ml") {
                                       echo "bordeaux_375";
                                   } else if ($bordeaux_bottle === "BORDEAUX - 12/500 ml") {
                                       echo "bordeaux_500";
                                   } else if ($bordeaux_bottle === "BORDEAUX - 12/750 ml") {
                                       echo "bordeaux_750";
                                   }
                                   ?>")'/>
                            <label for="<?php echo $bordeaux_bottle; ?>"><?php echo $bordeaux_bottle; ?></label>
                            <br/>
                        <?php } ?>
                    </span>


                                        <div style="margin-top:2px;margin-bottom:2px;"></div>


                                        <!--                    Antique/green dorica bottles-->
                                        <button type="button" id="dorica_bottles" class="btn btn-warning" style="width:180px;text-align: left;">Antique Dorica<span class="glyphicon glyphicon-triangle-bottom" style="float:right;"></span></button>
                                        <br/>
                    <span id="dorica_bottles">
                        <?php foreach ($dorica_bottles as $dorica_bottle) { ?>
                            <input type="radio" name="bottle" id="<?php echo $dorica_bottle; ?>"
                                   value="<?php echo $dorica_bottle; ?>"
                                   onclick='check_value("<?php if ($dorica_bottle === "DORICA - 24/100 ML") {
                                       echo "dorica_antique_100";
                                   } else if ($dorica_bottle === "DORICA - 12/250 ML") {
                                       echo "dorica_antique_250";
                                   } else if ($dorica_bottle === "DORICA - 12/500 ML") {
                                       echo "dorica_antique_500";
                                   }
                                   ?>")'/>
                            <label for="<?php echo $dorica_bottle; ?>"><?php echo $dorica_bottle; ?></label>
                            <br/>
                        <?php } ?>
                    </span>

                                        <div style="margin-top:2px;margin-bottom:2px;"></div>

                                        <!--                    clear dorica bottles-->
                                        <button type="button" id="clear_dorica_bottles" class="btn btn-warning" style="width:180px;text-align: left;">Clear Dorica<span class="glyphicon glyphicon-triangle-bottom" style="float:right;"></span></button>
                                        <br/>
                    <span id="clear_dorica_bottles">
                        <?php foreach ($clear_dorica_bottles as $clear_dorica_bottle) { ?>
                            <input type="radio" name="bottle" id="<?php echo $clear_dorica_bottle; ?>"
                                   value="<?php echo $clear_dorica_bottle; ?>"
                                   onclick='check_value("<?php if ($clear_dorica_bottle === "CLEAR DORICA - 12/250 ML") {
                                       echo "dorica_clear_250";
                                   } else if ($clear_dorica_bottle === "CLEAR DORICA - 12/500 ML") {
                                       echo "dorica_clear_500";
                                   }
                                   ?>")'/>
                            <label for="<?php echo $clear_dorica_bottle; ?>"><?php echo $clear_dorica_bottle; ?></label>
                            <br/>
                        <?php } ?>
                    </span>


                                        <div style="margin-top:2px;margin-bottom:2px;"></div>

                                        <!--                  clear  marasca bottles-->
                                        <button type="button" id="marasca_bottles" class="btn btn-warning" style="width:180px;text-align: left;">Clear Marasca<span class="glyphicon glyphicon-triangle-bottom" style="float:right;"></span></button>
                                        <br/>
                    <span id="marasca_bottles">
                        <?php foreach ($marasca_bottles as $marasca_bottle) { ?>
                            <input type="radio" name="bottle" id="<?php echo $marasca_bottle; ?>"
                                   value="<?php echo $marasca_bottle; ?>"
                                   onclick='check_value("<?php if ($marasca_bottle === "MARASCA - 24/100 ml") {
                                       echo "marasca_clear_100";
                                   } else if ($marasca_bottle === "MARASCA - 12/250 ml") {
                                       echo "marasca_clear_250";
                                   } else if ($marasca_bottle === "MARASCA - 12/500 ml") {
                                       echo "marasca_clear_500";
                                       //TODO:SEE IF WE SELL MARASCA 750ML
                                   } else if ($marasca_bottle === "MARASCA - 12/750 ml") {
                                       echo "marasca_clear_750";
                                       //TODO:SEE IF WE SELL MARASCA 6/1 LITER
                                   } else if ($marasca_bottle === "MARASCA - 6/1 Liter") {
                                       echo "marasca_clear_liter";
                                   }
                                   ?>")'/>
                            <label for="<?php echo $marasca_bottle ?>"><?php echo $marasca_bottle; ?></label>
                            <br/>
                        <?php } ?>
                    </span>

                                        <div style="margin-top:2px;margin-bottom:2px;"></div>

                                        <!--                  GREEN  marasca bottles-->
                                        <button type="button" id="green_marasca_bottles" class="btn btn-warning" style="width:180px;text-align: left;">Green Marasca<span class="glyphicon glyphicon-triangle-bottom" style="float:right;"></span></button>
                                        <br/>
                    <span id="green_marasca_bottles">
                        <?php foreach ($green_marasca_bottles as $green_marasca_bottle) { ?>
                            <input type="radio" name="bottle" id="<?php echo $green_marasca_bottle; ?>"
                                   value="<?php echo $green_marasca_bottle; ?>"
                                   onclick='check_value("<?php if ($green_marasca_bottle === "GREEN MARASCA - 24/100 ml") {
                                       echo "marasca_green_100";
                                   } else if ($green_marasca_bottle === "GREEN MARASCA - 12/250 ml") {
                                       echo "marasca_green_250";
                                   } else if ($green_marasca_bottle === "GREEN MARASCA - 12/750 ml") {
                                       echo "marasca_green_750";
                                   } else if ($green_marasca_bottle === "GREEN MARASCA - 12/500 ml") {
                                       echo "marasca_green_500";
                                   } else if ($green_marasca_bottle === "GREEN MARASCA - 6/1 Liter") {
                                       echo "marasca_green_liter";
                                   }
                                   ?>")'/>
                            <label
                                for="<?php echo $green_marasca_bottle ?>"><?php echo $green_marasca_bottle; ?></label>
                            <br/>
                        <?php } ?>
                    </span>


                                        <div style="margin-top:2px;margin-bottom:2px;"></div>

                                        <!--                  uvag  marasca bottles-->
                                        <button type="button" id="uvag_marasca_bottles" class="btn btn-warning" style="width:180px;text-align: left;">UVAG Marasca<span class="glyphicon glyphicon-triangle-bottom" style="float:right;"></span></button>
                                        <br/>
                    <span id="uvag_marasca_bottles">
                        <?php foreach ($uvag_marasca_bottles as $uvag_marasca_bottle) { ?>
                            <input type="radio" name="bottle" id="<?php echo $uvag_marasca_bottle; ?>"
                                   value="<?php echo $uvag_marasca_bottle; ?>"
                                   onclick='check_value("<?php if ($uvag_marasca_bottle === "UVAG MARASCA - 24/100 ml") {
                                       echo "marasca_uvag_100";
                                   } else if ($uvag_marasca_bottle === "UVAG MARASCA - 12/250 ml") {
                                       echo "marasca_uvag_250";
                                   } else if ($uvag_marasca_bottle === "UVAG MARASCA - 12/500 ml") {
                                       echo "marasca_uvag_500";
                                   } else if ($uvag_marasca_bottle === "UVAG MARASCA - 12/750 ml") {
                                       echo "marasca_uvag_750";
                                   } else if ($uvag_marasca_bottle === "UVAG MARASCA - 6/1 Liter") {
                                       echo "marasca_uvag_liter";
                                   }
                                   ?>")'/>
                            <label for="<?php echo $uvag_marasca_bottle ?>"><?php echo $uvag_marasca_bottle; ?></label>
                            <br/>
                        <?php } ?>
                    </span>

                                        <div style="margin-top:2px;margin-bottom:2px;"></div>

                                        <button type="button" id="serenade_bottles" class="btn btn-warning" style="width:180px;text-align: left;">Clear Serenade<span class="glyphicon glyphicon-triangle-bottom" style="float:right;"></span></button>
                                        <br/>
                    <span id="serenade_bottles">
                        <?php foreach ($serenade_bottles as $serenade_bottle) { ?>
                            <input type="radio" name="bottle" id="<?php echo $serenade_bottle; ?>"
                                   value="<?php echo $serenade_bottle; ?>"
                                   onclick='check_value("<?php if ($serenade_bottle === "SERENADE - 24/100 ml") {
                                       echo "serenade_100";
                                   } else if ($serenade_bottle === "SERENADE - 12/200 ml") {
                                       echo "serenade_200";
                                   } else if ($serenade_bottle === "SERENADE - 12/375 ml") {
                                       echo "serenade_375";
                                   }
                                   ?>")'/>
                            <label for="<?php echo $serenade_bottle; ?>"><?php echo $serenade_bottle; ?></label>
                            <br/>
                        <?php } ?>
                    </span>



                                        <div style="margin-top:2px;margin-bottom:2px;"></div>



                                    </div><!--end left-->

                                    <!--                        right-->
                                    <div class="col-md-6">
                                        <h2>2. Product</h2>
                                        <p>Click on the button below to select your product.</p>
                                        <hr style="margin-top:-12px;"/>

                                        <!--Oils-->
                                        <button type="button" id="oils" class="btn btn-primary" style="width:180px; text-align: left;">Oils<span class="glyphicon glyphicon-triangle-bottom" style="float:right;"></span></button>
                                        <br/>
                    <span id="oils">
                    <?php foreach ($oils as $oil) { ?>
                        <input type="radio" name="product" id="<?php echo $oil; ?>"
                               value="<?php echo $oil; ?>"
                               onclick='check_value("<?php echo $oil; ?>")'/>
                        <label for="<?php echo $oil; ?>"><?php echo $oil; ?></label>
                        <br/>
                    <?php } ?>
                    </span>


                                        <div style="margin-top:2px;margin-bottom:2px;"></div>

<!--                                        infused-->
                                        <button type="button" id="infused" class="btn btn-primary" style="width:180px; text-align: left;">Infused Oils<span class="glyphicon glyphicon-triangle-bottom" style="float:right;"></span></button>
                                        <br/>
                    <span id="infused">
                        <?php foreach ($infused as $infuse) { ?>
                            <input type="radio" name="product" id="<?php echo $infuse; ?>"
                                   value="<?php echo $infuse; ?>"
                                   onclick='check_value("<?php echo $infuse; ?>")'
                                />
                            <label for="<?php echo $infuse; ?>"><?php echo $infuse; ?></label>
                            <br/>
                        <?php } ?>
                    </span>

                                        <div style="margin-top:2px;margin-bottom:2px;"></div>
                                        <!--organic-->
                                        <button type="button" id="organic" class="btn btn-primary" style="width:180px; text-align: left;">Organic Oils<span class="glyphicon glyphicon-triangle-bottom" style="float:right;"></span></button>
                                        <br/>
                    <span id="organic">
                        <?php foreach ($organics as $organic) { ?>
                            <input type="radio" name="product" id="<?php echo $organic; ?>"
                                   value="<?php echo $organic; ?>"
                                   onclick='check_value("<?php echo $organic; ?>")'
                                />
                            <label for="<?php echo $organic; ?>"><?php echo $organic; ?></label>
                            <br/>
                        <?php } ?>
                    </span>


                                        <div style="margin-top:2px;margin-bottom:2px;"></div>

                                        <!--other oils-->
                                        <button type="button" id="other_oils" class="btn btn-primary" style="width:180px; text-align: left;">Specialty Oils<span class="glyphicon glyphicon-triangle-bottom" style="float:right;"></span></button>
                                        <br/>
                    <span id="other_oils">
                        <?php foreach ($other_oils as $other_oil) { ?>
                            <input type="radio" name="product" id="<?php echo $other_oil; ?>"
                                   value="<?php echo $other_oil; ?>"
                                   onclick='check_value("<?php echo $other_oil; ?>")'
                                />
                            <label for="<?php echo $other_oil; ?>"><?php echo $other_oil; ?></label>
                            <br/>
                        <?php } ?>
                    </span>
                                        <div style="margin-top:2px;margin-bottom:2px;"></div>

                                        <!--Naturally Flavored Oils-->
                                        <button type="button" id="naturally_flavored_oils" class="btn btn-primary" style="width:180px; text-align: left;">Flavored Oils<span class="glyphicon glyphicon-triangle-bottom" style="float:right;"></span></button>
                                        <br/>
                    <span id="naturally_flavored_oils">
                        <?php foreach ($naturally_flavored_oils as $naturally_flavored_oil) { ?>
                            <input type="radio" name="product" id="<?php echo $naturally_flavored_oil; ?>"
                                   value="<?php echo $naturally_flavored_oil; ?>"
                                   onclick='check_value("<?php echo $naturally_flavored_oil; ?>")'
                                />
                            <label
                                for="<?php echo $naturally_flavored_oil; ?>"><?php echo $naturally_flavored_oil; ?></label>
                            <br/>
                        <?php } ?>
                    </span>

                                        <div style="margin-top:2px;margin-bottom:2px;"></div>

                                        <!--Fused Olive Oils-->
                                        <button type="button" id="fused" class="btn btn-primary" style="width:180px; text-align: left;">Fused Oils<span class="glyphicon glyphicon-triangle-bottom" style="float:right;"></span></button>
                                        <br/>
                    <span id="fused">
                        <?php foreach ($fused_oils as $fused) { ?>
                            <input type="radio" name="product" id="<?php echo $fused; ?>" value="<?php echo $fused; ?>"
                                   onclick='check_value("<?php echo $fused; ?>")'
                                />
                            <label for="<?php echo $fused; ?>"><?php echo $fused; ?></label>
                            <br/>
                        <?php } ?>
                    </span>
                                        <div style="margin-top:2px;margin-bottom:2px;"></div>

                                        <!--Vinegars                    -->
                                        <button type="button" id="vinegars" class="btn btn-primary" style="width:180px; text-align: left;">Vinegars<span class="glyphicon glyphicon-triangle-bottom" style="float:right;"></span></button>
                                        <br/>
                    <span id="vinegars">
                        <?php foreach ($vinegars as $vinegar) { ?>
                            <input type="radio" name="product" id="<?php echo $vinegar; ?>"
                                   value="<?php echo $vinegar; ?>" onclick='check_value("<?php echo $vinegar; ?>")'/>
                            <label for="<?php echo $vinegar; ?>"><?php echo $vinegar; ?></label>
                            <br/>
                        <?php } ?>
                    </span>
                                        <div style="margin-top:2px;margin-bottom:2px;"></div>

                                        <!--Flavored Balsamic Vinegars                    -->
                                        <button type="button" id="flavored_balsamic_vinegars" class="btn btn-primary" style="width:180px; text-align: left;">Flavored Vinegars<span class="glyphicon glyphicon-triangle-bottom" style="float:right;"></span></button>
                                        <br/>
                    <span id="flavored_balsamic_vinegars">
                        <?php foreach ($flavored_balsamic_vinegars as $flavored_balsamic_vinegar) { ?>
                            <input type="radio" name="product" id="<?php echo $flavored_balsamic_vinegar; ?>"
                                   value="<?php echo $flavored_balsamic_vinegar; ?>"
                                   onclick='check_value("<?php echo $flavorded_balsamic_vinegar; ?>")'/>
                            <label
                                for="<?php echo $flavored_balsamic_vinegar; ?>"><?php echo $flavored_balsamic_vinegar; ?></label>
                            <br/>
                        <?php } ?>
                    </span>

                                        <div style="margin-top:2px;margin-bottom:2px;"></div>




                                    </div>
                                    </div>

                                    <!--                    PRODUCT-->
                                <div class="row">
                                    <!--                        left-->
                                    <div class="col-md-6">
                                        <h2>3. Cap</h2>
                                        <p>Click on the item picture below to select the cap.</p>


                                        <div id="bottle"></div>

                                        <div id="cap"></div>

                                    </div><!--end left-->

                                    <!--                        right-->
                                    <div class="col-md-6">
                                        <h2 style="font-size:25px;">4. Capsule/Sleeve</h2>
                                        <p>Click on item picture below to select your capsule/sleeve.</p>
                                        <hr style="margin-top:-12px;"/>

                                        <div id="capsule"></div>


                                    </div>
                                </div>

<!--                                <div class="row" style="margin-top:25px; padding-left: 15px; margin-bottom:0px;">-->
<!--                                    <h2>5. Label</h2>-->
<!--                                    <p>Click on item name to select your label.</p>-->
<!--                                    <div class="col-md-6">-->
<!--                                        <img src="assets/img/unlabeled.jpg" alt="Unlabeled" width="180px"/>-->
<!--                                        <input type="radio" id="unlabeled" name="label" value="Unlabeled"/>-->
<!--                                        <label for="unlabeled">Unlabeled</label>-->
<!--                                        </div>-->
<!--                                    <div class="col-md-6">-->
<!--                                        <img src="assets/img/cc.jpg" alt="Coastal Creations" width="180px" />-->
<!--                                        <br />-->
<!--                                        <input type="radio" id="labeled" name="label" value="Coastal Creations"/>-->
<!--                                        <label for="labeled">Coastal Creations</label>-->
<!--                                    </div>-->
<!--                                </div>-->

                                <!--                    qty and submit-->
                                <div class="row" style="background-color:lightgrey; height:80px; margin-top:25px; margin-bottom:0px;">

                                    <div class="form-group">
                                        <div class="col-md-6"><input type="text" name="qty" value="" class="form-control" placeholder="QTY" style="margin-top:15px;padding-left:5px;"/></div>
                                        <div class="col-md-6">
                                            <input type="submit" value="Add To Cart" class="btn btn-default" style="margin-top:15px;float:right; background-color:#547139;"/></div>

                                    </div>

                                </div>

                            </div><!--end panel body-->

                            </div>

                        </div><!-- end left-->


<!--end new-->

<!--CART-->
                        <!--        right-->
<!--                        <div class="col-md-7">-->
<!---->
<!---->
<!--                           --><?php //include('cart_get.php'); ?>
<!---->
<!---->
<!--                        </div>-->

<!--                    end cart-->
                    </div>

                </div><!--end right-->
                </div><!--end products-->

</form>

<!--instructions-->
<div class="container" style="width:61%">
    <a name="instructions"><h3 style="font-size:45px;font-weight:bold;color:black;"><u>Instructions</u></h3></a>
    <p>The Pre Packed Bottle Order Form is separated into Steps.</p>
    <p><strong>Step 1</strong> is the bottle section. Select the bottle then select the size. (see Step 1 below)</p>
    <p><strong>Step 2</strong> is where you select your product. Select the product then select the flavor.(see Step 2 below)</p>
    <p><strong>Step 3</strong>. is the cap section. Click on the image of the cap to select the cap. If there's only one cap option the cap is automatically selected for you. (see Step 3 below)</p>
    <p><strong>Step 4</strong>. is the capsule/sleeve section. Click on the image of the Capsule/Sleeve to select it. If there's only one capsule/sleeve option the capsule/sleeve is automatically selected for  you.(see Step 4 below)</p>
    <p>Once you've made all 4 selections next is the <strong>quantity of cases</strong>. Once you've entered in your quantity click on the "Add to cart" button. </p>
    <p><strong>Step 5</strong> is your shopping cart. Once you're confident that you're order is correct click on the "Proceed To Checkout" button.</p>
    <p><strong>The Order Form</strong>. The order form <strong>must be</strong> filled out completely. Once you're complete with the Order Form, you must agree to the terms and conditions. Last but not least click on the "Order Button" to submit your order. You will recieve and email reciept once you have completed your order. If you do not see the order reciept in a  few minutes check your spam folder.</p>
</div>
<div class="container">
<div class="row">
    <table class="table table-responsive table-hover table-bordered">
        <thead>
        <tr>
            <th style="text-align:center; color:white; background-color:black;">Step 1.</th>
            <th style="text-align:center; color:white; background-color:black;">Step 2.</th>
            <th style="text-align:center; color:white; background-color:black;">Step 3.</th>
            <th style="text-align:center; color:white; background-color:black;">Step 4.</th>
            >
        </thead>

        <tbody>
        <tr>
            <td><img src="assets/img/instructions/1-select-bottle.jpg" alt="" class="img-responsive" /></td>
            <td><img src="assets/img/instructions/2-select-product.jpg" alt="" class="img-responsive"/></td>
            <td><img src="assets/img/instructions/1-select-cap.jpg" alt="" class="img-responsive"/></td>
            <td><img src="assets/img/instructions/1-select-capsule.jpg" alt="" class="img-responsive"/></td>
            </tr>
        </tbody>
    </table>
    <table class="table table-responsive table-hover table-bordered" style="margin-bottom:100px;">
        <thead>
        <tr>
            <th style="text-align:center; color:white; background-color:black;">Step 5.</th>
            <th style="text-align:center; color:white; background-color:black;">Step 6.</th>
            <th style="text-align:center; color:white; background-color:black;">Step 7.</th>
        </tr>
        </thead>

        <tbody>
        <tr>
            <td><img src="assets/img/instructions/1-select-qty.jpg" alt="" class="img-responsive"/></td>
            <td><img src="assets/img/instructions/1-select-shopping-cart.jpg" alt="" class="img-responsive"/></td>
            <td><img src="assets/img/instructions/1-select-order-form.jpg" alt="" class="img-responsive"/></td>
        </tr>
        </tbody>
    </table>
</div>
</div>
<hr/>
<!--bottles-->
<div class="container">
    <div class="im-centered">

        <div class="row">
            <a name="bottles"><h3 style="font-size:45px;font-weight:bold;color:black;"><u>Bottles</u></h3></a>
            <div class="col-md-1">
                <div class="fix">
                    <img src="assets/img/bottles/375-bordolese.png" alt="" class="img-responsive"/>
                    <div class="desc">
                        <p class="desc_content">Green Bordolese 375ml</p>
                    </div>
                </div>
            </div><!--end col-->

            <div class="col-md-1">
                <div class="fix">
                    <img src="assets/img/bottles/375-bordeaux.jpg" alt="" class="img-responsive"/>
                    <div class="desc">
                        <div class="desc_content">Green Bordeaux 375ml</div>
                    </div>
                </div>
            </div>


            <div class="col-md-1">
                <div class="fix">
                    <img src="assets/img/bottles/500-bordeaux.jpg" alt="" class="img-responsive"/>
                    <div class="desc">
                        <div class="desc_content">Green Bordeaux 500ml</div>
                    </div>
                </div>
            </div>

            <div class="col-md-1">
                <div class="fix">
                    <img src="assets/img/bottles/750-bordeaux.jpg" alt="" class="img-responsive"/>
                    <div class="desc">
                        <div class="desc_content">Green Bordeaux 750ml</div>
                    </div>
                </div>
            </div>

            <div class="col-md-1">
                <div class="fix">
                    <img src="assets/img/bottles/100-antique-dorica.jpg" alt="" class="img-responsive"/>
                    <div class="desc">
                        <div class="desc_content">Antique Dorica 100ml</div>
                    </div>
                </div>
            </div>

            <div class="col-md-1">
                <div class="fix">
                    <img src="assets/img/bottles/250-antique-dorica.jpg" alt="" class="img-responsive"/>
                    <div class="desc">
                        <div class="desc_content">Antique Dorica 250ml</div>
                    </div>
                </div>
            </div>

            <div class="col-md-1">
                <div class="fix">
                    <img src="assets/img/bottles/500-antique-dorica.jpg" alt="" class="img-responsive"/>
                    <div class="desc">
                        <div class="desc_content">Antique Dorica 500ml</div>
                    </div>
                </div>
            </div>
            <div class="col-md-1">
                <div class="fix">
                    <img src="assets/img/bottles/500-green-marasca.jpg" alt="" class="img-responsive"/>
                    <div class="desc">
                        <div class="desc_content">1/2 Green Marasca 500ml</div>
                    </div>
                </div>
            </div>

            <div class="col-md-1">
                <div class="fix">
                    <img src="assets/img/bottles/750-green-marasca.jpg" alt="" class="img-responsive"/>
                    <div class="desc">
                        <div class="desc_content">1/2 Green Marasca 750ml</div>
                    </div>
                </div>
            </div>

            <div class="col-md-1">
                <div class="fix">
                    <img src="assets/img/bottles/500-uvag-marasca.jpg" alt="" class="img-responsive"/>
                    <div class="desc">
                        <div class="desc_content">UVAG Marasca 500ml</div>
                    </div>
                </div>
            </div>
            <div class="col-md-1">
                <div class="fix">
                    <img src="assets/img/bottles/250-clear-dorica.jpg" alt="" class="img-responsive"/>
                    <div class="desc">
                        <div class="desc_content">Clear Dorica 250ml</div>
                    </div>
                </div>
            </div>

            <div class="col-md-1">
                <div class="fix">
                    <img src="assets/img/bottles/500-clear-dorica.jpg" alt="" class="img-responsive"/>
                    <div class="desc">
                        <div class="desc_content">Clear Dorica 500ml</div>
                    </div>
                </div>
            </div>


            <div class="col-md-1">
                <div class="fix">
                    <img src="assets/img/bottles/100-clear-marasca.jpg" alt="" class="img-responsive"/>
                    <div class="desc">
                        <div class="desc_content">Clear Marasca 100ml</div>
                    </div>
                </div>
            </div>



            <div class="col-md-1">
                <div class="fix">
                    <img src="assets/img/bottles/250-clear-marasca.jpg" alt="" class="img-responsive"/>
                    <div class="desc">
                        <div class="desc_content">Clear Marasca 250ml</div>
                    </div>
                </div>
            </div>

            <div class="col-md-1">
                <div class="fix">
                    <img src="assets/img/bottles/500-clear-marasca.jpg" alt="" class="img-responsive"/>
                    <div class="desc">
                        <div class="desc_content">Clear Marasca 500ml</div>
                    </div>
                </div>
            </div>

            <div class="col-md-1">
                <div class="fix">
                    <img src="assets/img/bottles/100-serenade.jpg" alt="" class="img-responsive"/>
                    <div class="desc">
                        <div class="desc_content">Clear Serenade 100ml</div>
                    </div>
                </div>
            </div>

            <div class="col-md-1">
                <div class="fix">
                    <img src="assets/img/bottles/200-serenade.jpg" alt="" class="img-responsive"/>
                    <div class="desc">
                        <div class="desc_content">Clear Serenade 200ml</div>
                    </div>
                </div>
            </div>

            <div class="col-md-1">
                <div class="fix">
                    <img src="assets/img/bottles/375-serenade.jpg" alt="" class="img-responsive"/>
                    <div class="desc">
                        <div class="desc_content">Clear Serenade 375ml</div>
                    </div>
                </div>
            </div>

            <div class="col-md-1">
                <div class="fix">
                    <img src="assets/img/bottles/100-stephanie.jpg" alt="" class="img-responsive"/>
                    <div class="desc">
                        <div class="desc_content">Clear Stephanie 100ml</div>
                    </div>
                </div>
            </div>

            <div class="col-md-1">
                <div class="fix">
                    <img src="assets/img/bottles/200-stephanie.jpg" alt="" class="img-responsive"/>
                    <div class="desc">
                        <div class="desc_content">Clear Stephanie 200ml</div>
                    </div>
                </div>
            </div>

        </div><!--end row-->
    </div>
</div>
        <!--footer-->
        <?php include('includes/footer.php'); ?>


<?php include('includes/bottom_js.php'); ?>
</body>
</html>
