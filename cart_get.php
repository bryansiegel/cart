<?php
include('includes/cart_get_top.php');
?>


<?php //check if the session is new or not to show the cart
if ($_SESSION['visits'] > 1 && isset($_SESSION['id'])) { ?>


<?php  //get the products by the id
$products = ProductsQuery::create()->findByOrderId($_SESSION['id']);
?>

    <table class="table table-hover table-bordered table-responsive">
        <thead>
        <tr style="background-color: #080808; color:white;">
            <td colspan="10"><h3 class="panel-title" style="font-size: 25px; font-weight:bold;"><span class="glyphicon glyphicon-shopping-cart" style="padding-right:5px;"></span>Your Shopping Cart</h3></td>
        </tr>
        <tr>
            <th><strong>Product</strong></th>
            <th><strong>Bottle</strong></th>
            <th><strong>Cap</strong></th>
            <th><strong>Capsule/Sleeve</strong></th>
            <th><strong>Qty</strong></th>
<!--            <th><strong>Label</strong></th>-->
            <th><strong>Price</strong></th>
<!--            <th><strong>Accessory Price</strong></th>-->
<!--            <th><strong>Total Price</strong></th>-->
            <th></th>
        </tr>
        </thead>

        <?php
        //need these to calculate totals

        //price
        $total_items_price = array();

        //case count
        $total_items_quantity = array();
        ?>
              <tbody>
        <?php foreach ($products as $index) { ?>
            <tr>
                <td><?php echo $index->getProduct(); ?></td>
                <td><?php echo $index->getBottle(); ?></td>
                <td><?php echo $index->getCap(); ?></td>
                <td><?php
                    //TODO:IF CAPSULE GET PRICE?
                    echo $index->getCapsule();

                    ?></td>
                <td><?php
                    //TODO:REFACTOR
                    echo $index->getQty();
                    $total_items_quantity[] = $index->getQty();
                    ?></td>
<!--                <td>--><?php //echo $index->getLabel(); ?><!--</td>-->
                <td><?php
                    $total_product_price = $index->getQty() * $index->getPrice();
                    echo "$" . number_format($total_product_price, 2, '.', ','); ?></td>
<!--                <td>--><?php //echo "$" . number_format($index->getUpgradePrice(), 2, '.', ','); ?><!--</td>-->
<!--                <td>--><?php
//                    //TODO:REFACTOR
//                    $total_product_price = $index->getQty() * $index->getPrice();
//                    $upgrade_product_price = $index->getUpgradePrice();
//                    $total_price = $total_product_price + $upgrade_product_price;
//                    echo "$" . number_format($total_price, 2, '.', ',');
//                    $total_items_price[] = $total_price;
//                    ?><!--</td>-->

                <td>
                    <?php //delete post to delete form  ?>
                    <form action="cart_delete.php" method="post">
                        <input type="hidden" name="id" value="<?php echo $index->getId(); ?>"/>
                        <input type="submit" value="Delete" class="btn btn-danger"/>
                    </form>

                </td>
            </tr>
        <?php } ?>
        <tr>
            <td colspan="10">
                <span style="float:right;"><strong>Total Quantity: <?php echo array_sum($total_items_quantity); ?></strong></span>
                <br/>
                <hr/>
            <span style="float:right;"><strong>Total Price: $
                    <?php
                    //TODO:REFACTOR
                    $total_all_price = array_sum($total_items_price);
                    echo number_format($total_all_price, 2, '.', ',');
                    ?> </strong></span>
            </td>
        </tr>
        <tr>
            <td colspan="10">
                <!-- Button trigger modal -->
                <button type="button" class="btn btn-default" style="margin-top:15px;float:right; background-color:#547139;" data-toggle="modal" data-target="#myModal">
                    Proceed To Checkout
                </button>
            </td>
        </tr>
        </tbody>
    </table>

    <?php } ?>


<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <?php include('order_form.php'); ?>
            </div>

        </div>
    </div>
</div>
