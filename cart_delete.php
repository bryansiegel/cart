<?php

if(!isset($_SESSION)) session_start();

// setup the autoloading
require_once 'vendor/autoload.php';

// setup Propel orm
require_once 'generated-conf/config.php';

//get the id from the form
if(isset($_REQUEST['id']))
{
    $id = $_REQUEST['id'];
}

//find the product
$product = ProductsQuery::create()->findById($id);

//delete it
$product->delete();

//redirect
header('Location: /_PROJECTS/cart/cart_new.php');

?>