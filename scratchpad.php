//clear dorica bottles
//oils
case "Olive Oil Extra Virgin DORICA - 12/250 ML":
$price = 31.00;
break;
case "Australia - Leccino/Frantoio DORICA - 12/250 ML":
$price = 38.00;
break;
case "Australia - Frantoio DORICA - 12/250 ML":
$price = 38.00;
break;
case "California Arbequina DORICA - 12/250 ML":
$price = 55.00;
break;
case "California Arbosana DORICA - 12/250 ML":
$price = 53.00;
break;
case "California Mission DORICA - 12/250 ML":
$price = 53.00;
break;
case "California Arbequina/Arbosana/Koroneiki DORICA - 12/250 ML":
$price = 38.00;
break;
case "Chile - Arbequina DORICA - 12/250 ML":
$price = 31.00;
break;
case "Chile - Frantoio DORICA - 12/250 ML":
$price = 31.00;
break;
case "Chile - Leccino DORICA - 12/250 ML":
$price = 31.00;
break;
case "Chile - Picual DORICA - 12/250 ML":
$price = 30.00;
break;
case "Greece - Kalamata DORICA - 12/250 ML":
$price = 35.00;
break;
case "Greece - Koroneiki DORICA - 12/250 ML":
$price = 35.00;
break;
case "Italy - Umbrian region DORICA - 12/250 ML":
$price = 51.00;
break;
case "Italy - Calabria region - Carolea DORICA - 12/250 ML":
$price = 44.00;
break;
case "Italy (Puglia) - Coratina DORICA - 12/250 ML":
$price = 44.00;
break;
case "Italy (Puglia)- Ogliarola DORICA - 12/250 ML":
$price = 44.00;
break;
case "Spain - Hojiblanca DORICA - 12/250 ML":
$price = 36.00;
break;
case "Spain - Picual DORICA - 12/250 ML":
$price = 36.00;
break;
case "Spain - Signature blend DORICA - 12/250 ML":
$price = 36.00;
break;
case "Tunisia - Chemlali/Chetoui DORICA - 12/250 ML":
$price = 31.00;
break;
//infused
case "Black Pepper DORICA - 12/250 ML":
$price = 47.00;
break;
case "Garlic Oil DORICA - 12/250 ML":
$price = 40.00;
break;
case "Roasted Chili DORICA - 12/250 ML":
$price = 42.00;
break;
case "Oregano DORICA - 12/250 ML":
$price = 43.00;
break;
case "Lemon Pepper DORICA - 12/250 ML":
$price = 50.00;
break;
case "Jalapeno oil DORICA - 12/250 ML":
$price = 40.00;
break;
case "Basil DORICA - 12/250 ML":
$price = 41.00;
break;
case "Scallion DORICA - 12/250 ML":
$price = 44.00;
break;
case "Habenero DORICA - 12/250 ML":
$price = 53.00;
break;
case "Rosemary DORICA - 12/250 ML":
$price = 45.00;
break;
case "Chipotle DORICA - 12/250 ML":
$price = 42.00;
break;
//flavored olive oils
case "Blood Orange DORICA - 12/250 ML":
$price = 33.00;
break;
case "Lemon oil DORICA - 12/250 ML":
$price = 37.00;
break;
case "Lime DORICA - 12/250 ML":
$price = 38.00;
break;
case "Orange oil DORICA - 12/250 ML":
$price = 33.00;
break;
case "Tangerine oil DORICA - 12/250 ML":
$price = 34.00;
break;
case "Butter DORICA - 12/250 ML":
$price = 40.00;
break;
case "Cranberry Walnut oil DORICA - 12/250 ML":
$price =  33.00;
break;
case "Arbequina Rhubarb DORICA - 12/250 ML":
$price = 33.00;
break;
case "Sage Onion DORICA - 12/250 ML":
$price = 45.00;
break;
case "Sundried Tomato DORICA - 12/250 ML":
$price = 39.00;
break;
case "White Truffle DORICA - 12/250 ML":
$price = 80.00;
break;
case "Black Truffle oil DORICA - 12/250 ML":
$price = 79.00;
break;
case "Hickory oil DORICA - 12/250 ML":
$price = 38.00;
break;
case "Vanilla oil DORICA - 12/250 ML":
$price = 39.00;
break;
//fused
case "Garlic Mushroom DORICA - 12/250 ML":
$price = 45.00;
break;
case "Basil/Lemongrass DORICA - 12/250 ML":
$price = 46.00;
break;
case "Rosemary with Lavender DORICA - 12/250 ML":
$price = 50.00;
break;
case "Tuscan Herb DORICA - 12/250 ML":
$price = 49.00;
break;
case "Herbs d Provence DORICA - 12/250 ML":
$price = 48.00;
break;
case "Citrus Habanero DORICA - 12/250 ML":
$price = 43.00;
break;
case "Garlic Roasted Chili DORICA - 12/250 ML":
$price = 45.00;
break;
//specialty oils
case "Almond Oil DORICA - 12/250 ML":
$price = 38.00;
break;
case "Apricot Oil DORICA - 12/250 ML":
$price = 39.00;
break;
case "Avocado Oil DORICA - 12/250 ML":
$price = 35.00;
break;
case "Flaxseed Oil DORICA - 12/250 ML":
$price = 29.00;
break;
case "Grapeseed Oil DORICA - 12/250 ML":
$price = 25.00;
break;
case "Hempseed Oil DORICA - 12/250 ML":
$price = 59.00;
break;
case "Macadamia Nut Oil DORICA - 12/250 ML":
$price = 43.00;
break;
case "Toasted Sesame Oil DORICA - 12/250 ML":
$price = 30.00;
case "Walnut Oil DORICA - 12/250 ML":
$price = 28.00;
break;
//organics
case "Olive Oil, Extra Virgin, Organic , Italy DORICA - 12/250 ML":
$price = 38.00;
break;
case "Olive Oil, Extra Virgin, Organic , Country Varies DORICA - 12/250 ML":
$price = 33.00;
break;
case "Canola DORICA - 12/250 ML":
$price = 23.00;
break;
case "Soybean Oil DORICA - 12/250 ML":
$price = 19.00;
break;
case "Sunflower DORICA - 12/250 ML":
$price = 30.00;
break;
case "Sesame Toasted DORICA - 12/250 ML":
$price = 29.00;
break;
case "Sesame, Virgin DORICA - 12/250 ML":
$price = 29.00;
break;
//vinegars
case "4 Star Balsamic DORICA - 12/250 ML":
$price = 19.00;
break;
case "8 Star Balsamic DORICA - 12/250 ML":
$price = 26.00;
break;
case "12 Star Balsamic DORICA - 12/250 ML":
$price = 30.00;
break;
case "25 Star Dark Balsamic DORICA - 12/250 ML":
$price = 43.00;
break;
case "25 Star White Balsamic DORICA - 12/250 ML":
$price = 43.00;
break;
case "6 Star White Balsamic DORICA - 12/250 ML":
$price = 25.00;
break;
case "Red Wine Vinegar - Domestic - 50 Grain DORICA - 12/250 ML":
$price = 16.00;
break;
case "Organic Premium Quality Balsamic - Ltd Edition DORICA - 12/250 ML":
$price = 45.00;
break;
case "Serrano Chile Vinegar DORICA - 12/250 ML":
$price = 47.00;
break;
case "Lambrusco Vinegar DORICA - 12/250 ML":
$price = 43.00;
break;
//flavored balsamic
case "Almond Creme DORICA - 12/250 ML":
$price = 43.00;
break;
case "Apricot DORICA - 12/250 ML":
$price = 43.00;
break;
case "Bittersweet Chocolate with Orange DORICA - 12/250 ML":
$price = 43.00;
break;
case "Blackberry DORICA - 12/250 ML":
$price = 43.00;
break;
case "Blackberry Ginger DORICA - 12/250 ML":
$price = 43.00;
break;
case "Black Currant DORICA - 12/250 ML":
$price = 43.00;
break;
case "Black Walnut DORICA - 12/250 ML":
$price = 43.00;
break;
case "Blueberry DORICA - 12/250 ML":
$price = 43.00;
break;
case "Bordeaux Cherry DORICA - 12/250 ML":
$price = 43.00;
break;
case "Chili DORICA - 12/250 ML":
$price = 43.00;
break;
case "Chocolate DORICA - 12/250 ML":
$price = 43.00;
break;
case "Chocolate Covered Cherries DORICA - 12/250 ML":
$price = 43.00;
break;
case "Chocolate Jalapeno DORICA - 12/250 ML":
$price = 43.00;
break;
case "Chocolate Marshmallow DORICA - 12/250 ML":
$price = 43.00;
break;
case "Coconut DORICA - 12/250 ML":
$price = 43.00;
break;
case "Cranberry Orange DORICA - 12/250 ML":
$price = 43.00;
break;
case "Cranberry Walnut DORICA - 12/250 ML":
$price = 43.00;
break;
case "Cucumber Melon DORICA - 12/250 ML":
$price = 43.00;
break;
case "Elderberry DORICA - 12/250 ML":
$price = 43.00;
break;
case "Espresso DORICA - 12/250 ML":
$price = 43.00;
break;
case "Fig DORICA - 12/250 ML":
$price = 43.00;
break;
case "Garlic DORICA - 12/250 ML":
$price = 43.00;
break;
case "Garlic Cilantro DORICA - 12/250 ML":
$price = 43.00;
break;
case "Grapefruit DORICA - 12/250 ML":
$price = 43.00;
break;
case "Green Apple DORICA - 12/250 ML":
$price = 43.00;
break;
case "Hickory DORICA - 12/250 ML":
$price = 43.00;
break;
case "Honey DORICA - 12/250 ML":
$price = 43.00;
break;
case "Honey Ginger DORICA - 12/250 ML":
$price = 43.00;
break;
case "Huckleberry DORICA - 12/250 ML":
$price = 43.00;
break;
case "Jalapeno DORICA - 12/250 ML":
$price = 43.00;
break;
case "Jalapeno Fig in 8 Star DORICA - 12/250 ML":
$price = 38.00;
break;
case "Jalapeno & Lime DORICA - 12/250 ML":
$price = 43.00;
break;
case "Lavender DORICA - 12/250 ML":
$price = 43.00;
break;
case "Lemongrass Mint DORICA - 12/250 ML":
$price = 43.00;
break;
case "Lemon DORICA - 12/250 ML":
$price = 43.00;
break;
case "Lemon Vanilla DORICA - 12/250 ML":
$price = 43.00;
break;
case "Mandarin Orange DORICA - 12/250 ML":
$price = 43.00;
break;
case "Mango DORICA - 12/250 ML":
$price = 43.00;
break;
case "Mocha Almond Fudge DORICA - 12/250 ML":
$price = 43.00;
break;
case "Orange DORICA - 12/250 ML":
$price = 43.00;
break;
case "Orange/Mango/Passionfruit DORICA - 12/250 ML":
$price = 43.00;
break;
case "Orange Vanilla DORICA - 12/250 ML":
$price = 43.00;
break;
case "Peach DORICA - 12/250 ML":
$price = 43.00;
break;
case "Pear DORICA - 12/250 ML":
$price = 43.00;
break;
case "Pecan Praline DORICA - 12/250 ML":
$price = 43.00;
break;
case "Pineapple DORICA - 12/250 ML":
$price = 43.00;
break;
case "Pomegranate DORICA - 12/250 ML":
$price = 43.00;
break;
case "Pumpkin Spice DORICA - 12/250 ML":
$price = 43.00;
break;
case "Raspberry DORICA - 12/250 ML":
$price = 43.00;
break;
case "Raspberry Ginger DORICA - 12/250 ML":
$price = 43.00;
break;
case "Strawberry DORICA - 12/250 ML":
$price = 43.00;
break;
case "Strawberry Peach DORICA - 12/250 ML":
$price = 43.00;
break;
case "Tangerine DORICA - 12/250 ML":
$price = 43.00;
break;
case "Black Truffle DORICA - 12/250 ML":
$price = 43.00;
break;
case "Vanilla DORICA - 12/250 ML":
$price = 43.00;
break;
case "Vanilla Fig DORICA - 12/250 ML":
$price = 43.00;
break;
