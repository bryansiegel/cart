<?php

if(!isset($_SESSION))
{
    session_start();

}

?>

<!DOCTYPE html>
<html>

<head>
    <meta content='IE=EmulateIE7' http-equiv='X-UA-Compatible'/>
    <meta content='width=1100' name='viewport'/>
    <meta content='text/html; charset=UTF-8' http-equiv='Content-Type'/>

    <script src="//code.jquery.com/jquery-1.11.0.min.js"></script>


    <style>
        .products input[type="radio"] {
            display:none;
        }

        .products label {
            display:inline-block;
            background-color:#ddd;
            padding:4px 11px;
            font-family:Arial;
            font-size:16px;
        }

        .products input[type="radio"]:checked + label {
            border:1px solid black;
            color:red;
        }
    </style>

    <script language='JavaScript' type='text/javascript'>

        function check_value(fieldvalue)
        {
            switch(fieldvalue)
            {
                case 1:

                    document.getElementById("flags").innerHTML = "<img src='http://www.cibariasoapsupply.com/shop/media/catalog/product/cache/3/small_image/150x130/9df78eab33525d08d6e5fb8d27136e95/b/o/bottle_bordelese_375ml_1.jpg'>";
                    break;

                case 2:

                    document.getElementById("flags").innerHTML = "<img src='http://www.cibariasoapsupply.com/shop/media/catalog/product/cache/3/small_image/150x130/9df78eab33525d08d6e5fb8d27136e95/2/5/250_half-green-marasca.jpg'>";
                    break;

                case 3:

                    document.getElementById("flags").innerHTML = "<img src='bolivia.jpg'>";
                    break;

                case 4:

                    document.getElementById("flags").innerHTML = "<img src='cuba.jpg'>";
                    break;

                case 5:

                    document.getElementById("flags").innerHTML = "<img src='finland.jpg'>";
                    break;

                case 6:

                    document.getElementById("flags").innerHTML = "<img src='france.jpg'>";
                    break;

                case 7:

                    document.getElementById("flags").innerHTML = "<img src='italy.jpg'>";
                    break;

                case 8:

                    document.getElementById("flags").innerHTML = "<img src='peru.jpg'>";
                    break;

                case 9:

                    document.getElementById("flags").innerHTML = "<img src='syria.jpg'>";
                    break;

                case 10:

                    document.getElementById("flags").innerHTML = "<img src='tunisia.jpg'>";
                    break;

            }
        }

    </script>

</head>




<body>


<!--<a href="cart_display.php">Display Cart</a>-->


<form action="" method="post">
    <div class="products">
    <table cellpadding="5px;" border="1">
<tr>
    <thead>
    <td>Bottle</td>
    <td>Cap</td>
    <td>Show Image</td>
    <td>Cart</td>
    <td>Submit</td>
    </thead>
</tr>

    <tr>
        <td>
            <input type="radio" name="bottle" id="bottle1"  value="bottle1" onclick='check_value(1)'>
            <label for="bottle1">bottle1</label>
            <br />
            <input type="radio" name="bottle"  id="bottle2" value="bottle2" onclick='check_value(2)'>
            <label for="bottle2"> bottle2</label>
        </td>


    <td>
        <input type="radio" name="cap" id="cap1" value="cap1">
        <label for="cap1">cap1</label>
        <br />
        <input type="radio" name="cap"  id="cap2" value="cap2">
        <label for="cap2">cap2</label>
    </td>

    <td id="flags">

    </td>

     <td>
         Items:
          <?php  echo sizeof($_SESSION['cart']) ?>
         <hr />

         <?php
         $_SESSION['cart'] = array();

         if(isset($bottle)){
         $bottle = $_REQUEST['bottle'];            
         }

         if(isset($cap)){
         $cap = $_REQUEST['cap'];            
         }

         if(isset($bottle))
         {
            print_r($bottle);
         }

//
         echo "<br />";
//

         if(isset($cap))
         {
         print_r($cap);            
         }
         

         if(isset($cap) && isset($bottle))
         {
         array_push($_SESSION['cart'], $bottle, $cap);            
         }

         ?>


     </td>

    <td>
        <?php include('cart_display.php'); ?>
    </td>

    </tr>


<!--    <select name="bottle" id="" required>-->
<!--        <option value="bottle1">Bottle1</option>-->
<!--        <option value="bottle2">Bottle2</option>-->
<!--    </select>-->
<tr>
    <td>
        <input type="submit"/>
    </td>
</tr>

    </table>
    </div>
</form>


</body>
</html>