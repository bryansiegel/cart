<?php

if(!isset($_SESSION)) session_start();


if($_SERVER['REQUEST_METHOD'] == 'POST'){

    // setup the autoloading
    require_once 'vendor/autoload.php';

    // setup Propel orm
    require_once 'generated-conf/config.php';

    //get the values from post
    $cap = $_REQUEST['cap'];
    $bottle = $_REQUEST['bottle'];
    $product_name = $_REQUEST['product'];
    $label = $_REQUEST['label'];


    if(isset($_REQUEST['qty']) && $_REQUEST['qty'] != "qty")
    {
        $qty = $_REQUEST['qty'];
    } else {
        echo "";
    }
    if(isset($_REQUEST['id']))
    {
        $_SESSION['id'] = $_REQUEST['id'];
    } else {
        echo "";
    }

}
?>

<?php if ($_SESSION['visits'] > 1 && isset($_SESSION['id'])) { ?>

    <form action="order_form_post.php" id="orderForm" method="post">
        <input type="hidden" name="session_id" value="<?php echo $_SESSION['id']; ?>"/>
    <h1>Order Form</h1>
    <?php $products =  ProductsQuery::create()->findByOrderId($_SESSION['id']); ?>
    <table cellspacing="0" cellpadding="0" border="0" width="100%">
        <thead>
        <tr>
            <th align="left" width="48.5%" bgcolor="#d9e5ee" style="padding:5px 9px 6px 9px;border:1px solid #bebcb7;border-bottom:none;line-height:1em">Billing Information:</th>
            <th width="3%"></th>
            <th align="left" width="48.5%" bgcolor="#d9e5ee" style="padding:5px 9px 6px 9px;border:1px solid #bebcb7;border-bottom:none;line-height:1em">Shipping Information:</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td valign="top" style="padding:7px 9px 9px 9px;border:1px solid #bebcb7;border-top:0;background:#f8f7f5">
                <label for="billing_name">Name: (Required)</label>
                <input type="text" id="billing_name"  name="billing_name" size="50" /><br>

                <label for="billing_street_address">Street Address: (Required)</label>
                <input type="text" id="billing_street_address" name="billing_street_address" size="50" /><br>

                <label for="billing_city">City: (Required)</label>
                <input type="text" name="billing_city" /><br>

                <label for="billing_state">State: (Required)</label>
                <input type="text" id="billing_state" name="billing_state" /><br>

                <label for="billing_zip">Zip: (Required)</label>
                <input type="text" id="billing_zip" name="billing_zip" /><br>

                <label for="billing_telephone">Telephone: (Required)</label>
                <input type="text" id="billing_telephone" name="billing_telephone" /><br>

                <label for="billing_email">Email: (Required)</label>
                <input type="text" id="billing_email" name="billing_email" />
            </td>
            <td>&nbsp;</td>
            <td valign="top" style="padding:7px 9px 9px 9px;border:1px solid #bebcb7;border-top:0;background:#f8f7f5">
                <label for="shipping_name">Name: (Required)</label>
                <input type="text" id="shipping_name" name="shipping_name" size="50" /><br>

                <label for="shipping_street_address">Street Address: (Required)</label>
                <input type="text" id="shipping_street_address" name="shipping_street_address" size="50" /><br>

                <label for="shipping_city">City: (Required)</label>
                <input type="text" id="shipping_city" name="shipping_city" /><br>

                <label for="shipping_state">State: (Required)</label>
                <input type="text" id="shipping_state" name="shipping_state" /><br>

                <label for="shipping_zip">Zip: (Required)</label>
                <input type="text" id="shipping_zip" name="shipping_zip" /><br>

                <label for="shipping_telephone">Telephone: (Required)</label>
                <input type="text" id="shipping_telephone" name="shipping_telephone" /><br>

                <label for="shipping_email">Email: (Required)</label>
                <input type="text" id="shipping_email" name="shipping_email" />
            </td>
        </tr>
        </tbody>
    </table>
<br />
    <br />
<!--    end customer portion of form-->

    <table cellspacing="0" cellpadding="0" border="0" width="100%">
        <thead>
        <tr>
            <th align="left" width="48.5%" bgcolor="#d9e5ee" style="padding:5px 9px 6px 9px;border:1px solid #bebcb7;border-bottom:none;line-height:1em">Prefered Shipping Method:</th>
            <th width="3%"></th>
            <th align="left" width="48.5%" bgcolor="#d9e5ee" style="padding:5px 9px 6px 9px;border:1px solid #bebcb7;border-bottom:none;line-height:1em">Notes:</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td valign="top" style="padding:7px 9px 9px 9px;border:1px solid #bebcb7;border-top:0;background:#f8f7f5">
                <select name="shipping_method">
                    <option value="UPS Ground">UPS Ground</option>
                    <option value="UPS Second Day Air">UPS Second Day Air</option>
                    <option value="UPS Three-Day Select">UPS Three-Day Select</option>
                    <option value="UPS Next Day Air">UPS Next Day Air</option>
                    <option value="Pick Up">Pick Up</option>
                    <option value="I Will Use A Freight Company">I Will Use A Freight Company</option>
                </select>
            </td>
            <td>&nbsp;</td>
            <td valign="top" style="padding:7px 9px 9px 9px;border:1px solid #bebcb7;border-top:0;background:#f8f7f5">
                <textarea cols="70" rows="4" name="notes"></textarea>
            </td>
        </tr>
        </tbody>
    </table>
<!--    end shipping portion of form-->
<br />
    <div style="border:1px solid #bebcb7;background:#f8f7f5">
    </div>
    <div style="background-color:#d9e5ee">Products</div>
    <?php foreach($products as $index) { ?>
    <div style="border:1px solid #bebcb7;background:#f8f7f5">
        &nbsp;&nbsp;&nbsp;Quantity&nbsp;&nbsp;&nbsp;
        <input type="text" style="width:35px; color:red; font-weight: bold;" value="<?php echo $index->getQty(); ?>" name="product_qty[]" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <?php echo $index->getProduct(); ?>&nbsp;&nbsp;&nbsp;<?php echo $index->getBottle(); ?>&nbsp;&nbsp;&nbsp;<?php echo $index->getCap(); ?>&nbsp;&nbsp;&nbsp;<?php echo $index->getCapsule(); ?></div>

        <!--        value hidden inputs-->
        <input type="hidden" value="<?php echo $index->getProduct(); ?>" name="product[]" />
        <input type="hidden" value="<?php echo $index->getBottle(); ?>" name="bottle[]"/>
        <input type="hidden" value="<?php echo $index->getCap(); ?>" name="cap[]" />
        <input type="hidden" value="<?php echo $index->getCapsule(); ?>" name="capsule[]" />
        <input type="hidden" value="<?php echo $index->getLabel(); ?>" name="label[]" />


    <?php } ?>
        <div class="checkbox">
            <label for="terms">
                <input type="checkbox" id="terms" name="terms"> You agree the following order is correct.
            </label>
        </div>
        <br />
        <input type="submit" value="Order" class="btn-large btn-danger"/>
    </form>
<?php } else {echo "";} ?>
