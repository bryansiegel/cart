<?php

namespace Base;

use \Products as ChildProducts;
use \ProductsQuery as ChildProductsQuery;
use \Exception;
use \PDO;
use Map\ProductsTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'products' table.
 *
 *
 *
 * @method     ChildProductsQuery orderByProduct($order = Criteria::ASC) Order by the product column
 * @method     ChildProductsQuery orderBySku($order = Criteria::ASC) Order by the sku column
 * @method     ChildProductsQuery orderByPrice($order = Criteria::ASC) Order by the price column
 * @method     ChildProductsQuery orderByUpgradePrice($order = Criteria::ASC) Order by the upgrade_price column
 * @method     ChildProductsQuery orderByActive($order = Criteria::ASC) Order by the active column
 * @method     ChildProductsQuery orderByBottle($order = Criteria::ASC) Order by the bottle column
 * @method     ChildProductsQuery orderByCap($order = Criteria::ASC) Order by the cap column
 * @method     ChildProductsQuery orderByCapsule($order = Criteria::ASC) Order by the capsule column
 * @method     ChildProductsQuery orderByLabel($order = Criteria::ASC) Order by the label column
 * @method     ChildProductsQuery orderByOrderId($order = Criteria::ASC) Order by the order_id column
 * @method     ChildProductsQuery orderByQty($order = Criteria::ASC) Order by the qty column
 * @method     ChildProductsQuery orderById($order = Criteria::ASC) Order by the id column
 *
 * @method     ChildProductsQuery groupByProduct() Group by the product column
 * @method     ChildProductsQuery groupBySku() Group by the sku column
 * @method     ChildProductsQuery groupByPrice() Group by the price column
 * @method     ChildProductsQuery groupByUpgradePrice() Group by the upgrade_price column
 * @method     ChildProductsQuery groupByActive() Group by the active column
 * @method     ChildProductsQuery groupByBottle() Group by the bottle column
 * @method     ChildProductsQuery groupByCap() Group by the cap column
 * @method     ChildProductsQuery groupByCapsule() Group by the capsule column
 * @method     ChildProductsQuery groupByLabel() Group by the label column
 * @method     ChildProductsQuery groupByOrderId() Group by the order_id column
 * @method     ChildProductsQuery groupByQty() Group by the qty column
 * @method     ChildProductsQuery groupById() Group by the id column
 *
 * @method     ChildProductsQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildProductsQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildProductsQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildProductsQuery leftJoinOrder($relationAlias = null) Adds a LEFT JOIN clause to the query using the Order relation
 * @method     ChildProductsQuery rightJoinOrder($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Order relation
 * @method     ChildProductsQuery innerJoinOrder($relationAlias = null) Adds a INNER JOIN clause to the query using the Order relation
 *
 * @method     \OrdersQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildProducts findOne(ConnectionInterface $con = null) Return the first ChildProducts matching the query
 * @method     ChildProducts findOneOrCreate(ConnectionInterface $con = null) Return the first ChildProducts matching the query, or a new ChildProducts object populated from the query conditions when no match is found
 *
 * @method     ChildProducts findOneByProduct(string $product) Return the first ChildProducts filtered by the product column
 * @method     ChildProducts findOneBySku(string $sku) Return the first ChildProducts filtered by the sku column
 * @method     ChildProducts findOneByPrice(string $price) Return the first ChildProducts filtered by the price column
 * @method     ChildProducts findOneByUpgradePrice(string $upgrade_price) Return the first ChildProducts filtered by the upgrade_price column
 * @method     ChildProducts findOneByActive(int $active) Return the first ChildProducts filtered by the active column
 * @method     ChildProducts findOneByBottle(string $bottle) Return the first ChildProducts filtered by the bottle column
 * @method     ChildProducts findOneByCap(string $cap) Return the first ChildProducts filtered by the cap column
 * @method     ChildProducts findOneByCapsule(string $capsule) Return the first ChildProducts filtered by the capsule column
 * @method     ChildProducts findOneByLabel(string $label) Return the first ChildProducts filtered by the label column
 * @method     ChildProducts findOneByOrderId(int $order_id) Return the first ChildProducts filtered by the order_id column
 * @method     ChildProducts findOneByQty(string $qty) Return the first ChildProducts filtered by the qty column
 * @method     ChildProducts findOneById(int $id) Return the first ChildProducts filtered by the id column
 *
 * @method     ChildProducts[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildProducts objects based on current ModelCriteria
 * @method     ChildProducts[]|ObjectCollection findByProduct(string $product) Return ChildProducts objects filtered by the product column
 * @method     ChildProducts[]|ObjectCollection findBySku(string $sku) Return ChildProducts objects filtered by the sku column
 * @method     ChildProducts[]|ObjectCollection findByPrice(string $price) Return ChildProducts objects filtered by the price column
 * @method     ChildProducts[]|ObjectCollection findByUpgradePrice(string $upgrade_price) Return ChildProducts objects filtered by the upgrade_price column
 * @method     ChildProducts[]|ObjectCollection findByActive(int $active) Return ChildProducts objects filtered by the active column
 * @method     ChildProducts[]|ObjectCollection findByBottle(string $bottle) Return ChildProducts objects filtered by the bottle column
 * @method     ChildProducts[]|ObjectCollection findByCap(string $cap) Return ChildProducts objects filtered by the cap column
 * @method     ChildProducts[]|ObjectCollection findByCapsule(string $capsule) Return ChildProducts objects filtered by the capsule column
 * @method     ChildProducts[]|ObjectCollection findByLabel(string $label) Return ChildProducts objects filtered by the label column
 * @method     ChildProducts[]|ObjectCollection findByOrderId(int $order_id) Return ChildProducts objects filtered by the order_id column
 * @method     ChildProducts[]|ObjectCollection findByQty(string $qty) Return ChildProducts objects filtered by the qty column
 * @method     ChildProducts[]|ObjectCollection findById(int $id) Return ChildProducts objects filtered by the id column
 * @method     ChildProducts[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class ProductsQuery extends ModelCriteria
{

    /**
     * Initializes internal state of \Base\ProductsQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'cart', $modelName = '\\Products', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildProductsQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildProductsQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildProductsQuery) {
            return $criteria;
        }
        $query = new ChildProductsQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildProducts|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = ProductsTableMap::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ProductsTableMap::DATABASE_NAME);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildProducts A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT PRODUCT, SKU, PRICE, UPGRADE_PRICE, ACTIVE, BOTTLE, CAP, CAPSULE, LABEL, ORDER_ID, QTY, ID FROM products WHERE ID = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildProducts $obj */
            $obj = new ChildProducts();
            $obj->hydrate($row);
            ProductsTableMap::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildProducts|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildProductsQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ProductsTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildProductsQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ProductsTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the product column
     *
     * Example usage:
     * <code>
     * $query->filterByProduct('fooValue');   // WHERE product = 'fooValue'
     * $query->filterByProduct('%fooValue%'); // WHERE product LIKE '%fooValue%'
     * </code>
     *
     * @param     string $product The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProductsQuery The current query, for fluid interface
     */
    public function filterByProduct($product = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($product)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $product)) {
                $product = str_replace('*', '%', $product);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ProductsTableMap::COL_PRODUCT, $product, $comparison);
    }

    /**
     * Filter the query on the sku column
     *
     * Example usage:
     * <code>
     * $query->filterBySku('fooValue');   // WHERE sku = 'fooValue'
     * $query->filterBySku('%fooValue%'); // WHERE sku LIKE '%fooValue%'
     * </code>
     *
     * @param     string $sku The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProductsQuery The current query, for fluid interface
     */
    public function filterBySku($sku = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($sku)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $sku)) {
                $sku = str_replace('*', '%', $sku);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ProductsTableMap::COL_SKU, $sku, $comparison);
    }

    /**
     * Filter the query on the price column
     *
     * Example usage:
     * <code>
     * $query->filterByPrice('fooValue');   // WHERE price = 'fooValue'
     * $query->filterByPrice('%fooValue%'); // WHERE price LIKE '%fooValue%'
     * </code>
     *
     * @param     string $price The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProductsQuery The current query, for fluid interface
     */
    public function filterByPrice($price = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($price)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $price)) {
                $price = str_replace('*', '%', $price);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ProductsTableMap::COL_PRICE, $price, $comparison);
    }

    /**
     * Filter the query on the upgrade_price column
     *
     * Example usage:
     * <code>
     * $query->filterByUpgradePrice('fooValue');   // WHERE upgrade_price = 'fooValue'
     * $query->filterByUpgradePrice('%fooValue%'); // WHERE upgrade_price LIKE '%fooValue%'
     * </code>
     *
     * @param     string $upgradePrice The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProductsQuery The current query, for fluid interface
     */
    public function filterByUpgradePrice($upgradePrice = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($upgradePrice)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $upgradePrice)) {
                $upgradePrice = str_replace('*', '%', $upgradePrice);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ProductsTableMap::COL_UPGRADE_PRICE, $upgradePrice, $comparison);
    }

    /**
     * Filter the query on the active column
     *
     * Example usage:
     * <code>
     * $query->filterByActive(1234); // WHERE active = 1234
     * $query->filterByActive(array(12, 34)); // WHERE active IN (12, 34)
     * $query->filterByActive(array('min' => 12)); // WHERE active > 12
     * </code>
     *
     * @param     mixed $active The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProductsQuery The current query, for fluid interface
     */
    public function filterByActive($active = null, $comparison = null)
    {
        if (is_array($active)) {
            $useMinMax = false;
            if (isset($active['min'])) {
                $this->addUsingAlias(ProductsTableMap::COL_ACTIVE, $active['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($active['max'])) {
                $this->addUsingAlias(ProductsTableMap::COL_ACTIVE, $active['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProductsTableMap::COL_ACTIVE, $active, $comparison);
    }

    /**
     * Filter the query on the bottle column
     *
     * Example usage:
     * <code>
     * $query->filterByBottle('fooValue');   // WHERE bottle = 'fooValue'
     * $query->filterByBottle('%fooValue%'); // WHERE bottle LIKE '%fooValue%'
     * </code>
     *
     * @param     string $bottle The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProductsQuery The current query, for fluid interface
     */
    public function filterByBottle($bottle = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($bottle)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $bottle)) {
                $bottle = str_replace('*', '%', $bottle);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ProductsTableMap::COL_BOTTLE, $bottle, $comparison);
    }

    /**
     * Filter the query on the cap column
     *
     * Example usage:
     * <code>
     * $query->filterByCap('fooValue');   // WHERE cap = 'fooValue'
     * $query->filterByCap('%fooValue%'); // WHERE cap LIKE '%fooValue%'
     * </code>
     *
     * @param     string $cap The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProductsQuery The current query, for fluid interface
     */
    public function filterByCap($cap = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($cap)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $cap)) {
                $cap = str_replace('*', '%', $cap);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ProductsTableMap::COL_CAP, $cap, $comparison);
    }

    /**
     * Filter the query on the capsule column
     *
     * Example usage:
     * <code>
     * $query->filterByCapsule('fooValue');   // WHERE capsule = 'fooValue'
     * $query->filterByCapsule('%fooValue%'); // WHERE capsule LIKE '%fooValue%'
     * </code>
     *
     * @param     string $capsule The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProductsQuery The current query, for fluid interface
     */
    public function filterByCapsule($capsule = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($capsule)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $capsule)) {
                $capsule = str_replace('*', '%', $capsule);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ProductsTableMap::COL_CAPSULE, $capsule, $comparison);
    }

    /**
     * Filter the query on the label column
     *
     * Example usage:
     * <code>
     * $query->filterByLabel('fooValue');   // WHERE label = 'fooValue'
     * $query->filterByLabel('%fooValue%'); // WHERE label LIKE '%fooValue%'
     * </code>
     *
     * @param     string $label The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProductsQuery The current query, for fluid interface
     */
    public function filterByLabel($label = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($label)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $label)) {
                $label = str_replace('*', '%', $label);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ProductsTableMap::COL_LABEL, $label, $comparison);
    }

    /**
     * Filter the query on the order_id column
     *
     * Example usage:
     * <code>
     * $query->filterByOrderId(1234); // WHERE order_id = 1234
     * $query->filterByOrderId(array(12, 34)); // WHERE order_id IN (12, 34)
     * $query->filterByOrderId(array('min' => 12)); // WHERE order_id > 12
     * </code>
     *
     * @see       filterByOrder()
     *
     * @param     mixed $orderId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProductsQuery The current query, for fluid interface
     */
    public function filterByOrderId($orderId = null, $comparison = null)
    {
        if (is_array($orderId)) {
            $useMinMax = false;
            if (isset($orderId['min'])) {
                $this->addUsingAlias(ProductsTableMap::COL_ORDER_ID, $orderId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($orderId['max'])) {
                $this->addUsingAlias(ProductsTableMap::COL_ORDER_ID, $orderId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProductsTableMap::COL_ORDER_ID, $orderId, $comparison);
    }

    /**
     * Filter the query on the qty column
     *
     * Example usage:
     * <code>
     * $query->filterByQty('fooValue');   // WHERE qty = 'fooValue'
     * $query->filterByQty('%fooValue%'); // WHERE qty LIKE '%fooValue%'
     * </code>
     *
     * @param     string $qty The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProductsQuery The current query, for fluid interface
     */
    public function filterByQty($qty = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($qty)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $qty)) {
                $qty = str_replace('*', '%', $qty);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ProductsTableMap::COL_QTY, $qty, $comparison);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProductsQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(ProductsTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(ProductsTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProductsTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query by a related \Orders object
     *
     * @param \Orders|ObjectCollection $orders The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildProductsQuery The current query, for fluid interface
     */
    public function filterByOrder($orders, $comparison = null)
    {
        if ($orders instanceof \Orders) {
            return $this
                ->addUsingAlias(ProductsTableMap::COL_ORDER_ID, $orders->getId(), $comparison);
        } elseif ($orders instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ProductsTableMap::COL_ORDER_ID, $orders->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByOrder() only accepts arguments of type \Orders or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Order relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildProductsQuery The current query, for fluid interface
     */
    public function joinOrder($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Order');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Order');
        }

        return $this;
    }

    /**
     * Use the Order relation Orders object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \OrdersQuery A secondary query class using the current class as primary query
     */
    public function useOrderQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinOrder($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Order', '\OrdersQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildProducts $products Object to remove from the list of results
     *
     * @return $this|ChildProductsQuery The current query, for fluid interface
     */
    public function prune($products = null)
    {
        if ($products) {
            $this->addUsingAlias(ProductsTableMap::COL_ID, $products->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the products table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ProductsTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ProductsTableMap::clearInstancePool();
            ProductsTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ProductsTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(ProductsTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            ProductsTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            ProductsTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // ProductsQuery
