<?php

namespace Map;

use \Products;
use \ProductsQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'products' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class ProductsTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.ProductsTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'cart';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'products';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Products';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Products';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 12;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 12;

    /**
     * the column name for the PRODUCT field
     */
    const COL_PRODUCT = 'products.PRODUCT';

    /**
     * the column name for the SKU field
     */
    const COL_SKU = 'products.SKU';

    /**
     * the column name for the PRICE field
     */
    const COL_PRICE = 'products.PRICE';

    /**
     * the column name for the UPGRADE_PRICE field
     */
    const COL_UPGRADE_PRICE = 'products.UPGRADE_PRICE';

    /**
     * the column name for the ACTIVE field
     */
    const COL_ACTIVE = 'products.ACTIVE';

    /**
     * the column name for the BOTTLE field
     */
    const COL_BOTTLE = 'products.BOTTLE';

    /**
     * the column name for the CAP field
     */
    const COL_CAP = 'products.CAP';

    /**
     * the column name for the CAPSULE field
     */
    const COL_CAPSULE = 'products.CAPSULE';

    /**
     * the column name for the LABEL field
     */
    const COL_LABEL = 'products.LABEL';

    /**
     * the column name for the ORDER_ID field
     */
    const COL_ORDER_ID = 'products.ORDER_ID';

    /**
     * the column name for the QTY field
     */
    const COL_QTY = 'products.QTY';

    /**
     * the column name for the ID field
     */
    const COL_ID = 'products.ID';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Product', 'Sku', 'Price', 'UpgradePrice', 'Active', 'Bottle', 'Cap', 'Capsule', 'Label', 'OrderId', 'Qty', 'Id', ),
        self::TYPE_STUDLYPHPNAME => array('product', 'sku', 'price', 'upgradePrice', 'active', 'bottle', 'cap', 'capsule', 'label', 'orderId', 'qty', 'id', ),
        self::TYPE_COLNAME       => array(ProductsTableMap::COL_PRODUCT, ProductsTableMap::COL_SKU, ProductsTableMap::COL_PRICE, ProductsTableMap::COL_UPGRADE_PRICE, ProductsTableMap::COL_ACTIVE, ProductsTableMap::COL_BOTTLE, ProductsTableMap::COL_CAP, ProductsTableMap::COL_CAPSULE, ProductsTableMap::COL_LABEL, ProductsTableMap::COL_ORDER_ID, ProductsTableMap::COL_QTY, ProductsTableMap::COL_ID, ),
        self::TYPE_RAW_COLNAME   => array('COL_PRODUCT', 'COL_SKU', 'COL_PRICE', 'COL_UPGRADE_PRICE', 'COL_ACTIVE', 'COL_BOTTLE', 'COL_CAP', 'COL_CAPSULE', 'COL_LABEL', 'COL_ORDER_ID', 'COL_QTY', 'COL_ID', ),
        self::TYPE_FIELDNAME     => array('product', 'sku', 'price', 'upgrade_price', 'active', 'bottle', 'cap', 'capsule', 'label', 'order_id', 'qty', 'id', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Product' => 0, 'Sku' => 1, 'Price' => 2, 'UpgradePrice' => 3, 'Active' => 4, 'Bottle' => 5, 'Cap' => 6, 'Capsule' => 7, 'Label' => 8, 'OrderId' => 9, 'Qty' => 10, 'Id' => 11, ),
        self::TYPE_STUDLYPHPNAME => array('product' => 0, 'sku' => 1, 'price' => 2, 'upgradePrice' => 3, 'active' => 4, 'bottle' => 5, 'cap' => 6, 'capsule' => 7, 'label' => 8, 'orderId' => 9, 'qty' => 10, 'id' => 11, ),
        self::TYPE_COLNAME       => array(ProductsTableMap::COL_PRODUCT => 0, ProductsTableMap::COL_SKU => 1, ProductsTableMap::COL_PRICE => 2, ProductsTableMap::COL_UPGRADE_PRICE => 3, ProductsTableMap::COL_ACTIVE => 4, ProductsTableMap::COL_BOTTLE => 5, ProductsTableMap::COL_CAP => 6, ProductsTableMap::COL_CAPSULE => 7, ProductsTableMap::COL_LABEL => 8, ProductsTableMap::COL_ORDER_ID => 9, ProductsTableMap::COL_QTY => 10, ProductsTableMap::COL_ID => 11, ),
        self::TYPE_RAW_COLNAME   => array('COL_PRODUCT' => 0, 'COL_SKU' => 1, 'COL_PRICE' => 2, 'COL_UPGRADE_PRICE' => 3, 'COL_ACTIVE' => 4, 'COL_BOTTLE' => 5, 'COL_CAP' => 6, 'COL_CAPSULE' => 7, 'COL_LABEL' => 8, 'COL_ORDER_ID' => 9, 'COL_QTY' => 10, 'COL_ID' => 11, ),
        self::TYPE_FIELDNAME     => array('product' => 0, 'sku' => 1, 'price' => 2, 'upgrade_price' => 3, 'active' => 4, 'bottle' => 5, 'cap' => 6, 'capsule' => 7, 'label' => 8, 'order_id' => 9, 'qty' => 10, 'id' => 11, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('products');
        $this->setPhpName('Products');
        $this->setClassName('\\Products');
        $this->setPackage('');
        $this->setUseIdGenerator(true);
        // columns
        $this->addColumn('PRODUCT', 'Product', 'LONGVARCHAR', true, null, null);
        $this->addColumn('SKU', 'Sku', 'VARCHAR', true, 255, null);
        $this->addColumn('PRICE', 'Price', 'VARCHAR', true, 50, null);
        $this->addColumn('UPGRADE_PRICE', 'UpgradePrice', 'VARCHAR', true, 50, null);
        $this->addColumn('ACTIVE', 'Active', 'INTEGER', true, null, null);
        $this->addColumn('BOTTLE', 'Bottle', 'LONGVARCHAR', true, null, null);
        $this->addColumn('CAP', 'Cap', 'LONGVARCHAR', true, null, null);
        $this->addColumn('CAPSULE', 'Capsule', 'LONGVARCHAR', true, null, null);
        $this->addColumn('LABEL', 'Label', 'LONGVARCHAR', true, null, null);
        $this->addForeignKey('ORDER_ID', 'OrderId', 'INTEGER', 'orders', 'ID', true, null, null);
        $this->addColumn('QTY', 'Qty', 'VARCHAR', true, 50, null);
        $this->addPrimaryKey('ID', 'Id', 'INTEGER', true, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Order', '\\Orders', RelationMap::MANY_TO_ONE, array('order_id' => 'id', ), null, null);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_STUDLYPHPNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 11 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return (string) $row[TableMap::TYPE_NUM == $indexType ? 11 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_STUDLYPHPNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 11 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? ProductsTableMap::CLASS_DEFAULT : ProductsTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_STUDLYPHPNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Products object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = ProductsTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = ProductsTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + ProductsTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = ProductsTableMap::OM_CLASS;
            /** @var Products $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            ProductsTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = ProductsTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = ProductsTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Products $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                ProductsTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(ProductsTableMap::COL_PRODUCT);
            $criteria->addSelectColumn(ProductsTableMap::COL_SKU);
            $criteria->addSelectColumn(ProductsTableMap::COL_PRICE);
            $criteria->addSelectColumn(ProductsTableMap::COL_UPGRADE_PRICE);
            $criteria->addSelectColumn(ProductsTableMap::COL_ACTIVE);
            $criteria->addSelectColumn(ProductsTableMap::COL_BOTTLE);
            $criteria->addSelectColumn(ProductsTableMap::COL_CAP);
            $criteria->addSelectColumn(ProductsTableMap::COL_CAPSULE);
            $criteria->addSelectColumn(ProductsTableMap::COL_LABEL);
            $criteria->addSelectColumn(ProductsTableMap::COL_ORDER_ID);
            $criteria->addSelectColumn(ProductsTableMap::COL_QTY);
            $criteria->addSelectColumn(ProductsTableMap::COL_ID);
        } else {
            $criteria->addSelectColumn($alias . '.PRODUCT');
            $criteria->addSelectColumn($alias . '.SKU');
            $criteria->addSelectColumn($alias . '.PRICE');
            $criteria->addSelectColumn($alias . '.UPGRADE_PRICE');
            $criteria->addSelectColumn($alias . '.ACTIVE');
            $criteria->addSelectColumn($alias . '.BOTTLE');
            $criteria->addSelectColumn($alias . '.CAP');
            $criteria->addSelectColumn($alias . '.CAPSULE');
            $criteria->addSelectColumn($alias . '.LABEL');
            $criteria->addSelectColumn($alias . '.ORDER_ID');
            $criteria->addSelectColumn($alias . '.QTY');
            $criteria->addSelectColumn($alias . '.ID');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(ProductsTableMap::DATABASE_NAME)->getTable(ProductsTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(ProductsTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(ProductsTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new ProductsTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Products or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Products object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ProductsTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Products) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(ProductsTableMap::DATABASE_NAME);
            $criteria->add(ProductsTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = ProductsQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            ProductsTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                ProductsTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the products table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return ProductsQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Products or Criteria object.
     *
     * @param mixed               $criteria Criteria or Products object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ProductsTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Products object
        }

        if ($criteria->containsKey(ProductsTableMap::COL_ID) && $criteria->keyContainsValue(ProductsTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.ProductsTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = ProductsQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // ProductsTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
ProductsTableMap::buildTableMap();
