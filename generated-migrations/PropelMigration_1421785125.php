<?php

/**
 * Data object containing the SQL and PHP code to migrate the database
 * up to version 1421785125.
 * Generated on 2015-01-20 12:18:45 by bryansiegel
 */
class PropelMigration_1421785125
{
    public $comment = '';

    public function preUp($manager)
    {
        // add the pre-migration code here
    }

    public function postUp($manager)
    {
        // add the post-migration code here
    }

    public function preDown($manager)
    {
        // add the pre-migration code here
    }

    public function postDown($manager)
    {
        // add the post-migration code here
    }

    /**
     * Get the SQL statements for the Up migration
     *
     * @return array list of the SQL strings to execute for the Up migration
     *               the keys being the datasources
     */
    public function getUpSQL()
    {
        return array (
  'cart' => '
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

ALTER TABLE `orders`

  CHANGE `order_number` `order_number` INTEGER NOT NULL;

ALTER TABLE `products`

  CHANGE `order_id` `order_id` INTEGER NOT NULL,

  CHANGE `product` `product` TEXT NOT NULL,

  CHANGE `sku` `sku` VARCHAR(255) NOT NULL,

  CHANGE `price` `price` VARCHAR(50) NOT NULL,

  CHANGE `active` `active` INTEGER NOT NULL,

  CHANGE `bottle` `bottle` TEXT NOT NULL,

  CHANGE `cap` `cap` TEXT NOT NULL,

  CHANGE `qty` `qty` VARCHAR(50) NOT NULL,

  CHANGE `capsule` `capsule` TEXT NOT NULL;

CREATE INDEX `products_fi_eade59` ON `products` (`order_id`);

ALTER TABLE `products` ADD CONSTRAINT `products_fk_eade59`
    FOREIGN KEY (`order_id`)
    REFERENCES `orders` (`id`);

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
',
);
    }

    /**
     * Get the SQL statements for the Down migration
     *
     * @return array list of the SQL strings to execute for the Down migration
     *               the keys being the datasources
     */
    public function getDownSQL()
    {
        return array (
  'cart' => '
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

ALTER TABLE `orders`

  CHANGE `order_number` `order_number` INTEGER;

ALTER TABLE `products` DROP FOREIGN KEY `products_fk_eade59`;

DROP INDEX `products_fi_eade59` ON `products`;

ALTER TABLE `products`

  CHANGE `order_id` `order_id` INTEGER,

  CHANGE `product` `product` TEXT,

  CHANGE `sku` `sku` VARCHAR(255),

  CHANGE `price` `price` VARCHAR(50),

  CHANGE `active` `active` INTEGER,

  CHANGE `bottle` `bottle` TEXT,

  CHANGE `cap` `cap` TEXT,

  CHANGE `qty` `qty` VARCHAR(50),

  CHANGE `capsule` `capsule` VARCHAR(255);

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
',
);
    }

}