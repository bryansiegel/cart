<?php
// setup the autoloading
require_once 'vendor/autoload.php';

// setup Propel
require_once 'generated-conf/config.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php include('includes/header.php'); ?>
</head>

<body>

<div class="container">

    <?php include('includes/nav.php'); ?>

    <!-- Jumbotron -->
    <div class="jumbotron">
        <h1>Prepacked Bottles</h1>
        <p class="lead">Here is where we put some sales pitch or maybe what it does. </p>
        <a class="btn btn-large btn-success" href="cart_new.php">Create A Prepacked Order Today</a>
        <hr>

    </div>
    <!-- Example row of columns -->
        <div class="row-fluid">
            <div class="span2">
                <h2>Bottles</h2>
                <p><img src="assets/img/bordeaux.jpg" alt=""/></p>
            </div>

            <div class="span2">
                <h1 style="font-size: 150px; margin-top:150px;">+</h1>
            </div>
            <div class="span2">
                <h2>Product</h2>
                <p><img src="assets/img/vinegars.jpg" alt=""/></p>
            </div>
            <div class="span2">
                <h1 style="font-size: 150px; margin-top:150px;">+</h1>
            </div>
            <div class="span2">
                <h2>Cap</h2>
                <p><img src="assets/img/vinegars.jpg" alt=""/></p>
            </div>
        </div>

    <hr />

    <div class="row-fluid">
        <div class="span6">
            <h1 style="font-size: 150px;">=</h1>
        </div>

        <div class="span6">
            <h2>Prepacked Product</h2>
        </div>
        </div>

    <?php include('includes/footer.php'); ?>

</body>
</html>
