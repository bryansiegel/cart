<?php

if (!isset($_SESSION)) session_start();

// setup the autoloading
require_once 'vendor/autoload.php';

// setup Propel orm
require_once 'generated-conf/config.php';


//terms of service
$terms = $_REQUEST['terms'];

//if they don't agree throw an error
if(isset($terms) && $terms === "on") {



    function died($error) {
    //html
        echo '' . $error;
        //end html

        echo "We are very sorry, but there were error(s) found with the form you submitted. ";
        echo "These errors appear below.<br /><br />";
        echo $error."<br /><br />";
        echo "Please go back and fix these errors.<br /><br />";
        die();
    }

    // validation expected data exists
    if(
        //billing
    !isset($_REQUEST['billing_name']) ||
    !isset($_REQUEST['billing_street_address']) ||
    !isset($_REQUEST['billing_city']) ||
    !isset($_REQUEST['billing_state']) ||
    !isset($_REQUEST['billing_zip']) ||
    !isset($_REQUEST['billing_telephone']) ||
    !isset($_REQUEST['billing_email']) ||

        //shipping
    !isset($_REQUEST['shipping_name']) ||
    !isset($_REQUEST['shipping_street_address']) ||
    !isset($_REQUEST['shipping_city']) ||
    !isset($_REQUEST['shipping_state']) ||
    !isset($_REQUEST['shipping_zip']) ||
    !isset($_REQUEST['shipping_telephone']) ||
    !isset($_REQUEST['shipping_email'])

    ) {
    //error
        died('We are sorry, but there appears to be a problem with the form you submitted.');
    }

    if(!isset($_REQUEST['product']))
    {
        died("You must enter a product");
    }

    //billing
    $billing_name = $_REQUEST['billing_name'];
    $billing_street_address = $_REQUEST['billing_street_address'];
    $billing_city = $_REQUEST['billing_city'];
    $billing_state = $_REQUEST['billing_state'];
    $billing_zip = $_REQUEST['billing_zip'];
    $billing_telephone = $_REQUEST['billing_telephone'];
    $billing_email = $_REQUEST['billing_email'];
    //shipping
    $shipping_name = $_REQUEST['shipping_name'];
    $shipping_street_address = $_REQUEST['shipping_street_address'];
    $shipping_city = $_REQUEST['shipping_city'];
    $shipping_state = $_REQUEST['shipping_state'];
    $shipping_zip = $_REQUEST['shipping_zip'];
    $shipping_telephone = $_REQUEST['shipping_telephone'];
    $shipping_email = $_REQUEST['shipping_email'];
    $shipping_method = $_REQUEST['shipping_method'];
    //session
    $session_id = $_REQUEST['session_id'];
    //notes
    $notes = $_REQUEST['notes'];
    //error message
    $error_message = "";

/*
 * Error Messages
 */

    //billing name
    if(strlen($billing_name) < 2) {
        $error_message .= 'The Billilng Name Cannot Be Empty.<br />';
    }
    //error
    if(strlen($error_message) > 0) {
        died($error_message);
    }


    //    $email_to = "customerservice@cibaria-intl.com";
    $email_to = $shipping_email . ",". "bsiegel@cibaria-intl.com";
    $email_subject = "PrePacked Bottle Order Form";

//email
include('includes/email.php');
    // create email headers
    $headers = 'From: '.$shipping_email."\r\n".
        'Reply-To: '.$shipping_email."\r\n" .
        "Content-Type: text/html; charset=ISO-8859-1\r\n" .
        'X-Mailer: PHP/' . phpversion();
    @mail($email_to, $email_subject, $email_message, $headers);
    ?>

    <!-- include your own success html here -->
    <p>The form has been submitted. Yay</p>
    <?php session_destroy(); ?>


<?php

} else{
    echo "You must agree to the terms of service";
    die();
}

?>
