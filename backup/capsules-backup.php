/*
Capsules
@return image/input
*/


//oils
var oils = "<img src='assets/img/caps/green_capsule.jpg'  /><input type='radio' name='capsule' id='green' value='Green Capsule'><label for='green'>Green</label><img src='assets/img/caps/green_gold_capsule.jpg' /><input type='radio' name='capsule'  id='green_gold' value='Green/Gold Capsule'> <label for='green_gold'>Green/Gold</label>";

var all_capsules = "<div class='newpurple-round-button'><div class='newpurple-round-button-circle'><span><img src='assets/img/caps/green_capsule.jpg' height='105px' /><input type='radio' name='capsule' id='all_capsules_green' value='Green Capsule'><label for='all_capsules_green'>Green Capsule</label></span></div></div><div style='margin-top:2px;margin-bottom:2px;'></div><div class='newpurple-round-button'><div class='newpurple-round-button-circle'><span><img src='assets/img/caps/green_gold_capsule.jpg' height='105px'/><input type='radio' name='capsule'  id='all_capsules_green_gold' value='Green/Gold Capsule'><label for='all_capsules_green_gold'>Green/Gold</label></span></div></div><div style='margin-top:2px;margin-bottom:2px;'></div><div class='newpurple-round-button'><div class='newpurple-round-button-circle'><span><img src='assets/img/caps/black_capsule.jpg' height='105px' /><div style='margin-top:2px;margin-bottom:2px;'></div><input type='radio' name='capsule' id='all_capsules_black' value='Black Capsule'><label for='all_capsules_black'>Black</label></span></div></div><div style='margin-top:2px;margin-bottom:2px;'></div><div class='newpurple-round-button'><div class='newpurple-round-button-circle'><span><img src='assets/img/caps/black_gold_capsule.jpg' height='105px'/><div style='margin-top:2px;margin-bottom:2px;'></div><input type='radio' name='capsule' id='all_capsules_black_gold' value='Black/Gold Capsule'><label for='all_capsules_black_gold'>Black/Gold</label></span></div></div><div style='margin-top:2px;margin-bottom:2px;'></div><div class='newpurple-round-button'><div class='newpurple-round-button-circle'><span><img src='assets/img/sleeves/black_sleeve.jpg' height='105px'/><div style='margin-top:2px;margin-bottom:2px;'></div><input type='radio' name='capsule' value='Black Sleeve' id='all_capsules_black_sleeve'><label for='all_capsules_black_sleeve'>Black Sleeve</label></span></div></div>";

var all_sleeves = "<img src='assets/img/sleeves/black_sleeve.jpg' /><input type='radio' name='capsule' id='black_all_sleeves' value='Black Sleeve'><label for='black_all_sleeves'>Black Sleeve</label><br /><img src='assets/img/sleeves/clear_sleeve.jpg'><input type='radio' name='capsule', id='clear_all_sleeves' value='Clear Sleeve'><label for='clear_all_sleeves'>Clear Sleeve</label>";

var all_capsules_sleeves = "<img src='assets/img/caps/green_capsule.jpg'  /><input type='radio' name='capsule' id='all_capsules_sleeves_green' value='Green Capsule'><label for='all_capsules_sleeves_green'>Green</label><img src='assets/img/caps/green_gold_capsule.jpg' /><input type='radio' name='capsule'  id='all_capsules_sleeves_green_gold' value='Green/Gold Capsule'> <label for='all_capsules_sleeves_green_gold'>Green/Gold</label><br /><img src='assets/img/caps/black_capsule.jpg' /><input type='radio' name='capsule' id='all_capsules_sleeves_black' value='Black Capsule'><label for='all_capsules_sleeves_black'>Black</label><br /><img src='assets/img/caps/black_gold_capsule.jpg' /><input type='radio' name='capsule' id='all_capsules_sleeves_black_gold' value='Black/Gold Capsule'><label for='all_capsules_sleeves_black_gold'>Black/Gold</label><br /><br /><img src='assets/img/sleeves/black_sleeve.jpg' /><input type='radio' name='capsule' value='Black Sleeve' id='all_capsules_sleeves_black_sleeve'><label for='all_capsules_sleeves_black_sleeve'>Black Sleeve</label><br /><img src='assets/img/sleeves/clear_sleeve.jpg' /><input type='radio' name='capsule' value='Clear Sleeve' id='all_capsules_sleeves_clear_sleeve'><label for='all_capsules_sleeves_clear_sleeve'>Clear Sleeve</label>";

//vinegars
var vinegars = "<img src='assets/img/caps/black_capsule.jpg' /><input type='hidden' name ='capsule' id='black' value='Black Capsule' />";

//clear sleeve
var clear_sleave = "<img src='assets/img/sleeves/clear_sleeve.jpg' /><input type='hidden' name ='capsule' id='clear_sleeve' value='Clear Sleeve' />";





/*
Bordolese Bottles
*/

//oils
<?php foreach($oils as $oil) { ?>
    if(document.getElementById('BORDOLESE - 12/375 ml').checked == true && document.getElementById('<?php echo $oil; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//infused
<?php foreach($infused as $infuse) { ?>
    if(document.getElementById('BORDOLESE - 12/375 ml').checked == true && document.getElementById('<?php echo $infuse; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//organics
<?php foreach($organics  as $organic) { ?>
    if(document.getElementById('BORDOLESE - 12/375 ml').checked == true && document.getElementById('<?php echo $organic; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//other oils
<?php foreach($other_oils  as $other_oil) { ?>
    if(document.getElementById('BORDOLESE - 12/375 ml').checked == true && document.getElementById('<?php echo $other_oil; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//naturally flavored oils
<?php foreach($naturally_flavored_oils  as $naturally_flavored_oil) { ?>
    if(document.getElementById('BORDOLESE - 12/375 ml').checked == true && document.getElementById('<?php echo $naturally_flavored_oil; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//fused oils
<?php foreach($fused_oils  as $fused_oil) { ?>
    if(document.getElementById('BORDOLESE - 12/375 ml').checked == true && document.getElementById('<?php echo $fused_oil; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>


//vinegars
<?php foreach($vinegars  as $vinegar) { ?>
    if(document.getElementById('BORDOLESE - 12/375 ml').checked == true && document.getElementById('<?php echo $vinegar; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//flavored balsamic vinegars
<?php foreach($flavored_balsamic_vinegars  as $flavored_balsamic_vinegar) { ?>
    if(document.getElementById('BORDOLESE - 12/375 ml').checked == true && document.getElementById('<?php echo $flavored_balsamic_vinegar; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

/*
Bordeaux Bottles
*/
//375 bordeaux
//oils
<?php foreach($oils as $oil) { ?>
    if(document.getElementById('BORDEAUX - 12/375 ml').checked == true && document.getElementById('<?php echo $oil; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//infused
<?php foreach($infused as $infuse) { ?>
    if(document.getElementById('BORDEAUX - 12/375 ml').checked == true && document.getElementById('<?php echo $infuse; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//organics
<?php foreach($organics  as $organic) { ?>
    if(document.getElementById('BORDEAUX - 12/375 ml').checked == true && document.getElementById('<?php echo $organic; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//other oils
<?php foreach($other_oils  as $other_oil) { ?>
    if(document.getElementById('BORDEAUX - 12/375 ml').checked == true && document.getElementById('<?php echo $other_oil; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//naturally flavored oils
<?php foreach($naturally_flavored_oils  as $naturally_flavored_oil) { ?>
    if(document.getElementById('BORDEAUX - 12/375 ml').checked == true && document.getElementById('<?php echo $naturally_flavored_oil; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//fused oils
<?php foreach($fused_oils  as $fused_oil) { ?>
    if(document.getElementById('BORDEAUX - 12/375 ml').checked == true && document.getElementById('<?php echo $fused_oil; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>


//vinegars
<?php foreach($vinegars  as $vinegar) { ?>
    if(document.getElementById('BORDEAUX - 12/375 ml').checked == true && document.getElementById('<?php echo $vinegar; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//flavored balsamic vinegars
<?php foreach($flavored_balsamic_vinegars  as $flavored_balsamic_vinegar) { ?>
    if(document.getElementById('BORDEAUX - 12/375 ml').checked == true && document.getElementById('<?php echo $flavored_balsamic_vinegar; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//500 Bordeaux
//oils
<?php foreach($oils as $oil) { ?>
    if(document.getElementById('BORDEAUX - 12/500 ml').checked == true && document.getElementById('<?php echo $oil; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//infused
<?php foreach($infused as $infuse) { ?>
    if(document.getElementById('BORDEAUX - 12/500 ml').checked == true && document.getElementById('<?php echo $infuse; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//organics
<?php foreach($organics  as $organic) { ?>
    if(document.getElementById('BORDEAUX - 12/500 ml').checked == true && document.getElementById('<?php echo $organic; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//other oils
<?php foreach($other_oils  as $other_oil) { ?>
    if(document.getElementById('BORDEAUX - 12/500 ml').checked == true && document.getElementById('<?php echo $other_oil; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//naturally flavored oils
<?php foreach($naturally_flavored_oils  as $naturally_flavored_oil) { ?>
    if(document.getElementById('BORDEAUX - 12/500 ml').checked == true && document.getElementById('<?php echo $naturally_flavored_oil; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//fused oils
<?php foreach($fused_oils  as $fused_oil) { ?>
    if(document.getElementById('BORDEAUX - 12/500 ml').checked == true && document.getElementById('<?php echo $fused_oil; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>


//vinegars
<?php foreach($vinegars  as $vinegar) { ?>
    if(document.getElementById('BORDEAUX - 12/500 ml').checked == true && document.getElementById('<?php echo $vinegar; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//flavored balsamic vinegars
<?php foreach($flavored_balsamic_vinegars  as $flavored_balsamic_vinegar) { ?>
    if(document.getElementById('BORDEAUX - 12/500 ml').checked == true && document.getElementById('<?php echo $flavored_balsamic_vinegar; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//750 Bordeaux
//oils
<?php foreach($oils as $oil) { ?>
    if(document.getElementById('BORDEAUX - 12/750 ml').checked == true && document.getElementById('<?php echo $oil; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//infused
<?php foreach($infused as $infuse) { ?>
    if(document.getElementById('BORDEAUX - 12/750 ml').checked == true && document.getElementById('<?php echo $infuse; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//organics
<?php foreach($organics  as $organic) { ?>
    if(document.getElementById('BORDEAUX - 12/750 ml').checked == true && document.getElementById('<?php echo $organic; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//other oils
<?php foreach($other_oils  as $other_oil) { ?>
    if(document.getElementById('BORDEAUX - 12/750 ml').checked == true && document.getElementById('<?php echo $other_oil; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//naturally flavored oils
<?php foreach($naturally_flavored_oils  as $naturally_flavored_oil) { ?>
    if(document.getElementById('BORDEAUX - 12/750 ml').checked == true && document.getElementById('<?php echo $naturally_flavored_oil; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//fused oils
<?php foreach($fused_oils  as $fused_oil) { ?>
    if(document.getElementById('BORDEAUX - 12/750 ml').checked == true && document.getElementById('<?php echo $fused_oil; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>


//vinegars
<?php foreach($vinegars  as $vinegar) { ?>
    if(document.getElementById('BORDEAUX - 12/750 ml').checked == true && document.getElementById('<?php echo $vinegar; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//flavored balsamic vinegars
<?php foreach($flavored_balsamic_vinegars  as $flavored_balsamic_vinegar) { ?>
    if(document.getElementById('BORDEAUX - 12/750 ml').checked == true && document.getElementById('<?php echo $flavored_balsamic_vinegar; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//100 antique Dorica
//oils
<?php foreach($oils as $oil) { ?>
    if(document.getElementById('DORICA - 24/100 ML').checked == true && document.getElementById('<?php echo $oil; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = clear_sleave;
    }
<?php } ?>

//infused
<?php foreach($infused as $infuse) { ?>
    if(document.getElementById('DORICA - 24/100 ML').checked == true && document.getElementById('<?php echo $infuse; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = clear_sleave;
    }
<?php } ?>

//organics
<?php foreach($organics  as $organic) { ?>
    if(document.getElementById('DORICA - 24/100 ML').checked == true && document.getElementById('<?php echo $organic; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = clear_sleave;
    }
<?php } ?>

//other oils
<?php foreach($other_oils  as $other_oil) { ?>
    if(document.getElementById('DORICA - 24/100 ML').checked == true && document.getElementById('<?php echo $other_oil; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = clear_sleave;
    }
<?php } ?>

//naturally flavored oils
<?php foreach($naturally_flavored_oils  as $naturally_flavored_oil) { ?>
    if(document.getElementById('DORICA - 24/100 ML').checked == true && document.getElementById('<?php echo $naturally_flavored_oil; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = clear_sleave;
    }
<?php } ?>

//fused oils
<?php foreach($fused_oils  as $fused_oil) { ?>
    if(document.getElementById('DORICA - 24/100 ML').checked == true && document.getElementById('<?php echo $fused_oil; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = clear_sleave;
    }
<?php } ?>


//vinegars
<?php foreach($vinegars  as $vinegar) { ?>
    if(document.getElementById('DORICA - 24/100 ML').checked == true && document.getElementById('<?php echo $vinegar; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = clear_sleave;
    }
<?php } ?>

//flavored balsamic vinegars
<?php foreach($flavored_balsamic_vinegars  as $flavored_balsamic_vinegar) { ?>
    if(document.getElementById('DORICA - 24/100 ML').checked == true && document.getElementById('<?php echo $flavored_balsamic_vinegar; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = clear_sleave;
    }
<?php } ?>


//250 antique dorica
//oils
<?php foreach($oils as $oil) { ?>
    if(document.getElementById('DORICA - 12/250 ML').checked == true && document.getElementById('<?php echo $oil; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//infused
<?php foreach($infused as $infuse) { ?>
    if(document.getElementById('DORICA - 12/250 ML').checked == true && document.getElementById('<?php echo $infuse; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//organics
<?php foreach($organics  as $organic) { ?>
    if(document.getElementById('DORICA - 12/250 ML').checked == true && document.getElementById('<?php echo $organic; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//other oils
<?php foreach($other_oils  as $other_oil) { ?>
    if(document.getElementById('DORICA - 12/250 ML').checked == true && document.getElementById('<?php echo $other_oil; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//naturally flavored oils
<?php foreach($naturally_flavored_oils  as $naturally_flavored_oil) { ?>
    if(document.getElementById('DORICA - 12/250 ML').checked == true && document.getElementById('<?php echo $naturally_flavored_oil; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//fused oils
<?php foreach($fused_oils  as $fused_oil) { ?>
    if(document.getElementById('DORICA - 12/250 ML').checked == true && document.getElementById('<?php echo $fused_oil; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>


//vinegars
<?php foreach($vinegars  as $vinegar) { ?>
    if(document.getElementById('DORICA - 12/250 ML').checked == true && document.getElementById('<?php echo $vinegar; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//flavored balsamic vinegars
<?php foreach($flavored_balsamic_vinegars  as $flavored_balsamic_vinegar) { ?>
    if(document.getElementById('DORICA - 12/250 ML').checked == true && document.getElementById('<?php echo $flavored_balsamic_vinegar; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>


//500 antique dorica
//oils
<?php foreach($oils as $oil) { ?>
    if(document.getElementById('DORICA - 12/500 ML').checked == true && document.getElementById('<?php echo $oil; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//infused
<?php foreach($infused as $infuse) { ?>
    if(document.getElementById('DORICA - 12/500 ML').checked == true && document.getElementById('<?php echo $infuse; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//organics
<?php foreach($organics  as $organic) { ?>
    if(document.getElementById('DORICA - 12/500 ML').checked == true && document.getElementById('<?php echo $organic; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//other oils
<?php foreach($other_oils  as $other_oil) { ?>
    if(document.getElementById('DORICA - 12/500 ML').checked == true && document.getElementById('<?php echo $other_oil; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//naturally flavored oils
<?php foreach($naturally_flavored_oils  as $naturally_flavored_oil) { ?>
    if(document.getElementById('DORICA - 12/500 ML').checked == true && document.getElementById('<?php echo $naturally_flavored_oil; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//fused oils
<?php foreach($fused_oils  as $fused_oil) { ?>
    if(document.getElementById('DORICA - 12/500 ML').checked == true && document.getElementById('<?php echo $fused_oil; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>


//vinegars
<?php foreach($vinegars  as $vinegar) { ?>
    if(document.getElementById('DORICA - 12/500 ML').checked == true && document.getElementById('<?php echo $vinegar; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//flavored balsamic vinegars
<?php foreach($flavored_balsamic_vinegars  as $flavored_balsamic_vinegar) { ?>
    if(document.getElementById('DORICA - 12/500 ML').checked == true && document.getElementById('<?php echo $flavored_balsamic_vinegar; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>


//250 clear dorica
//oils
<?php foreach($oils as $oil) { ?>
    if(document.getElementById('CLEAR DORICA - 12/250 ML').checked == true && document.getElementById('<?php echo $oil; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//infused
<?php foreach($infused as $infuse) { ?>
    if(document.getElementById('CLEAR DORICA - 12/250 ML').checked == true && document.getElementById('<?php echo $infuse; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//organics
<?php foreach($organics  as $organic) { ?>
    if(document.getElementById('CLEAR DORICA - 12/250 ML').checked == true && document.getElementById('<?php echo $organic; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//other oils
<?php foreach($other_oils  as $other_oil) { ?>
    if(document.getElementById('CLEAR DORICA - 12/250 ML').checked == true && document.getElementById('<?php echo $other_oil; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//naturally flavored oils
<?php foreach($naturally_flavored_oils  as $naturally_flavored_oil) { ?>
    if(document.getElementById('CLEAR DORICA - 12/250 ML').checked == true && document.getElementById('<?php echo $naturally_flavored_oil; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//fused oils
<?php foreach($fused_oils  as $fused_oil) { ?>
    if(document.getElementById('CLEAR DORICA - 12/250 ML').checked == true && document.getElementById('<?php echo $fused_oil; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>


//vinegars
<?php foreach($vinegars  as $vinegar) { ?>
    if(document.getElementById('CLEAR DORICA - 12/250 ML').checked == true && document.getElementById('<?php echo $vinegar; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//flavored balsamic vinegars
<?php foreach($flavored_balsamic_vinegars  as $flavored_balsamic_vinegar) { ?>
    if(document.getElementById('CLEAR DORICA - 12/250 ML').checked == true && document.getElementById('<?php echo $flavored_balsamic_vinegar; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>


//500 clear dorica
//oils
<?php foreach($oils as $oil) { ?>
    if(document.getElementById('CLEAR DORICA - 12/500 ML').checked == true && document.getElementById('<?php echo $oil; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//infused
<?php foreach($infused as $infuse) { ?>
    if(document.getElementById('CLEAR DORICA - 12/500 ML').checked == true && document.getElementById('<?php echo $infuse; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//organics
<?php foreach($organics  as $organic) { ?>
    if(document.getElementById('CLEAR DORICA - 12/500 ML').checked == true && document.getElementById('<?php echo $organic; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//other oils
<?php foreach($other_oils  as $other_oil) { ?>
    if(document.getElementById('CLEAR DORICA - 12/500 ML').checked == true && document.getElementById('<?php echo $other_oil; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//naturally flavored oils
<?php foreach($naturally_flavored_oils  as $naturally_flavored_oil) { ?>
    if(document.getElementById('CLEAR DORICA - 12/500 ML').checked == true && document.getElementById('<?php echo $naturally_flavored_oil; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//fused oils
<?php foreach($fused_oils  as $fused_oil) { ?>
    if(document.getElementById('CLEAR DORICA - 12/500 ML').checked == true && document.getElementById('<?php echo $fused_oil; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>


//vinegars
<?php foreach($vinegars  as $vinegar) { ?>
    if(document.getElementById('CLEAR DORICA - 12/500 ML').checked == true && document.getElementById('<?php echo $vinegar; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//flavored balsamic vinegars
<?php foreach($flavored_balsamic_vinegars  as $flavored_balsamic_vinegar) { ?>
    if(document.getElementById('CLEAR DORICA - 12/500 ML').checked == true && document.getElementById('<?php echo $flavored_balsamic_vinegar; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//clear marasca bottles
//oils
<?php foreach($oils as $oil) { ?>
    if(document.getElementById('MARASCA - 24/100 ml').checked == true && document.getElementById('<?php echo $oil; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//infused
<?php foreach($infused as $infuse) { ?>
    if(document.getElementById('MARASCA - 24/100 ml').checked == true && document.getElementById('<?php echo $infuse; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//organics
<?php foreach($organics  as $organic) { ?>
    if(document.getElementById('MARASCA - 24/100 ml').checked == true && document.getElementById('<?php echo $organic; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//other oils
<?php foreach($other_oils  as $other_oil) { ?>
    if(document.getElementById('MARASCA - 24/100 ml').checked == true && document.getElementById('<?php echo $other_oil; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//naturally flavored oils
<?php foreach($naturally_flavored_oils  as $naturally_flavored_oil) { ?>
    if(document.getElementById('MARASCA - 24/100 ml').checked == true && document.getElementById('<?php echo $naturally_flavored_oil; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//fused oils
<?php foreach($fused_oils  as $fused_oil) { ?>
    if(document.getElementById('MARASCA - 24/100 ml').checked == true && document.getElementById('<?php echo $fused_oil; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>


//vinegars
<?php foreach($vinegars  as $vinegar) { ?>
    if(document.getElementById('MARASCA - 24/100 ml').checked == true && document.getElementById('<?php echo $vinegar; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//flavored balsamic vinegars
<?php foreach($flavored_balsamic_vinegars  as $flavored_balsamic_vinegar) { ?>
    if(document.getElementById('MARASCA - 24/100 ml').checked == true && document.getElementById('<?php echo $flavored_balsamic_vinegar; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//250 clear marasca
//oils
<?php foreach($oils as $oil) { ?>
    if(document.getElementById('MARASCA - 12/250 ml').checked == true && document.getElementById('<?php echo $oil; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//infused
<?php foreach($infused as $infuse) { ?>
    if(document.getElementById('MARASCA - 12/250 ml').checked == true && document.getElementById('<?php echo $infuse; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//organics
<?php foreach($organics  as $organic) { ?>
    if(document.getElementById('MARASCA - 12/250 ml').checked == true && document.getElementById('<?php echo $organic; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//other oils
<?php foreach($other_oils  as $other_oil) { ?>
    if(document.getElementById('MARASCA - 12/250 ml').checked == true && document.getElementById('<?php echo $other_oil; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//naturally flavored oils
<?php foreach($naturally_flavored_oils  as $naturally_flavored_oil) { ?>
    if(document.getElementById('MARASCA - 12/250 ml').checked == true && document.getElementById('<?php echo $naturally_flavored_oil; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//fused oils
<?php foreach($fused_oils  as $fused_oil) { ?>
    if(document.getElementById('MARASCA - 12/250 ml').checked == true && document.getElementById('<?php echo $fused_oil; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>


//vinegars
<?php foreach($vinegars  as $vinegar) { ?>
    if(document.getElementById('MARASCA - 12/250 ml').checked == true && document.getElementById('<?php echo $vinegar; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//flavored balsamic vinegars
<?php foreach($flavored_balsamic_vinegars  as $flavored_balsamic_vinegar) { ?>
    if(document.getElementById('MARASCA - 12/250 ml').checked == true && document.getElementById('<?php echo $flavored_balsamic_vinegar; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//500 clear marasca
//oils
<?php foreach($oils as $oil) { ?>
    if(document.getElementById('MARASCA - 12/500 ml').checked == true && document.getElementById('<?php echo $oil; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//infused
<?php foreach($infused as $infuse) { ?>
    if(document.getElementById('MARASCA - 12/500 ml').checked == true && document.getElementById('<?php echo $infuse; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//organics
<?php foreach($organics  as $organic) { ?>
    if(document.getElementById('MARASCA - 12/500 ml').checked == true && document.getElementById('<?php echo $organic; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//other oils
<?php foreach($other_oils  as $other_oil) { ?>
    if(document.getElementById('MARASCA - 12/500 ml').checked == true && document.getElementById('<?php echo $other_oil; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//naturally flavored oils
<?php foreach($naturally_flavored_oils  as $naturally_flavored_oil) { ?>
    if(document.getElementById('MARASCA - 12/500 ml').checked == true && document.getElementById('<?php echo $naturally_flavored_oil; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//fused oils
<?php foreach($fused_oils  as $fused_oil) { ?>
    if(document.getElementById('MARASCA - 12/500 ml').checked == true && document.getElementById('<?php echo $fused_oil; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>


//vinegars
<?php foreach($vinegars  as $vinegar) { ?>
    if(document.getElementById('MARASCA - 12/500 ml').checked == true && document.getElementById('<?php echo $vinegar; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//flavored balsamic vinegars
<?php foreach($flavored_balsamic_vinegars  as $flavored_balsamic_vinegar) { ?>
    if(document.getElementById('MARASCA - 12/500 ml').checked == true && document.getElementById('<?php echo $flavored_balsamic_vinegar; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>


//500 green marasca
//oils
<?php foreach($oils as $oil) { ?>
    if(document.getElementById('GREEN MARASCA - 12/500 ml').checked == true && document.getElementById('<?php echo $oil; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//infused
<?php foreach($infused as $infuse) { ?>
    if(document.getElementById('GREEN MARASCA - 12/500 ml').checked == true && document.getElementById('<?php echo $infuse; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//organics
<?php foreach($organics  as $organic) { ?>
    if(document.getElementById('GREEN MARASCA - 12/500 ml').checked == true && document.getElementById('<?php echo $organic; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//other oils
<?php foreach($other_oils  as $other_oil) { ?>
    if(document.getElementById('GREEN MARASCA - 12/500 ml').checked == true && document.getElementById('<?php echo $other_oil; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//naturally flavored oils
<?php foreach($naturally_flavored_oils  as $naturally_flavored_oil) { ?>
    if(document.getElementById('GREEN MARASCA - 12/500 ml').checked == true && document.getElementById('<?php echo $naturally_flavored_oil; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//fused oils
<?php foreach($fused_oils  as $fused_oil) { ?>
    if(document.getElementById('GREEN MARASCA - 12/500 ml').checked == true && document.getElementById('<?php echo $fused_oil; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>


//vinegars
<?php foreach($vinegars  as $vinegar) { ?>
    if(document.getElementById('GREEN MARASCA - 12/500 ml').checked == true && document.getElementById('<?php echo $vinegar; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//flavored balsamic vinegars
<?php foreach($flavored_balsamic_vinegars  as $flavored_balsamic_vinegar) { ?>
    if(document.getElementById('GREEN MARASCA - 12/500 ml').checked == true && document.getElementById('<?php echo $flavored_balsamic_vinegar; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//750 green marasca
//oils
<?php foreach($oils as $oil) { ?>
    if(document.getElementById('GREEN MARASCA - 12/750 ml').checked == true && document.getElementById('<?php echo $oil; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//infused
<?php foreach($infused as $infuse) { ?>
    if(document.getElementById('GREEN MARASCA - 12/750 ml').checked == true && document.getElementById('<?php echo $infuse; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//organics
<?php foreach($organics  as $organic) { ?>
    if(document.getElementById('GREEN MARASCA - 12/750 ml').checked == true && document.getElementById('<?php echo $organic; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//other oils
<?php foreach($other_oils  as $other_oil) { ?>
    if(document.getElementById('GREEN MARASCA - 12/750 ml').checked == true && document.getElementById('<?php echo $other_oil; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//naturally flavored oils
<?php foreach($naturally_flavored_oils  as $naturally_flavored_oil) { ?>
    if(document.getElementById('GREEN MARASCA - 12/750 ml').checked == true && document.getElementById('<?php echo $naturally_flavored_oil; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//fused oils
<?php foreach($fused_oils  as $fused_oil) { ?>
    if(document.getElementById('GREEN MARASCA - 12/750 ml').checked == true && document.getElementById('<?php echo $fused_oil; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>


//vinegars
<?php foreach($vinegars  as $vinegar) { ?>
    if(document.getElementById('GREEN MARASCA - 12/750 ml').checked == true && document.getElementById('<?php echo $vinegar; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//flavored balsamic vinegars
<?php foreach($flavored_balsamic_vinegars  as $flavored_balsamic_vinegar) { ?>
    if(document.getElementById('GREEN MARASCA - 12/750 ml').checked == true && document.getElementById('<?php echo $flavored_balsamic_vinegar; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//500 uvag marasca
//oils
<?php foreach($oils as $oil) { ?>
    if(document.getElementById('UVAG MARASCA - 12/500 ml').checked == true && document.getElementById('<?php echo $oil; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//infused
<?php foreach($infused as $infuse) { ?>
    if(document.getElementById('UVAG MARASCA - 12/500 ml').checked == true && document.getElementById('<?php echo $infuse; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//organics
<?php foreach($organics  as $organic) { ?>
    if(document.getElementById('UVAG MARASCA - 12/500 ml').checked == true && document.getElementById('<?php echo $organic; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//other oils
<?php foreach($other_oils  as $other_oil) { ?>
    if(document.getElementById('UVAG MARASCA - 12/500 ml').checked == true && document.getElementById('<?php echo $other_oil; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//naturally flavored oils
<?php foreach($naturally_flavored_oils  as $naturally_flavored_oil) { ?>
    if(document.getElementById('UVAG MARASCA - 12/500 ml').checked == true && document.getElementById('<?php echo $naturally_flavored_oil; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//fused oils
<?php foreach($fused_oils  as $fused_oil) { ?>
    if(document.getElementById('UVAG MARASCA - 12/500 ml').checked == true && document.getElementById('<?php echo $fused_oil; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>


//vinegars
<?php foreach($vinegars  as $vinegar) { ?>
    if(document.getElementById('UVAG MARASCA - 12/500 ml').checked == true && document.getElementById('<?php echo $vinegar; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//flavored balsamic vinegars
<?php foreach($flavored_balsamic_vinegars  as $flavored_balsamic_vinegar) { ?>
    if(document.getElementById('UVAG MARASCA - 12/500 ml').checked == true && document.getElementById('<?php echo $flavored_balsamic_vinegar; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//100 serenade
//oils
<?php foreach($oils as $oil) { ?>
    if(document.getElementById('SERENADE - 24/100 ml').checked == true && document.getElementById('<?php echo $oil; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_sleeves;
    }
<?php } ?>

//infused
<?php foreach($infused as $infuse) { ?>
    if(document.getElementById('SERENADE - 24/100 ml').checked == true && document.getElementById('<?php echo $infuse; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_sleeves;
    }
<?php } ?>

//organics
<?php foreach($organics  as $organic) { ?>
    if(document.getElementById('SERENADE - 24/100 ml').checked == true && document.getElementById('<?php echo $organic; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_sleeves;
    }
<?php } ?>

//other oils
<?php foreach($other_oils  as $other_oil) { ?>
    if(document.getElementById('SERENADE - 24/100 ml').checked == true && document.getElementById('<?php echo $other_oil; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_sleeves;
    }
<?php } ?>

//naturally flavored oils
<?php foreach($naturally_flavored_oils  as $naturally_flavored_oil) { ?>
    if(document.getElementById('SERENADE - 24/100 ml').checked == true && document.getElementById('<?php echo $naturally_flavored_oil; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_sleeves;
    }
<?php } ?>

//fused oils
<?php foreach($fused_oils  as $fused_oil) { ?>
    if(document.getElementById('SERENADE - 24/100 ml').checked == true && document.getElementById('<?php echo $fused_oil; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_sleeves;
    }
<?php } ?>


//vinegars
<?php foreach($vinegars  as $vinegar) { ?>
    if(document.getElementById('SERENADE - 24/100 ml').checked == true && document.getElementById('<?php echo $vinegar; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_sleeves;
    }
<?php } ?>

//flavored balsamic vinegars
<?php foreach($flavored_balsamic_vinegars  as $flavored_balsamic_vinegar) { ?>
    if(document.getElementById('SERENADE - 24/100 ml').checked == true && document.getElementById('<?php echo $flavored_balsamic_vinegar; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_sleeves;
    }
<?php } ?>

//200 serenade
//oils
<?php foreach($oils as $oil) { ?>
    if(document.getElementById('SERENADE - 12/200 ml').checked == true && document.getElementById('<?php echo $oil; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules_sleeves;
    }
<?php } ?>

//infused
<?php foreach($infused as $infuse) { ?>
    if(document.getElementById('SERENADE - 12/200 ml').checked == true && document.getElementById('<?php echo $infuse; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules_sleeves;
    }
<?php } ?>

//organics
<?php foreach($organics  as $organic) { ?>
    if(document.getElementById('SERENADE - 12/200 ml').checked == true && document.getElementById('<?php echo $organic; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules_sleeves;
    }
<?php } ?>

//other oils
<?php foreach($other_oils  as $other_oil) { ?>
    if(document.getElementById('SERENADE - 12/200 ml').checked == true && document.getElementById('<?php echo $other_oil; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules_sleeves;
    }
<?php } ?>

//naturally flavored oils
<?php foreach($naturally_flavored_oils  as $naturally_flavored_oil) { ?>
    if(document.getElementById('SERENADE - 12/200 ml').checked == true && document.getElementById('<?php echo $naturally_flavored_oil; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules_sleeves;
    }
<?php } ?>

//fused oils
<?php foreach($fused_oils  as $fused_oil) { ?>
    if(document.getElementById('SERENADE - 12/200 ml').checked == true && document.getElementById('<?php echo $fused_oil; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules_sleeves;
    }
<?php } ?>


//vinegars
<?php foreach($vinegars  as $vinegar) { ?>
    if(document.getElementById('SERENADE - 12/200 ml').checked == true && document.getElementById('<?php echo $vinegar; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules_sleeves;
    }
<?php } ?>

//flavored balsamic vinegars
<?php foreach($flavored_balsamic_vinegars  as $flavored_balsamic_vinegar) { ?>
    if(document.getElementById('SERENADE - 12/200 ml').checked == true && document.getElementById('<?php echo $flavored_balsamic_vinegar; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules_sleeves;
    }
<?php } ?>

//375 serenade
//oils
<?php foreach($oils as $oil) { ?>
    if(document.getElementById('SERENADE - 12/375 ml').checked == true && document.getElementById('<?php echo $oil; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules_sleeves;
    }
<?php } ?>

//infused
<?php foreach($infused as $infuse) { ?>
    if(document.getElementById('SERENADE - 12/375 ml').checked == true && document.getElementById('<?php echo $infuse; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules_sleeves;
    }
<?php } ?>

//organics
<?php foreach($organics  as $organic) { ?>
    if(document.getElementById('SERENADE - 12/375 ml').checked == true && document.getElementById('<?php echo $organic; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules_sleeves;
    }
<?php } ?>

//other oils
<?php foreach($other_oils  as $other_oil) { ?>
    if(document.getElementById('SERENADE - 12/375 ml').checked == true && document.getElementById('<?php echo $other_oil; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules_sleeves;
    }
<?php } ?>

//naturally flavored oils
<?php foreach($naturally_flavored_oils  as $naturally_flavored_oil) { ?>
    if(document.getElementById('SERENADE - 12/375 ml').checked == true && document.getElementById('<?php echo $naturally_flavored_oil; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules_sleeves;
    }
<?php } ?>

//fused oils
<?php foreach($fused_oils  as $fused_oil) { ?>
    if(document.getElementById('SERENADE - 12/375 ml').checked == true && document.getElementById('<?php echo $fused_oil; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules_sleeves;
    }
<?php } ?>


//vinegars
<?php foreach($vinegars  as $vinegar) { ?>
    if(document.getElementById('SERENADE - 12/375 ml').checked == true && document.getElementById('<?php echo $vinegar; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules_sleeves;
    }
<?php } ?>

//flavored balsamic vinegars
<?php foreach($flavored_balsamic_vinegars  as $flavored_balsamic_vinegar) { ?>
    if(document.getElementById('SERENADE - 12/375 ml').checked == true && document.getElementById('<?php echo $flavored_balsamic_vinegar; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules_sleeves;
    }
<?php } ?>

//100 stephanie
//oils
<?php foreach($oils as $oil) { ?>
    if(document.getElementById('STEPHANIE - 24/100 ml').checked == true && document.getElementById('<?php echo $oil; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = clear_sleave;
    }
<?php } ?>

//infused
<?php foreach($infused as $infuse) { ?>
    if(document.getElementById('STEPHANIE - 24/100 ml').checked == true && document.getElementById('<?php echo $infuse; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = clear_sleave;
    }
<?php } ?>

//organics
<?php foreach($organics  as $organic) { ?>
    if(document.getElementById('STEPHANIE - 24/100 ml').checked == true && document.getElementById('<?php echo $organic; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = clear_sleave;
    }
<?php } ?>

//other oils
<?php foreach($other_oils  as $other_oil) { ?>
    if(document.getElementById('STEPHANIE - 24/100 ml').checked == true && document.getElementById('<?php echo $other_oil; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = clear_sleave;
    }
<?php } ?>

//naturally flavored oils
<?php foreach($naturally_flavored_oils  as $naturally_flavored_oil) { ?>
    if(document.getElementById('STEPHANIE - 24/100 ml').checked == true && document.getElementById('<?php echo $naturally_flavored_oil; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = clear_sleave;
    }
<?php } ?>

//fused oils
<?php foreach($fused_oils  as $fused_oil) { ?>
    if(document.getElementById('STEPHANIE - 24/100 ml').checked == true && document.getElementById('<?php echo $fused_oil; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = clear_sleave;
    }
<?php } ?>


//vinegars
<?php foreach($vinegars  as $vinegar) { ?>
    if(document.getElementById('STEPHANIE - 24/100 ml').checked == true && document.getElementById('<?php echo $vinegar; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = clear_sleave;
    }
<?php } ?>

//flavored balsamic vinegars
<?php foreach($flavored_balsamic_vinegars  as $flavored_balsamic_vinegar) { ?>
    if(document.getElementById('STEPHANIE - 24/100 ml').checked == true && document.getElementById('<?php echo $flavored_balsamic_vinegar; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = clear_sleave;
    }
<?php } ?>


//200 stephanie
//oils
<?php foreach($oils as $oil) { ?>
    if(document.getElementById('STEPHANIE - 12/200 ml').checked == true && document.getElementById('<?php echo $oil; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//infused
<?php foreach($infused as $infuse) { ?>
    if(document.getElementById('STEPHANIE - 12/200 ml').checked == true && document.getElementById('<?php echo $infuse; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//organics
<?php foreach($organics  as $organic) { ?>
    if(document.getElementById('STEPHANIE - 12/200 ml').checked == true && document.getElementById('<?php echo $organic; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//other oils
<?php foreach($other_oils  as $other_oil) { ?>
    if(document.getElementById('STEPHANIE - 12/200 ml').checked == true && document.getElementById('<?php echo $other_oil; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//naturally flavored oils
<?php foreach($naturally_flavored_oils  as $naturally_flavored_oil) { ?>
    if(document.getElementById('STEPHANIE - 12/200 ml').checked == true && document.getElementById('<?php echo $naturally_flavored_oil; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//fused oils
<?php foreach($fused_oils  as $fused_oil) { ?>
    if(document.getElementById('STEPHANIE - 12/200 ml').checked == true && document.getElementById('<?php echo $fused_oil; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>


//vinegars
<?php foreach($vinegars  as $vinegar) { ?>
    if(document.getElementById('STEPHANIE - 12/200 ml').checked == true && document.getElementById('<?php echo $vinegar; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>

//flavored balsamic vinegars
<?php foreach($flavored_balsamic_vinegars  as $flavored_balsamic_vinegar) { ?>
    if(document.getElementById('STEPHANIE - 12/200 ml').checked == true && document.getElementById('<?php echo $flavored_balsamic_vinegar; ?>').checked == true)
    {
    document.getElementById("capsule").innerHTML = all_capsules;
    }
<?php } ?>
