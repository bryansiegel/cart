<?php

/*
  * Email
  */

//clean string
function clean_string($string)
{
    $bad = array("content-type", "bcc:", "to:", "cc:", "href");
    return str_replace($bad, "", $string);
}

$email_message = '<html>
<head>
    <title>Prepacked Bottles</title>
    <link rel="important stylesheet" href="chrome://messagebody/skin/messageBody.css">
</head>
<body>
<br>
<style type="text/css">
    body, td {
        color: #2f2f2f;
        font: 11px/1.35em Verdana, Arial, Helvetica, sans-serif;
    }
</style>
<div style="font:11px/1.35em Verdana, Arial, Helvetica, sans-serif;">
    <table cellspacing="0" cellpadding="0" border="0" width="100%"
           style="margin-top:10px; font:11px/1.35em Verdana, Arial, Helvetica, sans-serif; margin-bottom:10px;">
        <tr>
            <td align="center" valign="top">
                <!-- [ header starts here] -->
                <table cellspacing="0" cellpadding="0" border="0" width="650">
                    <tr>
                        <td valign="top">&nbsp;</td>
                    </tr>
                </table>
                <!-- [ middle starts here] -->
                <table cellspacing="0" cellpadding="0" border="0" width="650">
                    <tr>
                        <td valign="top">
                            <p>
                                <strong> Hello ';

//name
$email_message .= $billing_name;


$email_message .= ' </strong>,<br/>
                                Thank you, Cibaria Store Supply is in receipt of your Purchase Order.
                                <strong>Customer Service will send a Sales Confirmation verifying product selection,
                                    cost, freight weight/pallet information, shipping method and pick up appointment
                                    time within 2-3 business days.</strong> Please be sure to review document for
                                accuracy. Any changes or additions to the original Purchase Order must be submitted in
                                writing and may result an additional charges. Changes will delay your order.
                                If you have any questions about your order please contact us at <a
                                    href="mailto:customerservice@cibaria-intl.com" style="color:#1E7EC8;">customerservice@cibaria-intl.com</a>
                                or call us at <span class="nobr">951-823-8490</span> Monday - Friday, 7:30am - 4pm PST.
                            </p>

                            <p>Your order confirmation is below. Thank you again for your business.</p>

                            <h3 style="border-bottom:2px solid #eee; font-size:1.05em; padding-bottom:1px; ">Your Order
                                #';

//add 40000 and order id to get the order id
$orders = ProductsQuery::create()->findByOrderId($session_id);
$email_message .= '40000';

foreach ($orders as $order) {
    $order_id = $order->getOrderId();
}

//order id
$email_message .= $order_id . " ";

$email_message .= '<small>(placed on';

//date
$email_message .= " " . date("m/d/y");

$email_message .= ')</small>
                            </h3>
                            <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                <thead>
                                <tr>
                                    <th align="left" width="48.5%" bgcolor="#d9e5ee"
                                        style="padding:5px 9px 6px 9px; border:1px solid #bebcb7; border-bottom:none; line-height:1em;">
                                        Billing Information:
                                    </th>
                                    <th width="3%"></th>
                                    <th align="left" width="48.5%" bgcolor="#d9e5ee"
                                        style="padding:5px 9px 6px 9px; border:1px solid #bebcb7; border-bottom:none; line-height:1em;">
                                        Special Instructions:
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td valign="top"
                                        style="padding:7px 9px 9px 9px; border:1px solid #bebcb7; border-top:0; background:#f8f7f5;">
                       ';
//return product

//billing
$email_message .= clean_string($billing_name) . "<br />";
$email_message .= clean_string($billing_street_address) . "<br />";
$email_message .= clean_string($billing_city) . ", ";
$email_message .= clean_string($billing_state) . ", ";
$email_message .= clean_string($billing_zip) . "<br />";
$email_message .= clean_string($billing_telephone) . "<br />";
$email_message .= clean_string($billing_email) . "<br />";
$email_message .= '</td>
                                    <td>&nbsp;</td>
                                    <td valign="top"
                                        style="padding:7px 9px 9px 9px; border:1px solid #bebcb7; border-top:0; background:#f8f7f5;">
                                        <p><span class="nowrap">';
//notes
$email_message .= clean_string($notes);

$email_message .= '</span></p>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            <br/>
                            <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                <thead>
                                <tr>
                                    <th align="left" width="48.5%" bgcolor="#d9e5ee"
                                        style="padding:5px 9px 6px 9px; border:1px solid #bebcb7; border-bottom:none; line-height:1em;">
                                        Shipping Information:
                                    </th>
                                    <th width="3%"></th>
                                    <th align="left" width="48.5%" bgcolor="#d9e5ee"
                                        style="padding:5px 9px 6px 9px; border:1px solid #bebcb7; border-bottom:none; line-height:1em;">
                                        Shipping Method:
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td valign="top"
                                        style="padding:7px 9px 9px 9px; border:1px solid #bebcb7; border-top:0; background:#f8f7f5;">';

//shipping
$email_message .= clean_string($shipping_name) . "<br />";
$email_message .= clean_string($shipping_street_address) . "<br />";
$email_message .= clean_string($shipping_city) . ", ";
$email_message .= clean_string($shipping_state) . ", ";
$email_message .= clean_string($shipping_zip) . "<br />";
$email_message .= clean_string($shipping_telephone) . "<br />";
$email_message .= clean_string($shipping_email) . "<br />";
$email_message .= '</td>
                                    <td>&nbsp;</td>
                                    <td valign="top"
                                        style="padding:7px 9px 9px 9px; border:1px solid #bebcb7; border-top:0; background:#f8f7f5;">';

//shipping method
$email_message .= "Shipping Method: " . clean_string($shipping_method) . "\n";
$email_message .= '</td>
                                </tr>
                                </tbody>
                            </table>
                            <br/>';
$email_message .= '<table cellspacing="0" cellpadding="0" border="1" width="100%"
                                   style="border:1px solid #bebcb7; background:#f8f7f5;">
                                <thead>
                                <tr>
                                    <th align="left" bgcolor="#d9e5ee" style="padding:3px 9px">Product</th>
                                    <th align="left" bgcolor="#d9e5ee" style="padding:3px 9px">Bottle</th>
                                    <th align="center" bgcolor="#d9e5ee" style="padding:3px 9px">Cap</th>
                                    <th align="center" bgcolor="#d9e5ee" style="padding:3px 9px">Capsule</th>
                                    <th align="center" bgcolor="#d9e5ee" style="padding:3px 9px">QTY</th>
                                </tr>
                                </thead>
                                <tbody bgcolor="#eeeded">
                                ';

//products
$products = ProductsQuery::create()->findByOrderId($session_id);

foreach ($products as $index) {
    $email_message .= '<tr>';
    $email_message .= '<td>' . $index->getProduct() . '</td>';
    $email_message .= '<td>' . $index->getBottle() . '</td>';
    $email_message .= '<td>' . $index->getCap() . '</td>';
    $email_message .= '<td>' . $index->getCapsule() . '</td>';
    $email_message .= '<td>' . $index->getQty() . '</td>';
    $email_message .= '</tr>';
}
$email_message .= '
                               </tbody>
                                <tfoot>
                                <tr>
                                    <td colspan="5" align="right" style="padding:3px 9px; font-weight:bold;">Subtotal</td>

    <td align="right" style="padding:3px 9px"><span class="price">';
//subtotal
foreach ($products as $subtotal) {
    $total_product_price = $subtotal->getQty() * $subtotal->getPrice();
    $upgrade_product_price = $subtotal->getUpgradePrice();
    $total_price = $total_product_price + $upgrade_product_price;
    $total_items_price[] = $total_price;
}

$total_all_price = array_sum($total_items_price);

//subtotal
$email_message .= '$ ';
$email_message .= number_format($total_all_price, 2, '.', ',');
$email_message .= '</span></td>
                                </tr>
                                </tfoot>
                            </table>
                            <br/>

                            <p>Thank you again,<br/><strong>Cibaria Store Supply </strong></p>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>
</body>
</html>';
?>