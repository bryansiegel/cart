<!--show/hide-->
<script>
    $(document).ready(function(){
//        oils
        $("span#oils").hide(function(){
            $("button#oils").click(function(){
                $("span#oils").toggle();
            });
        });
//infused
        $("span#infused").hide(function(){
            $("button#infused").click(function(){
                $("span#infused").toggle();
            });
        });
//organic
        $("span#organic").hide(function(){
            $("button#organic").click(function(){
                $("span#organic").toggle();
            });
        });
//other oils
        $("span#other_oils").hide(function(){
            $("button#other_oils").click(function(){
                $("span#other_oils").toggle();
            });
        });
//naturally flavored oils
        $("span#naturally_flavored_oils").hide(function(){
            $("button#naturally_flavored_oils").click(function(){
                $("span#naturally_flavored_oils").toggle();
            });
        });
//fused olive oils
        $("span#fused").hide(function(){
            $("button#fused").click(function(){
                $("span#fused").toggle();
            });
        });
//vinegars
        $("span#vinegars").hide(function(){
            $("button#vinegars").click(function(){
                $("span#vinegars").toggle();
            });
        });
//flavored balsamic vinegars
        $("span#flavored_balsamic_vinegars").hide(function(){
            $("button#flavored_balsamic_vinegars").click(function(){
                $("span#flavored_balsamic_vinegars").toggle();
            });
        });
//bordolese bottles
        $("span#bordolese_bottles").hide(function(){
            $("button#bordolese_bottles").click(function(){
                $("span#bordolese_bottles").toggle();
            });
        });
//bordeaux bottles
        $("span#bordeaux_bottles").hide(function(){
            $("button#bordeaux_bottles").click(function(){
                $("span#bordeaux_bottles").toggle();
            });
        });
//antique/green dorica bottles
        $("span#dorica_bottles").hide(function(){
            $("button#dorica_bottles").click(function(){
                $("span#dorica_bottles").toggle();
            });
        });
//clear dorica bottles
        $("span#clear_dorica_bottles").hide(function(){
            $("button#clear_dorica_bottles").click(function(){
                $("span#clear_dorica_bottles").toggle();
            });
        });

//marasca bottles
        $("span#marasca_bottles").hide(function(){
            $("button#marasca_bottles").click(function(){
                $("span#marasca_bottles").toggle();
            });
        });
//green marasca bottles
        $("span#green_marasca_bottles").hide(function(){
            $("button#green_marasca_bottles").click(function(){
                $("span#green_marasca_bottles").toggle();
            });
        });
//uvag marasca bottles
        $("span#uvag_marasca_bottles").hide(function(){
            $("button#uvag_marasca_bottles").click(function(){
                $("span#uvag_marasca_bottles").toggle();
            });
        });
//serenade bottles
        $("span#serenade_bottles").hide(function(){
            $("button#serenade_bottles").click(function(){
                $("span#serenade_bottles").toggle();
            });
        });
//stephanie bottles
        $("span#stephanie_bottles").hide(function(){
            $("button#stephanie_bottles").click(function(){
                $("span#stephanie_bottles").toggle();
            });
        });


//    end
    });

</script>