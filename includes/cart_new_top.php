<?php

//session start
if (!isset($_SESSION)) session_start();

//propel
include('propel.php');

//functions
include('functions.php');

//make a new order
$order = new Orders();

//check if the session is new
if (!isset($_SESSION["visits"]))
    $_SESSION["visits"] = 0;
$_SESSION["visits"] = $_SESSION["visits"] + 1;

//if the session isn't new create an order
if ($_SESSION['visits'] > 1) {
    echo $order->getOrderNumber();
    echo $order->getId();
} else {
    $order->setOrderNumber("40001");
//have to save to get the id
    $order->save();

    //echo $order->getOrderNumber();
    //echo $order->getId();

    //debugging
    //echo "not refreshed";
}

?>