<?php

//get the values from post

//qty validation
if(isset($_REQUEST['qty']) && $_REQUEST['qty'] && empty($_REQUEST['qty']) != " ")
{
    $qty = $_REQUEST['qty'];
} else {
    echo "<div class='alert alert-danger' role='alert'>You Must Enter A Quantity</div>";
}

//bottle validation
if(isset($_REQUEST['bottle']) && $_REQUEST['bottle'] != "bottle")
{
    $bottle = $_REQUEST['bottle'];
} else {
    echo "<div class='alert alert-danger' role='alert'>You Must Select A Bottle</div>";
}

//cap validation
if(isset($_REQUEST['cap']) && $_REQUEST['cap'] && empty($_REQUEST['cap']) != " ")
{
    $cap = $_REQUEST['cap'];

    //upgrade price
    switch($cap)
    {
        case "Cork with Pourspout":
            $upgrade_price = 19.40;
            break;
        case "Stainless Steel Pour Spout":
            $upgrade_price = 20.08;
            break;
        case "Plastic Pour Spout":
            $upgrade_price = 16.58;
            break;
        default:
            $upgrade_price = 0.00;
    }
} else {
    echo "<div class='alert alert-danger' role='alert'>You Must Select A Cap</div>";
}

//capsule validation
if(isset($_REQUEST['capsule']) && $_REQUEST['capsule'] != "capsule")
{
    $capsule = $_REQUEST['capsule'];
} else {
    echo "<div class='alert alert-danger' role='alert'>You Must Select A Capsule</div>";
}

//product validation
if(isset($_REQUEST['product']) && $_REQUEST['product'] != "product")
{
    $product_name = $_REQUEST['product'];
} else {
    echo "<div class='alert alert-danger' role='alert'>You Must Select A Product</div>";
}

//label
//if(isset($_REQUEST['label']) && $_REQUEST['label'] != " ")
//{
//    $label = $_REQUEST['label'];
//} else {
//    echo "<div class='alert alert-danger' role='alert'>You Must Select A Label</div>";
//}



//upgrade price
if(isset($product_name) && isset($bottle) && isset($qty) && isset($cap)){
    echo "<div class='alert alert-success' role='alert'>" .
        "<strong>". $product_name. " with " .$bottle. " and a " . $cap . " and a Quantity of ". $qty. " has been added to your cart.". "</strong>".
        "</div>";
}




if(isset($_REQUEST['id']))
{
    $_SESSION['id'] = $_REQUEST['id'];
    $id = $_SESSION['id'];
}




?>