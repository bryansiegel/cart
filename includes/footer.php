<footer>
    <div id="footer">

        <!-- Begin contacts //-->

        <div class="column-contacts">

            <ul>

                <h3>Cibaria International</h3>

                <li class="phone">1203 Hall Ave. Riverside, CA 92509</li>

                <li class="phone">Phone: (951) 823 - 8490</li>

                <li class="mobile">Fax: (951) 823 - 8495</li>

                <li class="email"><a href="http://www.cibariastoresupply.com/contact.php" rel="nofollow">Contact
                        Us</a></li>

            </ul>

        </div>

        <!-- End contacts //-->

        <!-- Begin footer columns //-->

        <div class="column4">

            <h3>Cibaria Websites</h3>

            <ul>

                <li><a href="http://www.cibariasoapsupply.com/shop/">Cibaria Soap Supply</a></li>

                <li><a href="http://www.cibaria-intl.com">Cibaria International</a></li>
                <li><a href="http://olivetopartyrecipes.com/" title="Olive To Party Recipes">O'live To Party
                        Recipes</a></li>
                <li><a href="http://18yearbalsamicvinegarfacts.com/" title="18 Year Balsamic Vinegar Facts">18
                        Year Balsamic Vinegar Facts</a></li>
            </ul>

        </div>

        <div class="column3">

            <h3>My Account</h3>

            <ul>

                <li><a href="/shop/customer/account" rel="nofollow">My Account</a></li>

                <li><a href="/shop/sales/order/history/" rel="nofollow">Order History</a></li>

                <li><a href="/shop/wishlist" rel="nofollow">Wish List</a></li>

            </ul>

        </div>

        <div class="column2">

            <h3>Customer Service</h3>

            <ul>

                <li><a href="http://www.cibariastoresupply.com/contact.php" rel="nofollow">Contact Us</a></li>

                <li><a href="/shop/freight-policy" rel="nofollow">Freight Policy</a></li>

                <li><a href="/shop/payments" rel="nofollow">Website Payment Procedure</a></li>

                <li><a href="/shop/ordering-policy" rel="nofollow">Ordering Policy</a></li>

                <li><a href="/shop/damage-policy" rel="nofollow">Damage Policy</a></li>

                <li><a href="/shop/policy-changes" rel="nofollow">Policy Changes</a></li>

                <li><a href="/shop/privacy-policy" rel="nofollow">Privacy Policy</a></li>

                <li><a href="/shop/site-terms" rel="nofollow">Terms &amp; Conditions</a></li>

            </ul>

        </div>

        <div class="column1">

            <h3>Information</h3>

            <ul>

                <li><a href="/shop/about-us">About Us</a></li>

                <li><a href="/shop/index.php/faq">FAQ</a></li>

                <li><a href="/shop/company-holidays" rel="nofollow">Company Holidays</a></li>

                <li><a href="/shop/index.php/articles">Articles</a></li>

            </ul>

        </div>


    </div>




    <script src="/assets/js/button_toggle.js"></script>

    <script type="text/javascript" src="js/bootstrap.js"></script>

</footer>

<!-- Le javascript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<!--<script src="assets/js/transition.js"></script>-->
<!--<script src="assets/js/alert.js"></script>-->
<!--<script src="assets/js/modal.js"></script>-->
<!--<script src="assets/js/dropdown.js"></script>-->
<!--<script src="assets/js/scrollspy.js"></script>-->
<!--<script src="assets/js/tab.js"></script>-->
<!--<script src="assets/js/tooltip.js"></script>-->
<!--<script src="assets/js/popover.js"></script>-->
<!--<script src="assets/js/button.js"></script>-->
<!--<script src="assets/js/collapse.js"></script>-->
<!--<script src="assets/js/carousel.js"></script>-->
<!--<script src="/assets/js/typeahead.js"></script>-->

<!--toggle buttons for prepacked-->
