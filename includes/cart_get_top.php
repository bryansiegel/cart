<?php

//session start
if (!isset($_SESSION)) session_start();

//check if post
if ($_SERVER['REQUEST_METHOD'] == 'POST') {

//propel
    include('includes/propel.php');

// validation
    include('includes/validation.php');


}

//set the values to db
if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    $product = new Products();
    $product->setOrderId($_SESSION['id']);

    //product name
    $product->setProduct($product_name);
    //sku TODO:REFACTOR ONCE SKU ISSUE IS RESOLVED FOR PRODUCTS
    $product->setSku("OOS2343");
    //active
    $product->setActive(1);

    /*
     * PRICE FOR OILS/VINEGARS
    */

    include('price.php');

    //values from post and set to db

    //bottle
    if (isset($bottle)) {
        $product->setBottle($bottle);
    }

    //cap
    if (isset($cap)) {
        $product->setCap($cap);
    }

    //upgrade price
    if(isset($upgrade_price))
    {
        //$upgrade_price from caps in validation.php
        $price_qty = $upgrade_price * $qty;
        //set the upgrade price
        $product->setUpgradePrice($price_qty);
    }

    //capsule
    if (isset($capsule) && $capsule != '') {
        //TODO:GET CAPSULE AND IF UPGRADE GET COST
        $product->setCapsule($capsule);
    }

    //qty
    if (isset($qty)) {
        $product->setQty($qty);
    }

    //price
    if (isset($price)) {
        $product->setPrice($price);
    }

    //label
//    if(isset($label)) {
//        $product->setLabel($label);
//    }




    /**********************************
     * SAVE THE VALUES FROM POST TO DB
     * *******************************
     */
    //if the values exist
    if (isset($product_name) && isset($qty) && isset($bottle) && isset($price) && isset($cap) && isset($capsule)) {

        //save the product

            $product->save();








        //TODO:FIX THIS IT'S NOT WORKING
        //unset the values so that if the client refreshes it doesn't add the post data to the database again
        header('Location: cart_new.php');


    }
}