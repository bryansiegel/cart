<?php

//cart_new.php

//bordolese
$bordolese_bottles = array("BORDOLESE - 12/375 ml");

//bordeaux
$bordeaux_bottles = array(
    "BORDEAUX - 12/375 ml",
    "BORDEAUX - 12/500 ml",
    "BORDEAUX - 12/750 ml" );

//dorica bottles
//antique/green
$dorica_bottles = array(
    "DORICA - 24/100 ML",
    "DORICA - 12/250 ML",
    "DORICA - 12/500 ML",
);

$clear_dorica_bottles = array(
    "CLEAR DORICA - 12/250 ML",
    "CLEAR DORICA - 12/500 ML",
);

//mara bottles
//clear
$marasca_bottles = array(
    "MARASCA - 24/100 ml",
    "MARASCA - 12/250 ml",
    "MARASCA - 12/500 ml",
//    "MARASCA - 12/750 ml",
//    "MARASCA - 6/1 Liter",
);

//1/2 green marasca
$green_marasca_bottles = array(
//    "GREEN MARASCA - 24/100 ml",
//    "GREEN MARASCA - 12/250 ml",
    "GREEN MARASCA - 12/500 ml",
    "GREEN MARASCA - 12/750 ml",
//    "GREEN MARASCA - 6/1 Liter",
);
//uvag marasca bottles
$uvag_marasca_bottles = array(
//    "UVAG MARASCA - 24/100 ml",
//    "UVAG MARASCA - 12/250 ml",
    "UVAG MARASCA - 12/500 ml",
//    "UVAG MARASCA - 12/750 ml",
//    "UVAG MARASCA - 6/1 Liter",
);

//serenade bottles
$serenade_bottles = array(
    "SERENADE - 24/100 ml",
    "SERENADE - 12/200 ml",
    "SERENADE - 12/375 ml",
);

//stephanie bottles
$stephanie_bottles = array(
    "STEPHANIE - 24/100 ml",
    "STEPHANIE - 12/200 ml",
);

//oils
$oils = array(
    "Olive Oil Extra Virgin",
    "Australia - Leccino/Frantoio",
    "Australia - Frantoio",
    "California Arbequina",
    "California Arbosana",
    "California Mission",
    "California Arbequina/Arbosana/Koroneiki",
    "Chile - Arbequina",
    "Chile - Frantoio",
    "Chile - Leccino",
    "Chile - Picual",
    "Greece - Kalamata",
    "Greece - Koroneiki",
    "Italy - Umbrian region",
    "Italy - Calabria region - Carolea",
    "Italy (Puglia) - Coratina",
    "Italy (Puglia)- Ogliarola",
    "Spain - Hojiblanca",
    "Spain - Picual",
    "Spain - Signature blend",
    "Tunisia - Chemlali/Chetoui",
    );

//infused
$infused = array(
    "Black Pepper",
    "Garlic Oil",
    "Roasted Chili",
    "Oregano",
    "Lemon Pepper",
    "Jalapeno oil",
    "Basil",
    "Scallion",
    "Habenero",
    "Rosemary",
    "Chipotle",
);

//organics
$organics = array(
    "Olive Oil, Extra Virgin, Organic , Italy",
    "Olive Oil, Extra Virgin, Organic , Country Varies",
    "Canola",
    "Soybean Oil",
    "Sunflower",
    "Sesame Toasted",
    "Sesame, Virgin",
);

//other oils
$other_oils = array(
    "Almond Oil",
    "Apricot Oil",
    "Avocado Oil",
    "Flaxseed Oil",
    "Grapeseed Oil",
    "Hempseed Oil",
    "Macadamia Nut Oil",
    "Toasted Sesame Oil",
    "Walnut Oil",
);

//naturally flavored oils
$naturally_flavored_oils = array(
    "Blood Orange",
    "Lemon oil",
    "Lime",
    "Orange oil",
    "Tangerine oil",
    "Butter",
    "Cranberry Walnut oil",
    "Arbequina Rhubarb",
    "Sage Onion",
    "Sundried Tomato",
    "White Truffle",
    "Black Truffle oil",
    "Hickory oil",
    "Vanilla oil",
);

//fused oils
$fused_oils = array(
    "Garlic Mushroom",
    "Basil/Lemongrass",
    "Rosemary with Lavender",
    "Tuscan Herb",
    "Herbs d Provence",
    "Citrus Habanero",
    "Garlic Roasted Chili",
);

//vinegars

$vinegars = array(
    "4 Star Balsamic",
    "8 Star Balsamic",
    "12 Star Balsamic",
    "25 Star Dark Balsamic",
    "25 Star White Balsamic",
    "6 Star White Balsamic",
    "Red Wine Vinegar - Domestic - 50 Grain",
    "Organic Premium Quality Balsamic - Ltd Edition",
    "Serrano Chile Vinegar",
    "Lambrusco Vinegar",
);

//flavored balsamic vinegars
$flavored_balsamic_vinegars = array(
    "Almond Creme",
    "Apricot",
    "Bittersweet Chocolate with Orange",
    "Blackberry",
    "Blackberry Ginger",
    "Black Currant",
    "Black Walnut",
    "Blueberry",
    "Bordeaux Cherry",
    "Chili",
    "Chocolate",
    "Chocolate Covered Cherries",
    "Chocolate Jalapeno",
    "Chocolate Marshmallow",
    "Coconut",
    "Cranberry Orange",
    "Cranberry Walnut",
    "Cucumber Melon",
    "Elderberry",
    "Espresso",
    "Fig",
    "Garlic",
    "Garlic Cilantro",
    "Grapefruit",
    "Green Apple",
    "Hickory",
    "Honey",
    "Honey Ginger",
    "Huckleberry",
    "Jalapeno",
    "Jalapeno Fig in 8 Star",
    "Jalapeno & Lime",
    "Lavender",
    "Lemongrass Mint",
    "Lemon",
    "Lemon Vanilla",
    "Mandarin Orange",
    "Mango",
    "Mocha Almond Fudge",
    "Orange",
    "Orange/Mango/Passionfruit",
    "Orange Vanilla",
    "Peach",
    "Pear",
    "Pecan Praline",
    "Pineapple",
    "Pomegranate",
    "Pumpkin Spice",
    "Raspberry",
    "Raspberry Ginger",
    "Strawberry",
    "Strawberry Peach",
    "Tangerine",
    "Black Truffle",
    "Vanilla",
    "Vanilla Fig",
);

$green_green_gold_capsules = array(
  "Green",
    "Green/Gold",
);



