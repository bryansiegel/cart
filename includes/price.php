<?php
/**
 * Created by PhpStorm.
 * User: bryansiegel
 * Date: 1/6/15
 * Time: 2:07 PM
 */


$product_name_bottle = $product_name . " " . $bottle;

//if($product_name_bottle == "Black Pepper infused oil BORDOLESE - 12/375 ml")
//{
//    $new_price = "12.34";
//    $price = "24.95";
//
//    $total_price = $new_price + $price;
//    echo $total_price;
//}

switch($product_name_bottle){

    //oils & Bordolese Bottles
    case "Olive Oil Extra Virgin BORDOLESE - 12/375 ml":
        $price = 38.00;
        break;
    case "Australia - Leccino/Frantoio BORDOLESE - 12/375 ml":
        $price = 49.00;
        break;
    case "Australia - Frantoio BORDOLESE - 12/375 ml":
        $price = 49.00;
        break;
    case "California Arbequina BORDOLESE - 12/375 ml":
        $price = 74.00;
        break;
    case "California Arbosana BORDOLESE - 12/375 ml":
        $price = 70.00;
        break;
    case "California Mission BORDOLESE - 12/375 ml":
        $price = 70.00;
        break;
    case "California Arbequina/Arbosana/Koroneiki BORDOLESE - 12/375 ml":
        $price = 49.00;
        break;
    case "Chile - Arbequina BORDOLESE - 12/375 ml":
        $price = 39.00;
        break;
    case "Chile - Frantoio BORDOLESE - 12/375 ml":
        $price = 39.00;
        break;
    case "Chile - Leccino BORDOLESE - 12/375 ml":
        $price = 39.00;
        break;
    case "Chile - Picual BORDOLESE - 12/375 ml":
        $price = 39.00;
        break;
    case "Greece - Kalamata BORDOLESE - 12/375 ml":
        $price = 44.00;
        break;
    case "Greece - Koroneiki BORDOLESE - 12/375 ml":
        $price = 44.00;
        break;
    case "Italy - Umbrian region BORDOLESE - 12/375 ml":
        $price = 67.00;
        break;
    case "Italy - Calabria region - Carolea BORDOLESE - 12/375 ml":
        $price = 57.00;
        break;
    case "Italy (Puglia) - Coratina BORDOLESE - 12/375 ml":
        $price = 57.00;
        break;
    case "Italy (Puglia)- Ogliarola BORDOLESE - 12/375 ml":
        $price = 57.00;
        break;
    case "Spain - Hojiblanca BORDOLESE - 12/375 ml":
        $price = 46.00;
        break;
    case "Spain - Picual BORDOLESE - 12/375 ml":
        $price = 46.00;
        break;
    case "Spain - Signature blend  BORDOLESE - 12/375 ml":
        $price = 46.00;
        break;
    case "Tunisia - Chemlali/Chetoui BORDOLESE - 12/375 ml":
        $price = 38.00;
        break;
    //infused
    case "Black Pepper BORDOLESE - 12/375 ml":
        $price = 62.00;
        break;
    case "Garlic Oil BORDOLESE - 12/375 ml":
        $price = 53.00;
        break;
    case "Roasted Chili BORDOLESE - 12/375 ml":
        $price = 55.00;
        break;
    case "Oregano BORDOLESE - 12/375 ml":
        $price = 57.00;
        break;
    case "Lemon Pepper BORDOLESE - 12/375 ml":
        $price = 66.00;
        break;
    case "Jalapeno oil BORDOLESE - 12/375 ml":
        $price = 53.00;
        break;
    case "Basil BORDOLESE - 12/375 ml":
        $price = 53.00;
        break;
    case "Scallion BORDOLESE - 12/375 ml":
        $price = 58.00;
        break;
    case "Habenero BORDOLESE - 12/375 ml":
        $price = 70.00;
        break;
    case "Rosemary BORDOLESE - 12/375 ml":
        $price = 59.00;
        break;
    case "Chipotle BORDOLESE - 12/375 ml":
        $price = 55.00;
        break;
    //flavored olive oils
    case "Blood Orange BORDOLESE - 12/375 ml":
        $price = 41.00;
        break;
    case "Lemon oil BORDOLESE - 12/375 ml":
        $price = 47.00;
        break;
    case "Lime BORDOLESE - 12/375 ml":
        $price = 49.00;
        break;
    case "Orange oil BORDOLESE - 12/375 ml":
        $price = 41.00;
        break;
    case "Tangerine oil BORDOLESE - 12/375 ml":
        $price = 43.00;
        break;
    case "Butter BORDOLESE - 12/375 ml":
        $price = 52.00;
        break;
    case "Cranberry Walnut oil BORDOLESE - 12/375 ml":
        $price = 42.00;
        break;
    case "Arbequina Rhubarb BORDOLESE - 12/375 ml":
        $price = 42.00;
        break;
    case "Sage Onion BORDOLESE - 12/375 ml":
        $price = 59.00;
        break;
    case "Sundried Tomato BORDOLESE - 12/375 ml":
        $price = 50.00;
        break;
    case "White Truffle BORDOLESE - 12/375 ml":
        $price = 108.00;
        break;
    case "Black Truffle oil BORDOLESE - 12/375 ml":
        $price = 106.00;
        break;
    case "Hickory oil BORDOLESE - 12/375 ml":
        $price = 49.00;
        break;
    case "Vanilla oil BORDOLESE - 12/375 ml":
        $price = 50.00;
        break;
    //fused
    case "Garlic Mushroom BORDOLESE - 12/375 ml":
        $price = 59.00;
        break;
    case "Basil/Lemongrass BORDOLESE - 12/375 ml":
        $price = 60.00;
        break;
    case "Rosemary with Lavender BORDOLESE - 12/375 ml":
        $price = 67.00;
        break;
    case "Tuscan Herb BORDOLESE - 12/375 ml":
        $price = 65.00;
        break;
    case "Herbs d Provence BORDOLESE - 12/375 ml":
        $price = 62.00;
        break;
    case "Citrus Habanero BORDOLESE - 12/375 ml":
        $price = 55.00;
        break;
    case "Garlic Roasted Chili BORDOLESE - 12/375 ml":
        $price = 59.00;
        break;
    //specialty oils
    case "Almond Oil BORDOLESE - 12/375 ml":
        $price = 49.00;
        break;
    case "Apricot Oil BORDOLESE - 12/375 ml":
        $price = 51.00;
        break;
    case "Avocado Oil BORDOLESE - 12/375 ml":
        $price = 44.00;
        break;
    case "Flaxseed Oil BORDOLESE - 12/375 ml":
        $price = 36.00;
        break;
    case "Grapeseed Oil BORDOLESE - 12/375 ml":
        $price = 31.00;
        break;
    case "Hempseed Oil BORDOLESE - 12/375 ml":
        $price = 80.00;
        break;
    case "Macadamia Nut Oil BORDOLESE - 12/375 ml":
        $price = 57.00;
        break;
    case "Toasted Sesame Oil BORDOLESE - 12/375 ml":
        $price = 38.00;
        break;
    case "Walnut Oil BORDOLESE - 12/375 ml":
        $price = 34.00;
        break;
    //organics
    case "Olive Oil, Extra Virgin, Organic , Italy BORDOLESE - 12/375 ml":
        $price = 46.00;
        break;
    case "Olive Oil, Extra Virgin, Organic , Country Varies BORDOLESE - 12/375 ml":
        $price = 42.00;
        break;
    case "Canola BORDOLESE - 12/375 ml":
        $price = 28.00;
        break;
    case "Soybean Oil BORDOLESE - 12/375 ml":
        $price = 22.00;
        break;
    case "Sunflower BORDOLESE - 12/375 ml":
        $price = 38.00;
        break;
    case "Sesame Toasted BORDOLESE - 12/375 ml":
        $price = 36.00;
        break;
    case "Sesame, Virgin BORDOLESE - 12/375 ml":
        $price = 36.00;
        break;
    //vinegars
    case "4 Star Balsamic BORDOLESE - 12/375 ml":
        $price = 24.00;
        break;
    case "8 Star Balsamic BORDOLESE - 12/375 ml":
        $price = 34.00;
        break;
    case "12 Star Balsamic BORDOLESE - 12/375 ml":
        $price = 38.00;
        break;
    case "25 Star Dark Balsamic BORDOLESE - 12/375 ml":
        $price = 59.00;
        break;
    case "25 Star White Balsamic BORDOLESE - 12/375 ml":
        $price = 59.00;
        break;
    case "6 Star White Balsamic BORDOLESE - 12/375 ml":
        $price = 32.00;
        break;
    case "Red Wine Vinegar - Domestic - 50 Grain BORDOLESE - 12/375 ml":
        $price = 20.00;
        break;
    case "Organic Premium Quality Balsamic - Ltd Edition BORDOLESE - 12/375 ml":
        $price = 61.00;
        break;
    case "Serrano Chile Vinegar BORDOLESE - 12/375 ml":
        $price = 64.00;
        break;
    case "Lambrusco Vinegar BORDOLESE - 12/375 ml":
        $price = 59.00;
        break;
    //flavored balsamic
    case "Almond Creme BORDOLESE - 12/375 ml":
        $price = 59;
        break;
    case "Apricot BORDOLESE - 12/375 ml":
        $price = 59.00;
        break;
    case "Bittersweet Chocolate with Orange BORDOLESE - 12/375 ml":
        $price = 59.00;
        break;
    case "Blackberry BORDOLESE - 12/375 ml":
        $price = 59.00;
        break;
    case "Blackberry Ginger BORDOLESE - 12/375 ml":
        $price = 59.00;
        break;
    case "Black Currant BORDOLESE - 12/375 ml":
        $price = 59.00;
        break;
    case "Black Walnut BORDOLESE - 12/375 ml":
        $price = 59.00;
        break;
    case "Blueberry BORDOLESE - 12/375 ml":
        $price = 59.00;
        break;
    case "Bordeaux Cherry BORDOLESE - 12/375 ml":
        $price = 59.00;
        break;
    case "Chili BORDOLESE - 12/375 ml":
        $price = 59.00;
        break;
    case "Chocolate BORDOLESE - 12/375 ml":
        $price = 59.00;
        break;
    case "Chocolate Covered Cherries BORDOLESE - 12/375 ml":
        $price = 59.00;
        break;
    case "Chocolate Jalapeno BORDOLESE - 12/375 ml":
        $price = 59.00;
        break;
    case "Chocolate Marshmallow BORDOLESE - 12/375 ml":
        $price = 59.00;
        break;
    case "Coconut BORDOLESE - 12/375 ml":
        $price = 59.00;
        break;
    case "Cranberry Orange BORDOLESE - 12/375 ml":
        $price = 59.00;
        break;
    case "Cranberry Walnut BORDOLESE - 12/375 ml":
        $price = 59.00;
        break;
    case "Cucumber Melon BORDOLESE - 12/375 ml":
        $price = 59.00;
        break;
    case "Elderberry BORDOLESE - 12/375 ml":
        $price = 59.00;
        break;
    case "Espresso BORDOLESE - 12/375 ml":
        $price = 59.00;
        break;
    case "Fig BORDOLESE - 12/375 ml":
        $price = 59.00;
        break;
    case "Garlic BORDOLESE - 12/375 ml":
        $price = 59.00;
        break;
    case "Garlic Cilantro BORDOLESE - 12/375 ml":
        $price = 59.00;
        break;
    case "Grapefruit BORDOLESE - 12/375 ml":
        $price = 59.00;
        break;
    case "Green Apple BORDOLESE - 12/375 ml":
        $price = 59.00;
        break;
    case "Hickory BORDOLESE - 12/375 ml":
        $price = 59.00;
        break;
    case "Honey BORDOLESE - 12/375 ml":
        $price = 59.00;
        break;
    case "Honey Ginger BORDOLESE - 12/375 ml":
        $price = 59.00;
        break;
    case "Huckleberry BORDOLESE - 12/375 ml":
        $price = 59.00;
        break;
    case "Jalapeno BORDOLESE - 12/375 ml":
        $price = 59.00;
        break;
    case "Jalapeno Fig in 8 Star BORDOLESE - 12/375 ml":
        $price = 47.00;
        break;
    case "Jalapeno & Lime BORDOLESE - 12/375 ml":
        $price = 59.00;
        break;
    case "Lavender BORDOLESE - 12/375 ml":
        $price = 59.00;
        break;
    case "Lemongrass Mint BORDOLESE - 12/375 ml":
        $price = 59.00;
        break;
    case "Lemon BORDOLESE - 12/375 ml":
        $price = 59.00;
        break;
    case "Lemon Vanilla BORDOLESE - 12/375 ml":
        $price = 59.00;
        break;
    case "Mandarin Orange BORDOLESE - 12/375 ml":
        $price = 59.00;
        break;
    case "Mango BORDOLESE - 12/375 ml":
        $price = 59.00;
        break;
    case "Mocha Almond Fudge BORDOLESE - 12/375 ml":
        $price = 59.00;
        break;
    case "Orange BORDOLESE - 12/375 ml":
        $price = 59.00;
        break;
    case "Orange/Mango/Passionfruit BORDOLESE - 12/375 ml":
        $price = 59.00;
        break;
    case "Orange Vanilla BORDOLESE - 12/375 ml":
        $price = 59.00;
        break;
    case "Peach BORDOLESE - 12/375 ml":
        $price = 59.00;
        break;
    case "Pear BORDOLESE - 12/375 ml":
        $price = 59.00;
        break;
    case "Pecan Praline BORDOLESE - 12/375 ml":
        $price = 59.00;
        break;
    case "Pineapple BORDOLESE - 12/375 ml":
        $price = 59.00;
        break;
    case "Pomegranate BORDOLESE - 12/375 ml":
        $price = 59.00;
        break;
    case "Pumpkin Spice BORDOLESE - 12/375 ml":
        $price = 59.00;
        break;
    case "Raspberry BORDOLESE - 12/375 ml":
        $price = 59.00;
        break;
    case "Raspberry Ginger BORDOLESE - 12/375 ml":
        $price = 59.00;
        break;
    case "Strawberry BORDOLESE - 12/375 ml":
        $price = 59.00;
        break;
    case "Strawberry Peach BORDOLESE - 12/375 ml":
        $price = 59.00;
        break;
    case "Tangerine BORDOLESE - 12/375 ml":
        $price = 59.00;
        break;
    case "Black Truffle BORDOLESE - 12/375 ml":
        $price = 59.00;
        break;
    case "Vanilla BORDOLESE - 12/375 ml":
        $price = 59.00;
        break;
    case "Vanilla Fig BORDOLESE - 12/375 ml":
        $price = 59.00;
        break;
//bordeaux bottles
//oils 375
    case "Olive Oil Extra Virgin BORDEAUX - 12/375 ml":
        $price = 41.00;
        break;
    case "Australia - Leccino/Frantoio BORDEAUX - 12/375 ml":
        $price = 52.00;
        break;
    case "Australia - Frantoio BORDEAUX - 12/375 ml":
        $price = 52.00;
        break;
    case "California Arbequina BORDEAUX - 12/375 ml":
        $price = 76.00;
        break;
    case "California Arbosana BORDEAUX - 12/375 ml":
        $price = 73.00;
        break;
    case "California Mission BORDEAUX - 12/375 ml":
        $price = 73.00;
        break;
    case "California Arbequina/Arbosana/Koroneiki BORDEAUX - 12/375 ml":
        $price = 52.00;
        break;
    case "Chile - Arbequina BORDEAUX - 12/375 ml":
        $price = 42.00;
        break;
    case "Chile - Frantoio BORDEAUX - 12/375 ml":
        $price = 42.00;
        break;
    case "Chile - Leccino BORDEAUX - 12/375 ml":
        $price = 42.00;
        break;
    case "Chile - Picual BORDEAUX - 12/375 ml":
        $price = 42.00;
        break;
    case "Greece - Kalamata BORDEAUX - 12/375 ml":
        $price = 47.00;
        break;
    case "Greece - Koroneiki BORDEAUX - 12/375 ml":
        $price = 47.00;
        break;
    case "Italy - Umbrian region BORDEAUX - 12/375 ml":
        $price = 70.00;
        break;
    case "Italy - Calabria region - Carolea BORDEAUX - 12/375 ml":
        $price = 60.00;
        break;
    case "Italy (Puglia) - Coratina BORDEAUX - 12/375 ml":
        $price = 60.00;
        break;
    case "Italy (Puglia)- Ogliarola BORDEAUX - 12/375 ml":
        $price = 60.00;
        break;
    case "Spain - Hojiblanca BORDEAUX - 12/375 ml":
        $price = 49.00;
        break;
    case "Spain - Picual BORDEAUX - 12/375 ml":
        $price = 49.00;
        break;
    case "Spain - Signature blend  BORDEAUX - 12/375 ml":
        $price = 49.00;
        break;
    case "Tunisia - Chemlali/Chetoui BORDEAUX - 12/375 ml":
        $price = 41.00;
        break;
    //infused
    case "Black Pepper BORDEAUX - 12/375 ml":
        $price = 65.00;
        break;
    case "Garlic Oil BORDEAUX - 12/375 ml":
        $price = 55.00;
        break;
    case "Roasted Chili BORDEAUX - 12/375 ml":
        $price = 58.00;
        break;
    case "Oregano BORDEAUX - 12/375 ml":
        $price = 59.00;
        break;
    case "Lemon Pepper BORDEAUX - 12/375 ml":
        $price = 69.00;
        break;
    case "Jalapeno oil BORDEAUX - 12/375 ml":
        $price = 55.00;
        break;
    case "Basil BORDEAUX - 12/375 ml":
        $price = 56.00;
        break;
    case "Scallion BORDEAUX - 12/375 ml":
        $price = 60.00;
        break;
    case "Habenero BORDEAUX - 12/375 ml":
        $price = 73.00;
        break;
    case "Rosemary BORDEAUX - 12/375 ml":
        $price = 61.00;
        break;
    case "Chipotle BORDEAUX - 12/375 ml":
        $price = 58.00;
        break;
    //flavored olive oils
    case "Blood Orange BORDEAUX - 12/375 ml":
        $price = 44.00;
        break;
    case "Lemon oil BORDEAUX - 12/375 ml":
        $price = 50.00;
        break;
    case "Lime BORDEAUX - 12/375 ml":
        $price = 51.00;
        break;
    case "Orange oil BORDEAUX - 12/375 ml":
        $price = 44.00;
        break;
    case "Tangerine oil BORDEAUX - 12/375 ml":
        $price = 45.00;
        break;
    case "Butter BORDEAUX - 12/375 ml":
        $price = 55.00;
        break;
    case "Cranberry Walnut oil BORDEAUX - 12/375 ml":
        $price = 45.00;
        break;
    case "Arbequina Rhubarb BORDEAUX - 12/375 ml":
        $price = 44.00;
        break;
    case "Sage Onion BORDEAUX - 12/375 ml":
        $price = 62.00;
        break;
    case "Sundried Tomato BORDEAUX - 12/375 ml":
        $price = 52.00;
        break;
    case "White Truffle BORDEAUX - 12/375 ml":
        $price = 111.00;
        break;
    case "Black Truffle oil BORDEAUX - 12/375 ml":
        $price = 109.00;
        break;
    case "Hickory oil BORDEAUX - 12/375 ml":
        $price = 52.00;
        break;
    case "Vanilla oil BORDEAUX - 12/375 ml":
        $price = 52.00;
        break;
    //fused
    case "Garlic Mushroom BORDEAUX - 12/375 ml":
        $price = 62.00;
        break;
    case "Basil/Lemongrass BORDEAUX - 12/375 ml":
        $price = 62.00;
        break;
    case "Rosemary with Lavender BORDEAUX - 12/375 ml":
        $price = 69.00;
        break;
    case "Tuscan Herb BORDEAUX - 12/375 ml":
        $price = 67.00;
        break;
    case "Herbs d Provence BORDEAUX - 12/375 ml":
        $price = 65.00;
        break;
    case "Citrus Habanero BORDEAUX - 12/375 ml":
        $price = 58.00;
        break;
    case "Garlic Roasted Chili BORDEAUX - 12/375 ml":
        $price = 61.00;
        break;
    //specialty oils
    case "Almond Oil BORDEAUX - 12/375 ml":
        $price = 51.00;
        break;
    case "Apricot Oil BORDEAUX - 12/375 ml":
        $price = 53.00;
        break;
    case "Avocado Oil BORDEAUX - 12/375 ml":
        $price = 47.00;
        break;
    case "Flaxseed Oil BORDEAUX - 12/375 ml":
        $price = 38.00;
        break;
    case "Grapeseed Oil BORDEAUX - 12/375 ml":
        $price = 33.00;
        break;
    case "Hempseed Oil BORDEAUX - 12/375 ml":
        $price = 82.00;
        break;
    case "Macadamia Nut Oil BORDEAUX - 12/375 ml":
        $price = 59.00;
        break;
    case "Toasted Sesame Oil BORDEAUX - 12/375 ml":
        $price = 40.00;
        break;
    case "Walnut Oil BORDEAUX - 12/375 ml":
        $price = 37.00;
        break;
//organics
    case "Olive Oil, Extra Virgin, Organic , Italy BORDEAUX - 12/375 ml":
        $price = 51.00;
        break;
    case "Olive Oil, Extra Virgin, Organic , Country Varies BORDEAUX - 12/375 ml":
        $price = 45.00;
        break;
    case "Canola BORDEAUX - 12/375 ml":
        $price = 31.00;
        break;
    case "Soybean Oil BORDEAUX - 12/375 ml":
        $price = 25.00;
        break;
    case "Sunflower BORDEAUX - 12/375 ml":
        $price = 41.00;
        break;
    case "Sesame Toasted BORDEAUX - 12/375 ml":
        $price = 39.00;
        break;
    case "Sesame, Virgin BORDEAUX - 12/375 ml":
        $price = 39.00;
        break;
    //vinegars
    case "4 Star Balsamic BORDEAUX - 12/375 ml":
        $price = 27.00;
        break;
    case "8 Star Balsamic BORDEAUX - 12/375 ml":
        $price = 37.00;
        break;
    case "12 Star Balsamic BORDEAUX - 12/375 ml":
        $price = 41.00;
        break;
    case "25 Star Dark Balsamic BORDEAUX - 12/375 ml":
        $price = 62.00;
        break;
    case "25 Star White Balsamic BORDEAUX - 12/375 ml":
        $price = 62.00;
        break;
    case "6 Star White Balsamic BORDEAUX - 12/375 ml":
        $price = 35.00;
        break;
    case "Red Wine Vinegar - Domestic - 50 Grain BORDEAUX - 12/375 ml":
        $price = 23.00;
        break;
    case "Organic Premium Quality Balsamic - Ltd Edition BORDEAUX - 12/375 ml":
        $price = 64.00;
        break;
    case "Serrano Chile Vinegar BORDEAUX - 12/375 ml":
        $price = 67.00;
        break;
    case "Lambrusco Vinegar BORDEAUX - 12/375 ml":
        $price = 62.00;
        break;
//flavored balsamic
    case "Almond Creme BORDEAUX - 12/375 ml":
        $price = 62.00;
        break;
    case "Apricot BORDEAUX - 12/375 ml":
        $price = 62.00;
        break;
    case "Apricot in White BV BORDEAUX - 12/375 ml":
        $price = 62.00;
        break;
    case "Bittersweet Chocolate with Orange BORDEAUX - 12/375 ml":
        $price = 62.00;
        break;
    case "Blackberry BORDEAUX - 12/375 ml":
        $price = 62.00;
        break;
    case "Blackberry Ginger BORDEAUX - 12/375 ml":
        $price = 62.00;
        break;
    case "Black Currant BORDEAUX - 12/375 ml":
        $price = 62.00;
        break;
    case "Black Walnut BORDEAUX - 12/375 ml":
        $price = 62.00;
        break;
    case "Blueberry BORDEAUX - 12/375 ml":
        $price = 62.00;
        break;
    case "Bordeaux Cherry BORDEAUX - 12/375 ml":
        $price = 62.00;
        break;
    case "Chili BORDEAUX - 12/375 ml":
        $price = 62.00;
        break;
    case "Chocolate BORDEAUX - 12/375 ml":
        $price = 62.00;
        break;
    case "Chocolate Covered Cherries BORDEAUX - 12/375 ml":
        $price = 62.00;
        break;
    case "Chocolate Jalapeno BORDEAUX - 12/375 ml":
        $price = 62.00;
        break;
    case "Chocolate Marshmallow BORDEAUX - 12/375 ml":
        $price = 62.00;
        break;
    case "Coconut BORDEAUX - 12/375 ml":
        $price = 62.00;
        break;
    case "Cranberry Orange BORDEAUX - 12/375 ml":
        $price = 62.00;
        break;
    case "Cranberry Walnut BORDEAUX - 12/375 ml":
        $price = 62.00;
        break;
    case "Cucumber Melon BORDEAUX - 12/375 ml":
        $price = 62.00;
        break;
    case "Elderberry BORDEAUX - 12/375 ml":
        $price = 62.00;
        break;
    case "Espresso BORDEAUX - 12/375 ml":
        $price = 62.00;
        break;
    case "Fig BORDEAUX - 12/375 ml":
        $price = 62.00;
        break;
    case "Garlic BORDEAUX - 12/375 ml":
        $price = 62.00;
        break;
    case "Garlic Cilantro BORDEAUX - 12/375 ml":
        $price = 62.00;
        break;
    case "Grapefruit BORDEAUX - 12/375 ml":
        $price = 62.00;
        break;
    case "Green Apple BORDEAUX - 12/375 ml":
        $price = 62.00;
        break;
    case "Hickory BORDEAUX - 12/375 ml":
        $price = 62.00;
        break;
    case "Honey BORDEAUX - 12/375 ml":
        $price = 62.00;
        break;
    case "Honey Ginger BORDEAUX - 12/375 ml":
        $price = 62.00;
        break;
    case "Huckleberry BORDEAUX - 12/375 ml":
        $price = 62.00;
        break;
    case "Jalapeno BORDEAUX - 12/375 ml":
        $price = 62.00;
        break;
    case "Jalapeno Fig in 8 Star BORDEAUX - 12/375 ml":
        $price = 51.00;
        break;
    case "Jalapeno & Lime BORDEAUX - 12/375 ml":
        $price = 62.00;
        break;
    case "Lavender BORDEAUX - 12/375 ml":
        $price = 62.00;
        break;
    case "Lemongrass Mint BORDEAUX - 12/375 ml":
        $price = 62.00;
        break;
    case "Lemon BORDEAUX - 12/375 ml":
        $price = 62.00;
        break;
    case "Lemon Vanilla BORDEAUX - 12/375 ml":
        $price = 62.00;
        break;
    case "Mandarin Orange BORDEAUX - 12/375 ml":
        $price = 62.00;
        break;
    case "Mango BORDEAUX - 12/375 ml":
        $price = 62.00;
        break;
    case "Mocha Almond Fudge BORDEAUX - 12/375 ml":
        $price = 62.00;
        break;
    case "Orange BORDEAUX - 12/375 ml":
        $price = 62.00;
        break;
    case "Orange/Mango/Passionfruit BORDEAUX - 12/375 ml":
        $price = 62.00;
        break;
    case "Orange Vanilla BORDEAUX - 12/375 ml":
        $price = 62.00;
        break;
    case "Peach BORDEAUX - 12/375 ml":
        $price = 62.00;
        break;
    case "Pear BORDEAUX - 12/375 ml":
        $price = 62.00;
        break;
    case "Pecan Praline BORDEAUX - 12/375 ml":
        $price = 62.00;
        break;
    case "Pineapple BORDEAUX - 12/375 ml":
        $price = 62.00;
        break;
    case "Pomegranate BORDEAUX - 12/375 ml":
        $price = 62.00;
        break;
    case "Pumpkin Spice BORDEAUX - 12/375 ml":
        $price = 62.00;
        break;
    case "Raspberry BORDEAUX - 12/375 ml":
        $price = 62.00;
        break;
    case "Raspberry Ginger BORDEAUX - 12/375 ml":
        $price = 62.00;
        break;
    case "Strawberry BORDEAUX - 12/375 ml":
        $price = 62.00;
        break;
    case "Strawberry Peach BORDEAUX - 12/375 ml":
        $price = 62.00;
        break;
    case "Tangerine BORDEAUX - 12/375 ml":
        $price = 62.00;
        break;
    case "Black Truffle BORDEAUX - 12/375 ml":
        $price = 62.00;
        break;
    case "Vanilla BORDEAUX - 12/375 ml":
        $price = 62.00;
        break;
    case "Vanilla Fig BORDEAUX - 12/375 ml":
        $price = 62.00;
        break;
    //olive oils bordeaux 500ml
    case "Olive Oil Extra Virgin BORDEAUX - 12/500 ml":
        $price = 50.00;
        break;
    case "Australia - Leccino/Frantoio BORDEAUX - 12/500 ml":
        $price = 65.00;
        break;
    case "Australia - Frantoio BORDEAUX - 12/500 ml":
        $price = 65.00;
        break;
    case "California Arbequina BORDEAUX - 12/500 ml":
        $price = 98.00;
        break;
    case "California Arbosana BORDEAUX - 12/500 ml":
        $price = 93.00;
        break;
    case "California Mission BORDEAUX - 12/500 ml":
        $price = 93.00;
        break;
    case "California Arbequina/Arbosana/Koroneiki BORDEAUX - 12/500 ml":
        $price = 65.00;
        break;
    case "Chile - Arbequina BORDEAUX - 12/500 ml":
        $price = 51.00;
        break;
    case "Chile - Frantoio BORDEAUX - 12/500 ml":
        $price = 51.00;
        break;
    case "Chile - Leccino BORDEAUX - 12/500 ml":
        $price = 51.00;
        break;
    case "Chile - Picual BORDEAUX - 12/500 ml":
        $price = 51.00;
        break;
    case "Greece - Kalamata BORDEAUX - 12/500 ml":
        $price = 58.00;
        break;
    case "Greece - Koroneiki BORDEAUX - 12/500 ml":
        $price = 58.00;
        break;
    case "Italy - Umbrian region BORDEAUX - 12/500 ml":
        $price = 89.00;
        break;
    case "Italy - Calabria region - Carolea BORDEAUX - 12/500 ml":
        $price = 75.00;
        break;
    case "Italy (Puglia) - Coratina BORDEAUX - 12/500 ml":
        $price = 76.00;
        break;
    case "Italy (Puglia)- Ogliarola BORDEAUX - 12/500 ml":
        $price = 76.00;
        break;
    case "Spain - Hojiblanca BORDEAUX - 12/500 ml":
        $price = 60.00;
        break;
    case "Spain - Picual BORDEAUX - 12/500 ml":
        $price = 60.00;
        break;
    case "Spain - Signature blend BORDEAUX - 12/500 ml":
        $price = 60.00;
        break;
    case "Tunisia - Chemlali/Chetoui BORDEAUX - 12/500 ml":
        $price = 50.00;
        break;
    //infused
    case "Black Pepper BORDEAUX - 12/500 ml":
        $price = 82.00;
        break;
    case "Garlic Oil BORDEAUX - 12/500 ml":
        $price = 69.00;
        break;
    case "Roasted Chili BORDEAUX - 12/500 ml":
        $price = 73.00;
        break;
    case "Oregano BORDEAUX - 12/500 ml":
        $price = 75.00;
        break;
    case "Lemon Pepper BORDEAUX - 12/500 ml":
        $price = 88.00;
        break;
    case "Jalapeno oil BORDEAUX - 12/500 ml":
        $price = 69.00;
        break;
    case "Basil BORDEAUX - 12/500 ml":
        $price = 70.00;
        break;
    case "Scallion BORDEAUX - 12/500 ml":
        $price = 76.00;
        break;
    case "Habenero BORDEAUX - 12/500 ml":
        $price = 93.00;
        break;
    case "Rosemary BORDEAUX - 12/500 ml":
        $price = 78.00;
        break;
    case "Chipotle BORDEAUX - 12/500 ml":
        $price = 73.00;
        break;
    //flavored oils
    case "Blood Orange BORDEAUX - 12/500 ml":
        $price = 54.00;
        break;
    case "Lemon oil BORDEAUX - 12/500 ml":
        $price = 62.00;
        break;
    case "Lime BORDEAUX - 12/500 ml":
        $price = 64.00;
        break;
    case "Orange oil BORDEAUX - 12/500 ml":
        $price = 54.00;
        break;
    case "Tangerine oil BORDEAUX - 12/500 ml":
        $price = 56.00;
        break;
    case "Butter BORDEAUX - 12/500 ml":
        $price = 69.00;
        break;
    case "Cranberry Walnut oil BORDEAUX - 12/500 ml":
        $price = 55.00;
        break;
    case "Arbequina Rhubarb BORDEAUX - 12/500 ml":
        $price = 55.00;
        break;
    case "Sage Onion BORDEAUX - 12/500 ml":
        $price = 78.00;
        break;
    case "Sundried Tomato BORDEAUX - 12/500 ml":
        $price = 65.00;
        break;
    case "White Truffle BORDEAUX - 12/500 ml":
        $price = 143.00;
        break;
    case "Black Truffle oil BORDEAUX - 12/500 ml":
        $price = 141.00;
        break;
    case "Hickory oil BORDEAUX - 12/500 ml":
        $price = 65.00;
        break;
    case "Vanilla oil BORDEAUX - 12/500 ml":
        $price = 65.00;
        break;
    //fused
    case "Garlic Mushroom BORDEAUX - 12/500 ml":
        $price = 78.00;
        break;
    case "Basil/Lemongrass BORDEAUX - 12/500 ml":
        $price = 79.00;
        break;
    case "Rosemary with Lavender BORDEAUX - 12/500 ml":
        $price = 88.00;
        break;
    case "Tuscan Herb BORDEAUX - 12/500 ml":
        $price = 85.00;
        break;
    case "Herbs d Provence BORDEAUX - 12/500 ml":
        $price = 83.00;
        break;
    case "Citrus Habanero BORDEAUX - 12/500 ml":
        $price = 73.00;
        break;
    case "Garlic Roasted Chili BORDEAUX - 12/500 ml":
        $price = 78.00;
        break;
    //specialty oils
    case "Almond Oil BORDEAUX - 12/500 ml":
        $price = 64.00;
        break;
    case "Apricot Oil BORDEAUX - 12/500 ml":
        $price = 67.00;
        break;
    case "Avocado Oil BORDEAUX - 12/500 ml":
        $price = 58.00;
        break;
    case "Flaxseed Oil BORDEAUX - 12/500 ml":
        $price = 47.00;
        break;
    case "Grapeseed Oil BORDEAUX - 12/500 ml":
        $price = 40.00;
        break;
    case "Hempseed Oil BORDEAUX - 12/500 ml":
        $price = 106.00;
        break;
    case "Macadamia Nut Oil BORDEAUX - 12/500 ml":
        $price = 75.00;
        break;
    case "Toasted Sesame Oil BORDEAUX - 12/500 ml":
        $price = 50.00;
        break;
    case "Walnut Oil BORDEAUX - 12/500 ml":
        $price = 45.00;
        break;
//organics
    case "Olive Oil, Extra Virgin, Organic , Italy BORDEAUX - 12/500 ml":
        $price = 65.00;
        break;
    case "Olive Oil, Extra Virgin, Organic , Country Varies BORDEAUX - 12/500 ml":
        $price = 56.00;
        break;
    case "Canola BORDEAUX - 12/500 ml":
        $price = 37.00;
        break;
    case "Soybean Oil BORDEAUX - 12/500 ml":
        $price = 29.00;
        break;
    case "Sunflower BORDEAUX - 12/500 ml":
        $price = 51.00;
        break;
    case "Sesame Toasted BORDEAUX - 12/500 ml":
        $price = 48.00;
        break;
    case "Sesame, Virgin BORDEAUX - 12/500 ml":
        $price = 48.00;
        break;
    //vinegars
    case "4 Star Balsamic BORDEAUX - 12/500 ml":
        $price = 32.00;
        break;
    case "8 Star Balsamic BORDEAUX - 12/500 ml":
        $price = 44.00;
        break;
    case "12 Star Balsamic BORDEAUX - 12/500 ml":
        $price = 50.00;
        break;
    case "25 Star Dark Balsamic BORDEAUX - 12/500 ml":
        $price = 77.00;
        break;
    case "25 Star White Balsamic BORDEAUX - 12/500 ml":
        $price = 77.00;
        break;
    case "6 Star White Balsamic BORDEAUX - 12/500 ml":
        $price = 42.00;
        break;
    case "Red Wine Vinegar - Domestic - 50 Grain BORDEAUX - 12/500 ml":
        $price = 25.00;
        break;
    case "Organic Premium Quality Balsamic - Ltd Edition BORDEAUX - 12/500 ml":
        $price = 81.00;
        break;
    case "Serrano Chile Vinegar BORDEAUX - 12/500 ml":
        $price = 85.00;
        break;
    case "Lambrusco Vinegar BORDEAUX - 12/500 ml":
        $price = 77.00;
        break;
    //flavored balsamic
    case "Almond Creme BORDEAUX - 12/500 ml":
        $price = 77.00;
        break;
    case "Apricot BORDEAUX - 12/500 ml":
        $price = 77.00;
        break;
    case "Bittersweet Chocolate with Orange BORDEAUX - 12/500 ml":
        $price = 77.00;
        break;
    case "Blackberry BORDEAUX - 12/500 ml":
        $price = 77.00;
        break;
    case "Blackberry Ginger BORDEAUX - 12/500 ml":
        $price = 77.00;
        break;
    case "Black Currant BORDEAUX - 12/500 ml":
        $price = 77.00;
        break;
    case "Black Walnut BORDEAUX - 12/500 ml":
        $price = 77.00;
        break;
    case "Blueberry BORDEAUX - 12/500 ml":
        $price = 77.00;
        break;
    case "Bordeaux Cherry BORDEAUX - 12/500 ml":
        $price = 77.00;
        break;
    case "Chili BORDEAUX - 12/500 ml":
        $price = 77.00;
        break;
    case "Chocolate BORDEAUX - 12/500 ml":
        $price = 77.00;
        break;
    case "Chocolate Covered Cherries BORDEAUX - 12/500 ml":
        $price = 77.00;
        break;
    case "Chocolate Jalapeno BORDEAUX - 12/500 ml":
        $price = 77.00;
        break;
    case "Chocolate Marshmallow BORDEAUX - 12/500 ml":
        $price = 77.00;
        break;
    case "Coconut BORDEAUX - 12/500 ml":
        $price = 77.00;
        break;
    case "Cranberry Orange BORDEAUX - 12/500 ml":
        $price = 77.00;
        break;
    case "Cranberry Walnut BORDEAUX - 12/500 ml":
        $price = 77.00;
        break;
    case "Cucumber Melon BORDEAUX - 12/500 ml":
        $price = 77.00;
        break;
    case "Elderberry BORDEAUX - 12/500 ml":
        $price = 77.00;
        break;
    case "Espresso BORDEAUX - 12/500 ml":
        $price = 77.00;
        break;
    case "Fig BORDEAUX - 12/500 ml":
        $price = 77.00;
        break;
    case "Garlic BORDEAUX - 12/500 ml":
        $price = 77.00;
        break;
    case "Garlic Cilantro BORDEAUX - 12/500 ml":
        $price = 77.00;
        break;
    case "Grapefruit BORDEAUX - 12/500 ml":
        $price = 77.00;
        break;
    case "Green Apple BORDEAUX - 12/500 ml":
        $price = 77.00;
        break;
    case "Hickory BORDEAUX - 12/500 ml":
        $price = 77.00;
        break;
    case "Honey BORDEAUX - 12/500 ml":
        $price = 77.00;
        break;
    case "Honey Ginger BORDEAUX - 12/500 ml":
        $price = 77.00;
        break;
    case "Huckleberry BORDEAUX - 12/500 ml":
        $price = 77.00;
        break;
    case "Jalapeno BORDEAUX - 12/500 ml":
        $price = 77.00;
        break;
    case "Jalapeno Fig in 8 Star BORDEAUX - 12/500 ml":
        $price = 62.00;
        break;
    case "Jalapeno & Lime BORDEAUX - 12/500 ml":
        $price = 77.00;
        break;
    case "Lavender BORDEAUX - 12/500 ml":
        $price = 77.00;
        break;
    case "Lemongrass Mint BORDEAUX - 12/500 ml":
        $price = 77.00;
        break;
    case "Lemon BORDEAUX - 12/500 ml":
        $price = 77.00;
        break;
    case "Lemon Vanilla BORDEAUX - 12/500 ml":
        $price = 77.00;
        break;
    case "Mandarin Orange BORDEAUX - 12/500 ml":
        $price = 77.00;
        break;
    case "Mango BORDEAUX - 12/500 ml":
        $price = 77.00;
        break;
    case "Mocha Almond Fudge BORDEAUX - 12/500 ml":
        $price = 77.00;
        break;
    case "Orange BORDEAUX - 12/500 ml":
        $price = 77.00;
        break;
    case "Orange/Mango/Passionfruit BORDEAUX - 12/500 ml":
        $price = 77.00;
        break;
    case "Orange Vanilla BORDEAUX - 12/500 ml":
        $price = 77.00;
        break;
    case "Peach BORDEAUX - 12/500 ml":
        $price = 77.00;
        break;
    case "Pear BORDEAUX - 12/500 ml":
        $price = 77.00;
        break;
    case "Pecan Praline BORDEAUX - 12/500 ml":
        $price = 77.00;
        break;
    case "Pineapple BORDEAUX - 12/500 ml":
        $price = 77.00;
        break;
    case "Pomegranate BORDEAUX - 12/500 ml":
        $price = 77.00;
        break;
    case "Pumpkin Spice BORDEAUX - 12/500 ml":
        $price = 77.00;
        break;
    case "Raspberry BORDEAUX - 12/500 ml":
        $price = 77.00;
        break;
    case "Raspberry Ginger BORDEAUX - 12/500 ml":
        $price = 77.00;
        break;
    case "Strawberry BORDEAUX - 12/500 ml":
        $price = 77.00;
        break;
    case "Strawberry Peach BORDEAUX - 12/500 ml":
        $price = 77.00;
        break;
    case "Tangerine BORDEAUX - 12/500 ml":
        $price = 77.00;
        break;
    case "Black Truffle BORDEAUX - 12/500 ml":
        $price = 77.00;
        break;
    case "Vanilla BORDEAUX - 12/500 ml":
        $price = 77.00;
        break;
    case "Vanilla Fig BORDEAUX - 12/500 ml":
        $price = 77.00;
        break;
    //oils bordeaux 750ml
    case "Olive Oil Extra Virgin BORDEAUX - 12/750 ml":
        $price = 70.00;
        break;
    case "Australia - Leccino/Frantoio BORDEAUX - 12/750 ml":
        $price = 93.00;
        break;
    case "Australia - Frantoio BORDEAUX - 12/750 ml":
        $price = 93.00;
        break;
    case "California Arbequina BORDEAUX - 12/750 ml":
        $price = 144.00;
        break;
    case "California Arbosana BORDEAUX - 12/750 ml":
        $price = 137.00;
        break;
    case "California Mission BORDEAUX - 12/750 ml":
        $price = 137.00;
        break;
    case "California Arbequina/Arbosana/Koroneiki BORDEAUX - 12/750 ml":
        $price = 94.00;
        break;
    case "Chile - Arbequina BORDEAUX - 12/750 ml":
        $price = 72.00;
        break;
    case "Chile - Frantoio BORDEAUX - 12/750 ml":
        $price = 72.00;
        break;
    case "Chile - Leccino BORDEAUX - 12/750 ml":
        $price = 72.00;
        break;
    case "Chile - Picual BORDEAUX - 12/750 ml":
        $price = 72.00;
        break;
    case "Greece - Kalamata BORDEAUX - 12/750 ml":
        $price = 83.00;
        break;
    case "Greece - Koroneiki BORDEAUX - 12/750 ml":
        $price = 83.00;
        break;
    case "Italy - Umbrian region BORDEAUX - 12/750 ml":
        $price = 132.00;
        break;
    case "Italy - Calabria region - Carolea BORDEAUX - 12/750 ml":
        $price = 110.00;
        break;
    case "Italy (Puglia) - Coratina BORDEAUX - 12/750 ml":
        $price = 111.00;
        break;
    case "Italy (Puglia)- Ogliarola BORDEAUX - 12/750 ml":
        $price = 110.00;
        break;
    case "Spain - Hojiblanca BORDEAUX - 12/750 ml":
        $price = 87.00;
        break;
    case "Spain - Picual BORDEAUX - 12/750 ml":
        $price = 87.00;
        break;
    case "Spain - Signature blend BORDEAUX - 12/750 ml":
        $price = 87.00;
        break;
    case "Tunisia - Chemlali/Chetoui BORDEAUX - 12/750 ml":
        $price = 70.00;
        break;
    //infused
    case "Black Pepper BORDEAUX - 12/750 ml":
        $price = 121.00;
        break;
    case "Garlic Oil BORDEAUX - 12/750 ml":
        $price = 101.00;
        break;
    case "Roasted Chili BORDEAUX - 12/750 ml":
        $price = 107.00;
        break;
    case "Oregano BORDEAUX - 12/750 ml":
        $price = 110.00;
        break;
    case "Lemon Pepper BORDEAUX - 12/750 ml":
        $price = 130.00;
        break;
    case "Jalapeno oil BORDEAUX - 12/750 ml":
        $price = 101.00;
        break;
    case "Basil BORDEAUX - 12/750 ml":
        $price = 103.00;
        break;
    case "Scallion BORDEAUX - 12/750 ml":
        $price = 112.00;
        break;
    case "Habenero BORDEAUX - 12/750 ml":
        $price = 138.00;
        break;
    case "Rosemary BORDEAUX - 12/750 ml":
        $price = 114.00;
        break;
    case "Chipotle BORDEAUX - 12/750 ml":
        $price = 107.00;
        break;
    //flavored olive oils
    case "Blood Orange BORDEAUX - 12/750 ml":
        $price = 77.00;
        break;
    case "Lemon oil BORDEAUX - 12/750 ml":
        $price = 90.00;
        break;
    case "Lime BORDEAUX - 12/750 ml":
        $price = 92.00;
        break;
    case "Orange oil BORDEAUX - 12/750 ml":
        $price = 77.00;
        break;
    case "Tangerine oil BORDEAUX - 12/750 ml":
        $price = 80.00;
        break;
    case "Butter BORDEAUX - 12/750 ml":
        $price = 100.00;
        break;
    case "Cranberry Walnut oil BORDEAUX - 12/750 ml":
        $price = 79.00;
        break;
    case "Arbequina Rhubarb BORDEAUX - 12/750 ml":
        $price = 78.00;
        break;
    case "Sage Onion BORDEAUX - 12/750 ml":
        $price = 115.00;
        break;
    case "Sundried Tomato BORDEAUX - 12/750 ml":
        $price = 94.00;
        break;
    case "White Truffle BORDEAUX - 12/750 ml":
        $price = 215.00;
        break;
    case "Black Truffle oil BORDEAUX - 12/750 ml":
        $price = 211.00;
        break;
    case "Hickory oil BORDEAUX - 12/750 ml":
        $price = 94.00;
        break;
    case "Vanilla oil BORDEAUX - 12/750 ml":
        $price = 94.00;
        break;
    //fused
    case "Garlic Mushroom BORDEAUX - 12/750 ml":
        $price = 114.00;
        break;
    case "Basil/Lemongrass BORDEAUX - 12/750 ml":
        $price = 116.00;
        break;
    case "Rosemary with Lavender BORDEAUX - 12/750 ml":
        $price = 130.00;
        break;
    case "Tuscan Herb BORDEAUX - 12/750 ml":
        $price = 126.00;
        break;
    case "Herbs d Provence BORDEAUX - 12/750 ml":
        $price = 121.00;
        break;
    case "Citrus Habanero BORDEAUX - 12/750 ml":
        $price = 106.00;
        break;
    case "Garlic Roasted Chili BORDEAUX - 12/750 ml":
        $price = 113.00;
        break;
    //specialty oils
    case "Almond Oil BORDEAUX - 12/750 ml":
        $price = 93.00;
        break;
    case "Apricot Oil BORDEAUX - 12/750 ml":
        $price = 97.00;
        break;
    case "Avocado Oil BORDEAUX - 12/750 ml":
        $price = 84.00;
        break;
    case "Flaxseed Oil BORDEAUX - 12/750 ml":
        $price = 66.00;
        break;
    case "Grapeseed Oil BORDEAUX - 12/750 ml":
        $price = 56.00;
        break;
    case "Hempseed Oil BORDEAUX - 12/750 ml":
        $price = 158.00;
        break;
    case "Macadamia Nut Oil BORDEAUX - 12/750 ml":
        $price = 110.00;
        break;
    case "Toasted Sesame Oil BORDEAUX - 12/750 ml":
        $price = 71.00;
        break;
    case "Walnut Oil BORDEAUX - 12/750 ml":
        $price = 63.00;
        break;
    //organics
    case "Olive Oil, Extra Virgin, Organic , Italy BORDEAUX - 12/750 ml":
        $price = 94.00;
        break;
    case "Olive Oil, Extra Virgin, Organic , Country Varies BORDEAUX - 12/750 ml":
        $price = 80.00;
        break;
    case "Canola BORDEAUX - 12/750 ml":
        $price = 51.00;
        break;
    case "Soybean Oil BORDEAUX - 12/750 ml":
        $price = 39.00;
        break;
    case "Sunflower BORDEAUX - 12/750 ml":
        $price = 72.00;
        break;
    case "Sesame Toasted BORDEAUX - 12/750 ml":
        $price = 68.00;
        break;
    case "Sesame, Virgin BORDEAUX - 12/750 ml":
        $price = 68.00;
        break;
    //vinegars
    case "4 Star Balsamic BORDEAUX - 12/750 ml":
        $price = 42.00;
        break;
    case "8 Star Balsamic BORDEAUX - 12/750 ml":
        $price = 60.00;
        break;
    case "12 Star Balsamic BORDEAUX - 12/750 ml":
        $price = 69.00;
        break;
    case "25 Star Dark Balsamic BORDEAUX - 12/750 ml":
        $price = 110.00;
        break;
    case "25 Star White Balsamic BORDEAUX - 12/750 ml":
        $price = 110.00;
        break;
    case "6 Star White Balsamic BORDEAUX - 12/750 ml":
        $price = 58.00;
        break;
    case "Red Wine Vinegar - Domestic - 50 Grain BORDEAUX - 12/750 ml":
        $price = 31.00;
        break;
    case "Organic Premium Quality Balsamic - Ltd Edition BORDEAUX - 12/750 ml":
        $price = 119.00;
        break;
    case "Serrano Chile Vinegar BORDEAUX - 12/750 ml":
        $price = 123.00;
        break;
    case "Lambrusco Vinegar BORDEAUX - 12/750 ml":
        $price = 110.00;
        break;
    //flavored balsamic
    case "Almond Creme BORDEAUX - 12/750 ml":
        $price = 110.00;
        break;
    case "Apricot BORDEAUX - 12/750 ml":
        $price = 110.00;
        break;
    case "Bittersweet Chocolate with Orange BORDEAUX - 12/750 ml":
        $price = 110.00;
        break;
    case "Blackberry BORDEAUX - 12/750 ml":
        $price = 110.00;
        break;
    case "Blackberry Ginger BORDEAUX - 12/750 ml":
        $price = 110.00;
        break;
    case "Black Currant BORDEAUX - 12/750 ml":
        $price = 110.00;
        break;
    case "Black Walnut BORDEAUX - 12/750 ml":
        $price = 110.00;
        break;
    case "Blueberry BORDEAUX - 12/750 ml":
        $price = 110.00;
        break;
    case "Bordeaux Cherry BORDEAUX - 12/750 ml":
        $price = 110.00;
        break;
    case "Chili BORDEAUX - 12/750 ml":
        $price = 110.00;
        break;
    case "Chocolate BORDEAUX - 12/750 ml":
        $price = 110.00;
        break;
    case "Chocolate Covered Cherries BORDEAUX - 12/750 ml":
        $price = 110.00;
        break;
    case "Chocolate Jalapeno BORDEAUX - 12/750 ml":
        $price = 110.00;
        break;
    case "Chocolate Marshmallow BORDEAUX - 12/750 ml":
        $price = 110.00;
        break;
    case "Coconut BORDEAUX - 12/750 ml":
        $price = 110.00;
        break;
    case "Cranberry Orange BORDEAUX - 12/750 ml":
        $price = 110.00;
        break;
    case "Cranberry Walnut BORDEAUX - 12/750 ml":
        $price = 110.00;
        break;
    case "Cucumber Melon BORDEAUX - 12/750 ml":
        $price = 110.00;
        break;
    case "Elderberry BORDEAUX - 12/750 ml":
        $price = 110.00;
        break;
    case "Espresso BORDEAUX - 12/750 ml":
        $price = 110.00;
        break;
    case "Fig BORDEAUX - 12/750 ml":
        $price = 110.00;
        break;
    case "Garlic BORDEAUX - 12/750 ml":
        $price = 110.00;
        break;
    case "Garlic Cilantro BORDEAUX - 12/750 ml":
        $price = 110.00;
        break;
    case "Grapefruit BORDEAUX - 12/750 ml":
        $price = 110.00;
        break;
    case "Green Apple BORDEAUX - 12/750 ml":
        $price = 110.00;
        break;
    case "Hickory BORDEAUX - 12/750 ml":
        $price = 110.00;
        break;
    case "Honey BORDEAUX - 12/750 ml":
        $price = 110.00;
        break;
    case "Honey Ginger BORDEAUX - 12/750 ml":
        $price = 110.00;
        break;
    case "Huckleberry BORDEAUX - 12/750 ml":
        $price = 110.00;
        break;
    case "Jalapeno BORDEAUX - 12/750 ml":
        $price = 110.00;
        break;
    case "Jalapeno Fig in 8 Star BORDEAUX - 12/750 ml":
        $price = 110.00;
        break;
    case "Jalapeno & Lime BORDEAUX - 12/750 ml":
        $price = 110.00;
        break;
    case "Lavender BORDEAUX - 12/750 ml":
        $price = 110.00;
        break;
    case "Lemongrass Mint BORDEAUX - 12/750 ml":
        $price = 110.00;
        break;
    case "Lemon BORDEAUX - 12/750 ml":
        $price = 110.00;
        break;
    case "Lemon Vanilla BORDEAUX - 12/750 ml":
        $price = 110.00;
        break;
    case "Mandarin Orange BORDEAUX - 12/750 ml":
        $price = 110.00;
        break;
    case "Mango BORDEAUX - 12/750 ml":
        $price = 110.00;
        break;
    case "Mocha Almond Fudge BORDEAUX - 12/750 ml":
        $price = 110.00;
        break;
    case "Orange BORDEAUX - 12/750 ml":
        $price = 110.00;
        break;
    case "Orange/Mango/Passionfruit BORDEAUX - 12/750 ml":
        $price = 110.00;
        break;
    case "Orange Vanilla BORDEAUX - 12/750 ml":
        $price = 110.00;
        break;
    case "Peach BORDEAUX - 12/750 ml":
        $price = 110.00;
        break;
    case "Pear BORDEAUX - 12/750 ml":
        $price = 110.00;
        break;
    case "Pecan Praline BORDEAUX - 12/750 ml":
        $price = 110.00;
        break;
    case "Pineapple BORDEAUX - 12/750 ml":
        $price = 110.00;
        break;
    case "Pomegranate BORDEAUX - 12/750 ml":
        $price = 110.00;
        break;
    case "Pumpkin Spice BORDEAUX - 12/750 ml":
        $price = 110.00;
        break;
    case "Raspberry BORDEAUX - 12/750 ml":
        $price = 110.00;
        break;
    case "Raspberry Ginger BORDEAUX - 12/750 ml":
        $price = 110.00;
        break;
    case "Strawberry BORDEAUX - 12/750 ml":
        $price = 110.00;
        break;
    case "Strawberry Peach BORDEAUX - 12/750 ml":
        $price = 110.00;
        break;
    case "Tangerine BORDEAUX - 12/750 ml":
        $price = 110.00;
        break;
    case "Black Truffle BORDEAUX - 12/750 ml":
        $price = 110.00;
        break;
    case "Vanilla BORDEAUX - 12/750 ml":
        $price = 110.00;
        break;
    case "Vanilla Fig BORDEAUX - 12/750 ml":
        $price = 110.00;
        break;
    //dorica bottles
    //oils 100ml
    case "Olive Oil Extra Virgin DORICA - 24/100 ML":
        $price = 32.00;
        break;
    case "Australia - Leccino/Frantoio DORICA - 24/100 ML":
        $price = 38.00;
        break;
    case "Australia - Frantoio DORICA - 24/100 ML":
        $price = 38.00;
        break;
    case "California Arbequina DORICA - 24/100 ML":
        $price = 51.00;
        break;
    case "California Arbosana DORICA - 24/100 ML":
        $price = 49.00;
        break;
    case "California Mission DORICA - 24/100 ML":
        $price = 49.00;
        break;
    case "California Arbequina/Arbosana/Koroneiki DORICA - 24/100 ML":
        $price = 38.00;
        break;
    case "Chile - Arbequina DORICA - 24/100 ML":
        $price = 33.00;
        break;
    case "Chile - Frantoio DORICA - 24/100 ML":
        $price = 33.00;
        break;
    case "Chile - Leccino DORICA - 24/100 ML":
        $price = 33.00;
        break;
    case "Chile - Picual DORICA - 24/100 ML":
        $price = 33.00;
        break;
    case "Greece - Kalamata DORICA - 24/100 ML":
        $price = 35.00;
        break;
    case "Greece - Koroneiki DORICA - 24/100 ML":
        $price = 35.00;
        break;
    case "Italy - Umbrian region DORICA - 24/100 ML":
        $price = 48.00;
        break;
    case "Italy - Calabria region - Carolea DORICA - 24/100 ML":
        $price = 42.00;
        break;
    case "Italy (Puglia) - Coratina DORICA - 24/100 ML":
        $price = 43.00;
        break;
    case "Italy (Puglia)- Ogliarola DORICA - 24/100 ML":
        $price = 43.00;
        break;
    case "Spain - Hojiblanca DORICA - 24/100 ML":
        $price = 36.00;
        break;
    case "Spain - Picual DORICA - 24/100 ML":
        $price = 36.00;
        break;
    case "Spain - Signature blend DORICA - 24/100 ML":
        $price = 36.00;
        break;
    case "Tunisia - Chemlali/Chetoui DORICA - 24/100 ML":
        $price = 32.00;
        break;
//infused
    case "Black Pepper DORICA - 24/100 ML":
        $price = 44.00;
        break;
    case "Garlic Oil DORICA - 24/100 ML":
        $price = 39.00;
        break;
    case "Roasted Chili DORICA - 24/100 ML":
        $price = 40.00;
        break;
    case "Oregano DORICA - 24/100 ML":
        $price = 41.00;
        break;
    case "Lemon Pepper DORICA - 24/100 ML":
        $price = 46.00;
        break;
    case "Jalapeno oil DORICA - 24/100 ML":
        $price = 39.00;
        break;
    case "Basil DORICA - 24/100 ML":
        $price = 39.00;
        break;
    case "Scallion DORICA - 24/100 ML":
        $price = 42.00;
        break;
    case "Habenero DORICA - 24/100 ML":
        $price = 48.00;
        break;
    case "Rosemary DORICA - 24/100 ML":
        $price = 42.00;
        break;
    case "Chipotle DORICA - 24/100 ML":
        $price = 40.00;
        break;
    //flavored oils
    case "Blood Orange DORICA - 24/100 ML":
        $price = 34.00;
        break;
    case "Lemon oil DORICA - 24/100 ML":
        $price = 37.00;
        break;
    case "Lime DORICA - 24/100 ML":
        $price = 38.00;
        break;
    case "Orange oil DORICA - 24/100 ML":
        $price = 34.00;
        break;
    case "Tangerine oil DORICA - 24/100 ML":
        $price = 35.00;
        break;
    case "Butter DORICA - 24/100 ML":
        $price = 40.00;
        break;
    case "Cranberry Walnut oil DORICA - 24/100 ML":
        $price = 34.00;
        break;
    case "Arbequina Rhubarb DORICA - 24/100 ML":
        $price = 34.00;
        break;
    case "Sage Onion DORICA - 24/100 ML":
        $price = 43.00;
        break;
    case "Sundried Tomato DORICA - 24/100 ML":
        $price = 38.00;
        break;
    case "White Truffle DORICA - 24/100 ML":
        $price = 72.00;
        break;
    case "Black Truffle oil DORICA - 24/100 ML":
        $price = 71.00;
        break;
    case "Hickory oil DORICA - 24/100 ML":
        $price = 38.00;
        break;
    case "Vanilla oil DORICA - 24/100 ML":
        $price = 38.00;
        break;
//fused
    case "Garlic Mushroom DORICA - 24/100 ML":
        $price = 43.00;
        break;
    case "Basil/Lemongrass DORICA - 24/100 ML":
        $price = 44.00;
        break;
    case "Rosemary with Lavender DORICA - 24/100 ML":
        $price = 47.00;
        break;
    case "Tuscan Herb DORICA - 24/100 ML":
        $price = 46.00;
        break;
    case "Herbs d Provence DORICA - 24/100 ML":
        $price = 45.00;
        break;
    case "Citrus Habanero DORICA - 24/100 ML":
        $price = 41.00;
        break;
    case "Garlic Roasted Chili DORICA - 24/100 ML":
        $price = 43.00;
        break;
    //specialty oils
    case "Almond Oil DORICA - 24/100 ML":
        $price = 37.00;
        break;
    case "Apricot Oil DORICA - 24/100 ML":
        $price = 38.00;
        break;
    case "Avocado Oil DORICA - 24/100 ML":
        $price = 35.00;
        break;
    case "Flaxseed Oil DORICA - 24/100 ML":
        $price = 30.00;
        break;
    case "Grapeseed Oil DORICA - 24/100 ML":
        $price = 27.00;
        break;
    case "Hempseed Oil DORICA - 24/100 ML":
        $price = 53.00;
        break;
    case "Macadamia Nut Oil DORICA - 24/100 ML":
        $price = 41.00;
        break;
    case "Toasted Sesame Oil DORICA - 24/100 ML":
        $price = 31.00;
        break;
    case "Walnut Oil DORICA - 24/100 ML":
        $price = 29.00;
        break;
    //organics
    case "Olive Oil, Extra Virgin, Organic , Italy DORICA - 24/100 ML":
        $price = 37.00;
        break;
    case "Olive Oil, Extra Virgin, Organic , Country Varies DORICA - 24/100 ML":
        $price = 34.00;
        break;
    case "Canola DORICA - 24/100 ML":
        $price = 26.00;
        break;
    case "Soybean Oil DORICA - 24/100 ML":
        $price = 23.00;
        break;
    case "Sunflower DORICA - 24/100 ML":
        $price = 31.00;
        break;
    case "Sesame Toasted DORICA - 24/100 ML":
        $price = 30.00;
        break;
    case "Sesame, Virgin DORICA - 24/100 ML":
        $price = 30.00;
        break;
    //vinegars
    case "4 Star Balsamic DORICA - 24/100 ML":
        $price = 28.00;
        break;
    case "8 Star Balsamic DORICA - 24/100 ML":
        $price = 34.00;
        break;
    case "12 Star Balsamic DORICA - 24/100 ML":
        $price = 36.00;
        break;
    case "25 Star Dark Balsamic DORICA - 24/100 ML":
        $price = 47.00;
        break;
    case "25 Star White Balsamic DORICA - 24/100 ML":
        $price = 47.00;
        break;
    case "6 Star White Balsamic DORICA - 24/100 ML":
        $price = 30.00;
        break;
    case "Red Wine Vinegar - Domestic - 50 Grain DORICA - 24/100 ML":
        $price = 24.00;
        break;
    case "Organic Premium Quality Balsamic - Ltd Edition DORICA - 24/100 ML":
        $price = 44.00;
        break;
    case "Serrano Chile Vinegar DORICA - 24/100 ML":
        $price = 48.00;
        break;
    case "Lambrusco Vinegar DORICA - 24/100 ML":
        $price = 47.00;
        break;
    //flavored balsamic
    case "Almond Creme DORICA - 24/100 ML":
        $price = 47.00;
        break;
    case "Apricot DORICA - 24/100 ML":
        $price = 47.00;
        break;
    case "Bittersweet Chocolate with Orange DORICA - 24/100 ML":
        $price = 47.00;
        break;
    case "Blackberry DORICA - 24/100 ML":
        $price = 47.00;
        break;
    case "Blackberry Ginger DORICA - 24/100 ML":
        $price = 47.00;
        break;
    case "Black Currant DORICA - 24/100 ML":
        $price = 47.00;
        break;
    case "Black Walnut DORICA - 24/100 ML":
        $price = 47.00;
        break;
    case "Blueberry DORICA - 24/100 ML":
        $price = 47.00;
        break;
    case "Bordeaux Cherry DORICA - 24/100 ML":
        $price = 47.00;
        break;
    case "Chili DORICA - 24/100 ML":
        $price = 47.00;
        break;
    case "Chocolate DORICA - 24/100 ML":
        $price = 47.00;
        break;
    case "Chocolate Covered Cherries DORICA - 24/100 ML":
        $price = 47.00;
        break;
    case "Chocolate Jalapeno DORICA - 24/100 ML":
        $price = 47.00;
        break;
    case "Chocolate Marshmallow DORICA - 24/100 ML":
        $price = 47.00;
        break;
    case "Coconut DORICA - 24/100 ML":
        $price = 47.00;
        break;
    case "Cranberry Orange DORICA - 24/100 ML":
        $price = 47.00;
        break;
    case "Cranberry Walnut DORICA - 24/100 ML":
        $price = 47.00;
        break;
    case "Cucumber Melon DORICA - 24/100 ML":
        $price = 47.00;
        break;
    case "Elderberry DORICA - 24/100 ML":
        $price = 47.00;
        break;
    case "Espresso DORICA - 24/100 ML":
        $price = 47.00;
        break;
    case "Fig DORICA - 24/100 ML":
        $price = 47.00;
        break;
    case "Garlic DORICA - 24/100 ML":
        $price = 47.00;
        break;
    case "Garlic Cilantro DORICA - 24/100 ML":
        $price = 47.00;
        break;
    case "Grapefruit DORICA - 24/100 ML":
        $price = 47.00;
        break;
    case "Green Apple DORICA - 24/100 ML":
        $price = 47.00;
        break;
    case "Hickory DORICA - 24/100 ML":
        $price = 47.00;
        break;
    case "Honey DORICA - 24/100 ML":
        $price = 47.00;
        break;
    case "Honey Ginger DORICA - 24/100 ML":
        $price = 47.00;
        break;
    case "Huckleberry DORICA - 24/100 ML":
        $price = 47.00;
        break;
    case "Jalapeno DORICA - 24/100 ML":
        $price = 47.00;
        break;
    case "Jalapeno Fig in 8 Star DORICA - 24/100 ML":
        $price = 41.00;
        break;
    case "Jalapeno & Lime DORICA - 24/100 ML":
        $price = 47.00;
        break;
    case "Lavender DORICA - 24/100 ML":
        $price = 47.00;
        break;
    case "Lemongrass Mint DORICA - 24/100 ML":
        $price = 47.00;
        break;
    case "Lemon DORICA - 24/100 ML":
        $price = 47.00;
        break;
    case "Lemon Vanilla DORICA - 24/100 ML":
        $price = 47.00;
        break;
    case "Mandarin Orange DORICA - 24/100 ML":
        $price = 47.00;
        break;
    case "Mango DORICA - 24/100 ML":
        $price = 47.00;
        break;
    case "Mocha Almond Fudge DORICA - 24/100 ML":
        $price = 47.00;
        break;
    case "Orange DORICA - 24/100 ML":
        $price = 47.00;
        break;
    case "Orange/Mango/Passionfruit DORICA - 24/100 ML":
        $price = 47.00;
        break;
    case "Orange Vanilla DORICA - 24/100 ML":
        $price = 47.00;
        break;
    case "Peach DORICA - 24/100 ML":
        $price = 47.00;
        break;
    case "Pear DORICA - 24/100 ML":
        $price = 47.00;
        break;
    case "Pecan Praline DORICA - 24/100 ML":
        $price = 47.00;
        break;
    case "Pineapple DORICA - 24/100 ML":
        $price = 47.00;
        break;
    case "Pomegranate DORICA - 24/100 ML":
        $price = 47.00;
        break;
    case "Pumpkin Spice DORICA - 24/100 ML":
        $price = 47.00;
        break;
    case "Raspberry DORICA - 24/100 ML":
        $price = 47.00;
        break;
    case "Raspberry Ginger DORICA - 24/100 ML":
        $price = 47.00;
        break;
    case "Strawberry DORICA - 24/100 ML":
        $price = 47.00;
        break;
    case "Strawberry Peach DORICA - 24/100 ML":
        $price = 47.00;
        break;
    case "Tangerine DORICA - 24/100 ML":
        $price = 47.00;
        break;
    case "Black Truffle DORICA - 24/100 ML":
        $price = 47.00;
        break;
    case "Vanilla DORICA - 24/100 ML":
        $price = 47.00;
        break;
    case "Vanilla Fig DORICA - 24/100 ML":
        $price = 47.00;
        break;
//dorica 500
//oils
    case "Olive Oil Extra Virgin DORICA - 12/500 ML":
        $price = 48.00;
        break;
    case "Australia - Leccino/Frantoio DORICA - 12/500 ML":
        $price = 62.00;
        break;
    case "Australia - Frantoio DORICA - 12/500 ML":
        $price = 62.00;
        break;
    case "California Arbequina DORICA - 12/500 ML":
        $price = 95.00;
        break;
    case "California Arbosana DORICA - 12/500 ML":
        $price = 90.00;
        break;
    case "California Mission DORICA - 12/500 ML":
        $price = 90.00;
        break;
    case "California Arbequina/Arbosana/Koroneiki DORICA - 12/500 ML":
        $price = 63.00;
        break;
    case "Chile - Arbequina DORICA - 12/500 ML":
        $price = 49.00;
        break;
    case "Chile - Frantoio DORICA - 12/500 ML":
        $price = 49.00;
        break;
    case "Chile - Leccino DORICA - 12/500 ML":
        $price = 49.00;
        break;
    case "Chile - Picual DORICA - 12/500 ML":
        $price = 49.00;
        break;
    case "Greece - Kalamata DORICA - 12/500 ML":
        $price = 56.00;
        break;
    case "Greece - Koroneiki DORICA - 12/500 ML":
        $price = 56.00;
        break;
    case "Italy - Umbrian region DORICA - 12/500 ML":
        $price = 87.00;
        break;
    case "Italy - Calabria region - Carolea DORICA - 12/500 ML":
        $price = 73.00;
        break;
    case "Italy (Puglia) - Coratina DORICA - 12/500 ML":
        $price = 73.00;
        break;
    case "Italy (Puglia)- Ogliarola DORICA - 12/500 ML":
        $price = 73.00;
        break;
    case "Spain - Hojiblanca DORICA - 12/500 ML":
        $price = 58.00;
        break;
    case "Spain - Picual DORICA - 12/500 ML":
        $price = 58.00;
        break;
    case "Spain - Signature blend DORICA - 12/500 ML":
        $price = 58.00;
        break;
    case "Tunisia - Chemlali/Chetoui DORICA - 12/500 ML":
        $price = 48.00;
        break;
    //infused
    case "Black Pepper DORICA - 12/500 ML":
        $price = 80.00;
        break;
    case "Garlic Oil DORICA - 12/500 ML":
        $price = 67.00;
        break;
    case "Roasted Chili DORICA - 12/500 ML":
        $price = 71.00;
        break;
    case "Oregano DORICA - 12/500 ML":
        $price = 73.00;
        break;
    case "Lemon Pepper DORICA - 12/500 ML":
        $price = 86.00;
        break;
    case "Jalapeno oil DORICA - 12/500 ML":
        $price = 67.00;
        break;
    case "Basil DORICA - 12/500 ML":
        $price = 68.00;
        break;
    case "Scallion DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Habenero DORICA - 12/500 ML":
        $price = 91.00;
        break;
    case "Rosemary DORICA - 12/500 ML":
        $price = 76.00;
        break;
    case "Chipotle DORICA - 12/500 ML":
        $price = 71.00;
        break;
    //flavored olive oils
    case "Blood Orange DORICA - 12/500 ML":
        $price = 52.00;
        break;
    case "Lemon oil DORICA - 12/500 ML":
        $price = 60.00;
        break;
    case "Lime DORICA - 12/500 ML":
        $price = 62.00;
        break;
    case "Orange oil DORICA - 12/500 ML":
        $price = 52.00;
        break;
    case "Tangerine oil DORICA - 12/500 ML":
        $price = 54.00;
        break;
    case "Butter DORICA - 12/500 ML":
        $price = 67.00;
        break;
    case "Cranberry Walnut oil DORICA - 12/500 ML":
        $price = 53.00;
        break;
    case "Arbequina Rhubarb DORICA - 12/500 ML":
        $price = 52.00;
        break;
    case "Sage Onion DORICA - 12/500 ML":
        $price = 76.00;
        break;
    case "Sundried Tomato DORICA - 12/500 ML":
        $price = 63.00;
        break;
    case "White Truffle DORICA - 12/500 ML":
        $price = 141.00;
        break;
    case "Black Truffle oil DORICA - 12/500 ML":
        $price = 138.00;
        break;
    case "Hickory oil DORICA - 12/500 ML":
        $price = 63.00;
        break;
    case "Vanilla oil DORICA - 12/500 ML":
        $price = 63.00;
        break;
    //fused
    case "Garlic Mushroom DORICA - 12/500 ML":
        $price = 76.00;
        break;
    case "Basil/Lemongrass DORICA - 12/500 ML":
        $price = 77.00;
        break;
    case "Rosemary with Lavender DORICA - 12/500 ML":
        $price = 86.00;
        break;
    case "Tuscan Herb DORICA - 12/500 ML":
        $price = 83.00;
        break;
    case "Herbs d Provence DORICA - 12/500 ML":
        $price = 80.00;
        break;
    case "Citrus Habanero DORICA - 12/500 ML":
        $price = 71.00;
        break;
    case "Garlic Roasted Chili DORICA - 12/500 ML":
        $price = 75.00;
        break;
    //specialty oils
    case "Almond Oil DORICA - 12/500 ML":
        $price = 62.00;
        break;
    case "Apricot Oil DORICA - 12/500 ML":
        $price = 65.00;
        break;
    case "Avocado Oil DORICA - 12/500 ML":
        $price = 56.00;
        break;
    case "Flaxseed Oil DORICA - 12/500 ML":
        $price = 45.00;
        break;
    case "Grapeseed Oil DORICA - 12/500 ML":
        $price = 38.00;
        break;
    case "Hempseed Oil DORICA - 12/500 ML":
        $price = 103.00;
        break;
    case "Macadamia Nut Oil DORICA - 12/500 ML":
        $price = 73.00;
        break;
    case "Toasted Sesame Oil DORICA - 12/500 ML":
        $price = 48.00;
        break;
    case "Walnut Oil DORICA - 12/500 ML":
        $price = 43.00;
        break;
    //organics
    case "Olive Oil, Extra Virgin, Organic , Italy DORICA - 12/500 ML":
        $price = 62.00;
        break;
    case "Olive Oil, Extra Virgin, Organic , Country Varies DORICA - 12/500 ML":
        $price = 53.00;
        break;
    case "Canola DORICA - 12/500 ML":
        $price = 35.00;
        break;
    case "Soybean Oil DORICA - 12/500 ML":
        $price = 27.00;
        break;
    case "Sunflower DORICA - 12/500 ML":
        $price = 48.00;
        break;
    case "Sesame Toasted DORICA - 12/500 ML":
        $price = 46.00;
        break;
    case "Sesame, Virgin DORICA - 12/500 ML":
        $price = 46.00;
        break;
    //vinegars
    case "4 Star Balsamic DORICA - 12/500 ML":
        $price = 30.00;
        break;
    case "8 Star Balsamic DORICA - 12/500 ML":
        $price = 41.00;
        break;
    case "12 Star Balsamic DORICA - 12/500 ML":
        $price = 47.00;
        break;
    case "25 Star Dark Balsamic DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "25 Star White Balsamic DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "6 Star White Balsamic DORICA - 12/500 ML":
        $price = 40.00;
        break;
    case "Red Wine Vinegar - Domestic - 50 Grain DORICA - 12/500 ML":
        $price = 23.00;
        break;
    case "Organic Premium Quality Balsamic - Ltd Edition DORICA - 12/500 ML":
        $price = 79.00;
        break;
    case "Serrano Chile Vinegar DORICA - 12/500 ML":
        $price = 82.00;
        break;
    case "Lambrusco Vinegar DORICA - 12/500 ML":
        $price = 74.00;
        break;
    //flavored balsamic
    case "Almond Creme DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Apricot DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Bittersweet Chocolate with Orange DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Blackberry DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Blackberry Ginger DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Black Currant DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Black Walnut DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Blueberry DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Bordeaux Cherry DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Chili DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Chocolate DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Chocolate Covered Cherries DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Chocolate Jalapeno DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Chocolate Marshmallow DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Coconut DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Cranberry Orange DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Cranberry Walnut DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Cucumber Melon DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Elderberry DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Espresso DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Fig DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Garlic DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Garlic Cilantro DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Grapefruit DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Green Apple DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Hickory DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Honey DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Honey Ginger DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Huckleberry DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Jalapeno DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Jalapeno Fig in 8 Star DORICA - 12/500 ML":
        $price = 59.00;
        break;
    case "Jalapeno & Lime DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Lavender DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Lemongrass Mint DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Lemon DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Lemon Vanilla DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Mandarin Orange DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Mango DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Mocha Almond Fudge DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Orange DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Orange/Mango/Passionfruit DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Orange Vanilla DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Peach DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Pear DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Pecan Praline DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Pineapple DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Pomegranate DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Pumpkin Spice DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Raspberry DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Raspberry Ginger DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Strawberry DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Strawberry Peach DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Tangerine DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Black Truffle DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Vanilla DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Vanilla Fig DORICA - 12/500 ML":
        $price = 74.00;
        break;

    //dorica 12/250 ml antique

    //clear dorica bottles
//oils
    case "Olive Oil Extra Virgin DORICA - 12/250 ML":
        $price = 31.00;
        break;
    case "Australia - Leccino/Frantoio DORICA - 12/250 ML":
        $price = 38.00;
        break;
    case "Australia - Frantoio DORICA - 12/250 ML":
        $price = 38.00;
        break;
    case "California Arbequina DORICA - 12/250 ML":
        $price = 55.00;
        break;
    case "California Arbosana DORICA - 12/250 ML":
        $price = 53.00;
        break;
    case "California Mission DORICA - 12/250 ML":
        $price = 53.00;
        break;
    case "California Arbequina/Arbosana/Koroneiki DORICA - 12/250 ML":
        $price = 38.00;
        break;
    case "Chile - Arbequina DORICA - 12/250 ML":
        $price = 31.00;
        break;
    case "Chile - Frantoio DORICA - 12/250 ML":
        $price = 31.00;
        break;
    case "Chile - Leccino DORICA - 12/250 ML":
        $price = 31.00;
        break;
    case "Chile - Picual DORICA - 12/250 ML":
        $price = 30.00;
        break;
    case "Greece - Kalamata DORICA - 12/250 ML":
        $price = 35.00;
        break;
    case "Greece - Koroneiki DORICA - 12/250 ML":
        $price = 35.00;
        break;
    case "Italy - Umbrian region DORICA - 12/250 ML":
        $price = 51.00;
        break;
    case "Italy - Calabria region - Carolea DORICA - 12/250 ML":
        $price = 44.00;
        break;
    case "Italy (Puglia) - Coratina DORICA - 12/250 ML":
        $price = 44.00;
        break;
    case "Italy (Puglia)- Ogliarola DORICA - 12/250 ML":
        $price = 44.00;
        break;
    case "Spain - Hojiblanca DORICA - 12/250 ML":
        $price = 36.00;
        break;
    case "Spain - Picual DORICA - 12/250 ML":
        $price = 36.00;
        break;
    case "Spain - Signature blend DORICA - 12/250 ML":
        $price = 36.00;
        break;
    case "Tunisia - Chemlali/Chetoui DORICA - 12/250 ML":
        $price = 31.00;
        break;
//infused
    case "Black Pepper DORICA - 12/250 ML":
        $price = 47.00;
        break;
    case "Garlic Oil DORICA - 12/250 ML":
        $price = 40.00;
        break;
    case "Roasted Chili DORICA - 12/250 ML":
        $price = 42.00;
        break;
    case "Oregano DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Lemon Pepper DORICA - 12/250 ML":
        $price = 50.00;
        break;
    case "Jalapeno oil DORICA - 12/250 ML":
        $price = 40.00;
        break;
    case "Basil DORICA - 12/250 ML":
        $price = 41.00;
        break;
    case "Scallion DORICA - 12/250 ML":
        $price = 44.00;
        break;
    case "Habenero DORICA - 12/250 ML":
        $price = 53.00;
        break;
    case "Rosemary DORICA - 12/250 ML":
        $price = 45.00;
        break;
    case "Chipotle DORICA - 12/250 ML":
        $price = 42.00;
        break;
//flavored olive oils
    case "Blood Orange DORICA - 12/250 ML":
        $price = 33.00;
        break;
    case "Lemon oil DORICA - 12/250 ML":
        $price = 37.00;
        break;
    case "Lime DORICA - 12/250 ML":
        $price = 38.00;
        break;
    case "Orange oil DORICA - 12/250 ML":
        $price = 33.00;
        break;
    case "Tangerine oil DORICA - 12/250 ML":
        $price = 34.00;
        break;
    case "Butter DORICA - 12/250 ML":
        $price = 40.00;
        break;
    case "Cranberry Walnut oil DORICA - 12/250 ML":
        $price =  33.00;
        break;
    case "Arbequina Rhubarb DORICA - 12/250 ML":
        $price = 33.00;
        break;
    case "Sage Onion DORICA - 12/250 ML":
        $price = 45.00;
        break;
    case "Sundried Tomato DORICA - 12/250 ML":
        $price = 39.00;
        break;
    case "White Truffle DORICA - 12/250 ML":
        $price = 80.00;
        break;
    case "Black Truffle oil DORICA - 12/250 ML":
        $price = 79.00;
        break;
    case "Hickory oil DORICA - 12/250 ML":
        $price = 38.00;
        break;
    case "Vanilla oil DORICA - 12/250 ML":
        $price = 39.00;
        break;
//fused
    case "Garlic Mushroom DORICA - 12/250 ML":
        $price = 45.00;
        break;
    case "Basil/Lemongrass DORICA - 12/250 ML":
        $price = 46.00;
        break;
    case "Rosemary with Lavender DORICA - 12/250 ML":
        $price = 50.00;
        break;
    case "Tuscan Herb DORICA - 12/250 ML":
        $price = 49.00;
        break;
    case "Herbs d Provence DORICA - 12/250 ML":
        $price = 48.00;
        break;
    case "Citrus Habanero DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Garlic Roasted Chili DORICA - 12/250 ML":
        $price = 45.00;
        break;
//specialty oils
    case "Almond Oil DORICA - 12/250 ML":
        $price = 38.00;
        break;
    case "Apricot Oil DORICA - 12/250 ML":
        $price = 39.00;
        break;
    case "Avocado Oil DORICA - 12/250 ML":
        $price = 35.00;
        break;
    case "Flaxseed Oil DORICA - 12/250 ML":
        $price = 29.00;
        break;
    case "Grapeseed Oil DORICA - 12/250 ML":
        $price = 25.00;
        break;
    case "Hempseed Oil DORICA - 12/250 ML":
        $price = 59.00;
        break;
    case "Macadamia Nut Oil DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Toasted Sesame Oil DORICA - 12/250 ML":
        $price = 30.00;
        break;
    case "Walnut Oil DORICA - 12/250 ML":
        $price = 28.00;
        break;
//organics
    case "Olive Oil, Extra Virgin, Organic , Italy DORICA - 12/250 ML":
        $price = 38.00;
        break;
    case "Olive Oil, Extra Virgin, Organic , Country Varies DORICA - 12/250 ML":
        $price = 33.00;
        break;
    case "Canola DORICA - 12/250 ML":
        $price = 23.00;
        break;
    case "Soybean Oil DORICA - 12/250 ML":
        $price = 19.00;
        break;
    case "Sunflower DORICA - 12/250 ML":
        $price = 30.00;
        break;
    case "Sesame Toasted DORICA - 12/250 ML":
        $price = 29.00;
        break;
    case "Sesame, Virgin DORICA - 12/250 ML":
        $price = 29.00;
        break;
//vinegars
    case "4 Star Balsamic DORICA - 12/250 ML":
        $price = 19.00;
        break;
    case "8 Star Balsamic DORICA - 12/250 ML":
        $price = 26.00;
        break;
    case "12 Star Balsamic DORICA - 12/250 ML":
        $price = 30.00;
        break;
    case "25 Star Dark Balsamic DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "25 Star White Balsamic DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "6 Star White Balsamic DORICA - 12/250 ML":
        $price = 25.00;
        break;
    case "Red Wine Vinegar - Domestic - 50 Grain DORICA - 12/250 ML":
        $price = 16.00;
        break;
    case "Organic Premium Quality Balsamic - Ltd Edition DORICA - 12/250 ML":
        $price = 45.00;
        break;
    case "Serrano Chile Vinegar DORICA - 12/250 ML":
        $price = 47.00;
        break;
    case "Lambrusco Vinegar DORICA - 12/250 ML":
        $price = 43.00;
        break;
//flavored balsamic
    case "Almond Creme DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Apricot DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Bittersweet Chocolate with Orange DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Blackberry DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Blackberry Ginger DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Black Currant DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Black Walnut DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Blueberry DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Bordeaux Cherry DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Chili DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Chocolate DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Chocolate Covered Cherries DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Chocolate Jalapeno DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Chocolate Marshmallow DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Coconut DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Cranberry Orange DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Cranberry Walnut DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Cucumber Melon DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Elderberry DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Espresso DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Fig DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Garlic DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Garlic Cilantro DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Grapefruit DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Green Apple DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Hickory DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Honey DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Honey Ginger DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Huckleberry DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Jalapeno DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Jalapeno Fig in 8 Star DORICA - 12/250 ML":
        $price = 38.00;
        break;
    case "Jalapeno & Lime DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Lavender DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Lemongrass Mint DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Lemon DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Lemon Vanilla DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Mandarin Orange DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Mango DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Mocha Almond Fudge DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Orange DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Orange/Mango/Passionfruit DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Orange Vanilla DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Peach DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Pear DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Pecan Praline DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Pineapple DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Pomegranate DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Pumpkin Spice DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Raspberry DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Raspberry Ginger DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Strawberry DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Strawberry Peach DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Tangerine DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Black Truffle DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Vanilla DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Vanilla Fig DORICA - 12/250 ML":
        $price = 43.00;
        break;

    //clear dorica bottles
//oils
    case "Olive Oil Extra Virgin CLEAR DORICA - 12/250 ML":
        $price = 31.00;
        break;
    case "Australia - Leccino/Frantoio CLEAR DORICA - 12/250 ML":
        $price = 38.00;
        break;
    case "Australia - Frantoio CLEAR DORICA - 12/250 ML":
        $price = 38.00;
        break;
    case "California Arbequina CLEAR DORICA - 12/250 ML":
        $price = 55.00;
        break;
    case "California Arbosana CLEAR DORICA - 12/250 ML":
        $price = 53.00;
        break;
    case "California Mission CLEAR DORICA - 12/250 ML":
        $price = 53.00;
        break;
    case "California Arbequina/Arbosana/Koroneiki CLEAR DORICA - 12/250 ML":
        $price = 38.00;
        break;
    case "Chile - Arbequina CLEAR DORICA - 12/250 ML":
        $price = 31.00;
        break;
    case "Chile - Frantoio CLEAR DORICA - 12/250 ML":
        $price = 31.00;
        break;
    case "Chile - Leccino CLEAR DORICA - 12/250 ML":
        $price = 31.00;
        break;
    case "Chile - Picual CLEAR DORICA - 12/250 ML":
        $price = 30.00;
        break;
    case "Greece - Kalamata CLEAR DORICA - 12/250 ML":
        $price = 35.00;
        break;
    case "Greece - Koroneiki CLEAR DORICA - 12/250 ML":
        $price = 35.00;
        break;
    case "Italy - Umbrian region CLEAR DORICA - 12/250 ML":
        $price = 51.00;
        break;
    case "Italy - Calabria region - Carolea CLEAR DORICA - 12/250 ML":
        $price = 44.00;
        break;
    case "Italy (Puglia) - Coratina CLEAR DORICA - 12/250 ML":
        $price = 44.00;
        break;
    case "Italy (Puglia)- Ogliarola CLEAR DORICA - 12/250 ML":
        $price = 44.00;
        break;
    case "Spain - Hojiblanca CLEAR DORICA - 12/250 ML":
        $price = 36.00;
        break;
    case "Spain - Picual CLEAR DORICA - 12/250 ML":
        $price = 36.00;
        break;
    case "Spain - Signature blend CLEAR DORICA - 12/250 ML":
        $price = 36.00;
        break;
    case "Tunisia - Chemlali/Chetoui CLEAR DORICA - 12/250 ML":
        $price = 31.00;
        break;
    //infused
    case "Black Pepper CLEAR DORICA - 12/250 ML":
        $price = 47.00;
        break;
    case "Garlic Oil CLEAR DORICA - 12/250 ML":
        $price = 40.00;
        break;
    case "Roasted Chili CLEAR DORICA - 12/250 ML":
        $price = 42.00;
        break;
    case "Oregano CLEAR DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Lemon Pepper CLEAR DORICA - 12/250 ML":
        $price = 50.00;
        break;
    case "Jalapeno oil CLEAR DORICA - 12/250 ML":
        $price = 40.00;
        break;
    case "Basil CLEAR DORICA - 12/250 ML":
        $price = 41.00;
        break;
    case "Scallion CLEAR DORICA - 12/250 ML":
        $price = 44.00;
        break;
    case "Habenero CLEAR DORICA - 12/250 ML":
        $price = 53.00;
        break;
    case "Rosemary CLEAR DORICA - 12/250 ML":
        $price = 45.00;
        break;
    case "Chipotle CLEAR DORICA - 12/250 ML":
        $price = 42.00;
        break;
//flavored olive oils
    case "Blood Orange CLEAR DORICA - 12/250 ML":
        $price = 33.00;
        break;
    case "Lemon oil CLEAR DORICA - 12/250 ML":
        $price = 37.00;
        break;
    case "Lime CLEAR DORICA - 12/250 ML":
        $price = 38.00;
        break;
    case "Orange oil CLEAR DORICA - 12/250 ML":
        $price = 33.00;
        break;
    case "Tangerine oil CLEAR DORICA - 12/250 ML":
        $price = 34.00;
        break;
    case "Butter CLEAR DORICA - 12/250 ML":
        $price = 40.00;
        break;
    case "Cranberry Walnut oil CLEAR DORICA - 12/250 ML":
        $price =  33.00;
        break;
    case "Arbequina Rhubarb CLEAR DORICA - 12/250 ML":
        $price = 33.00;
        break;
    case "Sage Onion CLEAR DORICA - 12/250 ML":
        $price = 45.00;
        break;
    case "Sundried Tomato CLEAR DORICA - 12/250 ML":
        $price = 39.00;
        break;
    case "White Truffle CLEAR DORICA - 12/250 ML":
        $price = 80.00;
        break;
    case "Black Truffle oil CLEAR DORICA - 12/250 ML":
        $price = 79.00;
        break;
    case "Hickory oil CLEAR DORICA - 12/250 ML":
        $price = 38.00;
        break;
    case "Vanilla oil CLEAR DORICA - 12/250 ML":
        $price = 39.00;
        break;
    //fused
    case "Garlic Mushroom CLEAR DORICA - 12/250 ML":
        $price = 45.00;
        break;
    case "Basil/Lemongrass CLEAR DORICA - 12/250 ML":
        $price = 46.00;
        break;
    case "Rosemary with Lavender CLEAR DORICA - 12/250 ML":
        $price = 50.00;
        break;
    case "Tuscan Herb CLEAR DORICA - 12/250 ML":
        $price = 49.00;
        break;
    case "Herbs d Provence CLEAR DORICA - 12/250 ML":
        $price = 48.00;
        break;
    case "Citrus Habanero CLEAR DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Garlic Roasted Chili CLEAR DORICA - 12/250 ML":
        $price = 45.00;
        break;
    //specialty oils
    case "Almond Oil CLEAR DORICA - 12/250 ML":
        $price = 38.00;
        break;
    case "Apricot Oil CLEAR DORICA - 12/250 ML":
        $price = 39.00;
        break;
    case "Avocado Oil CLEAR DORICA - 12/250 ML":
        $price = 35.00;
        break;
    case "Flaxseed Oil CLEAR DORICA - 12/250 ML":
        $price = 29.00;
        break;
    case "Grapeseed Oil CLEAR DORICA - 12/250 ML":
        $price = 25.00;
        break;
    case "Hempseed Oil CLEAR DORICA - 12/250 ML":
        $price = 59.00;
        break;
    case "Macadamia Nut Oil CLEAR DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Toasted Sesame Oil CLEAR DORICA - 12/250 ML":
        $price = 30.00;
    case "Walnut Oil CLEAR DORICA - 12/250 ML":
        $price = 28.00;
        break;
    //organics
    case "Olive Oil, Extra Virgin, Organic , Italy CLEAR DORICA - 12/250 ML":
        $price = 38.00;
        break;
    case "Olive Oil, Extra Virgin, Organic , Country Varies CLEAR DORICA - 12/250 ML":
        $price = 33.00;
        break;
    case "Canola CLEAR DORICA - 12/250 ML":
        $price = 23.00;
        break;
    case "Soybean Oil CLEAR DORICA - 12/250 ML":
        $price = 19.00;
        break;
    case "Sunflower CLEAR DORICA - 12/250 ML":
        $price = 30.00;
        break;
    case "Sesame Toasted CLEAR DORICA - 12/250 ML":
        $price = 29.00;
        break;
    case "Sesame, Virgin CLEAR DORICA - 12/250 ML":
        $price = 29.00;
        break;
//vinegars
    case "4 Star Balsamic CLEAR DORICA - 12/250 ML":
        $price = 19.00;
        break;
    case "8 Star Balsamic CLEAR DORICA - 12/250 ML":
        $price = 26.00;
        break;
    case "12 Star Balsamic CLEAR DORICA - 12/250 ML":
        $price = 30.00;
        break;
    case "25 Star Dark Balsamic CLEAR DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "25 Star White Balsamic CLEAR DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "6 Star White Balsamic CLEAR DORICA - 12/250 ML":
        $price = 25.00;
        break;
    case "Red Wine Vinegar - Domestic - 50 Grain CLEAR DORICA - 12/250 ML":
        $price = 16.00;
        break;
    case "Organic Premium Quality Balsamic - Ltd Edition CLEAR DORICA - 12/250 ML":
        $price = 45.00;
        break;
    case "Serrano Chile Vinegar CLEAR DORICA - 12/250 ML":
        $price = 47.00;
        break;
    case "Lambrusco Vinegar CLEAR DORICA - 12/250 ML":
        $price = 43.00;
        break;
    //flavored balsamic
    case "Almond Creme CLEAR DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Apricot CLEAR DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Bittersweet Chocolate with Orange CLEAR DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Blackberry CLEAR DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Blackberry Ginger CLEAR DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Black Currant CLEAR DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Black Walnut CLEAR DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Blueberry CLEAR DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Bordeaux Cherry CLEAR DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Chili CLEAR DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Chocolate CLEAR DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Chocolate Covered Cherries CLEAR DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Chocolate Jalapeno CLEAR DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Chocolate Marshmallow CLEAR DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Coconut CLEAR DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Cranberry Orange CLEAR DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Cranberry Walnut CLEAR DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Cucumber Melon CLEAR DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Elderberry CLEAR DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Espresso CLEAR DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Fig CLEAR DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Garlic CLEAR DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Garlic Cilantro CLEAR DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Grapefruit CLEAR DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Green Apple CLEAR DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Hickory CLEAR DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Honey CLEAR DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Honey Ginger CLEAR DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Huckleberry CLEAR DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Jalapeno CLEAR DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Jalapeno Fig in 8 Star CLEAR DORICA - 12/250 ML":
        $price = 38.00;
        break;
    case "Jalapeno & Lime CLEAR DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Lavender CLEAR DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Lemongrass Mint CLEAR DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Lemon CLEAR DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Lemon Vanilla CLEAR DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Mandarin Orange CLEAR DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Mango CLEAR DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Mocha Almond Fudge CLEAR DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Orange CLEAR DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Orange/Mango/Passionfruit CLEAR DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Orange Vanilla CLEAR DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Peach CLEAR DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Pear CLEAR DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Pecan Praline CLEAR DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Pineapple CLEAR DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Pomegranate CLEAR DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Pumpkin Spice CLEAR DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Raspberry CLEAR DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Raspberry Ginger CLEAR DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Strawberry CLEAR DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Strawberry Peach CLEAR DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Tangerine CLEAR DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Black Truffle CLEAR DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Vanilla CLEAR DORICA - 12/250 ML":
        $price = 43.00;
        break;
    case "Vanilla Fig CLEAR DORICA - 12/250 ML":
        $price = 43.00;
        break;
//clear dorica 500ml
//oils
    case "Olive Oil Extra Virgin CLEAR DORICA - 12/500 ML":
        $price = 48.00;
        break;
    case "Australia - Leccino/Frantoio CLEAR DORICA - 12/500 ML":
        $price = 62.00;
        break;
    case "Australia - Frantoio CLEAR DORICA - 12/500 ML":
        $price = 62.00;
        break;
    case "California Arbequina CLEAR DORICA - 12/500 ML":
        $price = 95.00;
        break;
    case "California Arbosana CLEAR DORICA - 12/500 ML":
        $price = 90.00;
        break;
    case "California Mission CLEAR DORICA - 12/500 ML":
        $price = 90.00;
        break;
    case "California Arbequina/Arbosana/Koroneiki CLEAR DORICA - 12/500 ML":
        $price = 63.00;
        break;
    case "Chile - Arbequina CLEAR DORICA - 12/500 ML":
        $price = 49.00;
        break;
    case "Chile - Frantoio CLEAR DORICA - 12/500 ML":
        $price = 49.00;
        break;
    case "Chile - Leccino CLEAR DORICA - 12/500 ML":
        $price = 49.00;
        break;
    case "Chile - Picual CLEAR DORICA - 12/500 ML":
        $price = 49.00;
        break;
    case "Greece - Kalamata CLEAR DORICA - 12/500 ML":
        $price = 56.00;
        break;
    case "Greece - Koroneiki CLEAR DORICA - 12/500 ML":
        $price = 56.00;
        break;
    case "Italy - Umbrian region CLEAR DORICA - 12/500 ML":
        $price = 87.00;
        break;
    case "Italy - Calabria region - Carolea CLEAR DORICA - 12/500 ML":
        $price = 73.00;
        break;
    case "Italy (Puglia) - Coratina CLEAR DORICA - 12/500 ML":
        $price = 73.00;
        break;
    case "Italy (Puglia)- Ogliarola CLEAR DORICA - 12/500 ML":
        $price = 73.00;
        break;
    case "Spain - Hojiblanca CLEAR DORICA - 12/500 ML":
        $price = 58.00;
        break;
    case "Spain - Picual CLEAR DORICA - 12/500 ML":
        $price = 58.00;
        break;
    case "Spain - Signature blend CLEAR DORICA - 12/500 ML":
        $price = 58.00;
        break;
    case "Tunisia - Chemlali/Chetoui CLEAR DORICA - 12/500 ML":
        $price = 48.00;
        break;
    //infused
    case "Black Pepper CLEAR DORICA - 12/500 ML":
        $price = 80.00;
        break;
    case "Garlic Oil CLEAR DORICA - 12/500 ML":
        $price = 67.00;
        break;
    case "Roasted Chili CLEAR DORICA - 12/500 ML":
        $price = 71.00;
        break;
    case "Oregano CLEAR DORICA - 12/500 ML":
        $price = 73.00;
        break;
    case "Lemon Pepper CLEAR DORICA - 12/500 ML":
        $price = 86.00;
        break;
    case "Jalapeno oil CLEAR DORICA - 12/500 ML":
        $price = 67.00;
        break;
    case "Basil CLEAR DORICA - 12/500 ML":
        $price = 68.00;
        break;
    case "Scallion CLEAR DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Habenero CLEAR DORICA - 12/500 ML":
        $price = 91.00;
        break;
    case "Rosemary CLEAR DORICA - 12/500 ML":
        $price = 76.00;
        break;
    case "Chipotle CLEAR DORICA - 12/500 ML":
        $price = 71.00;
        break;
//flavored olive oils
    case "Blood Orange CLEAR DORICA - 12/500 ML":
        $price = 52.00;
        break;
    case "Lemon oil CLEAR DORICA - 12/500 ML":
        $price = 60.00;
        break;
    case "Lime CLEAR DORICA - 12/500 ML":
        $price = 62.00;
        break;
    case "Orange oil CLEAR DORICA - 12/500 ML":
        $price = 52.00;
        break;
    case "Tangerine oil CLEAR DORICA - 12/500 ML":
        $price = 54.00;
        break;
    case "Butter CLEAR DORICA - 12/500 ML":
        $price = 67.00;
        break;
    case "Cranberry Walnut oil CLEAR DORICA - 12/500 ML":
        $price = 53.00;
        break;
    case "Arbequina Rhubarb CLEAR DORICA - 12/500 ML":
        $price = 52.00;
        break;
    case "Sage Onion CLEAR DORICA - 12/500 ML":
        $price = 76.00;
        break;
    case "Sundried Tomato CLEAR DORICA - 12/500 ML":
        $price = 63.00;
        break;
    case "White Truffle CLEAR DORICA - 12/500 ML":
        $price = 141.00;
        break;
    case "Black Truffle oil CLEAR DORICA - 12/500 ML":
        $price = 138.00;
        break;
    case "Hickory oil CLEAR DORICA - 12/500 ML":
        $price = 63.00;
        break;
    case "Vanilla oil CLEAR DORICA - 12/500 ML":
        $price = 63.00;
        break;
    //fused
    case "Garlic Mushroom CLEAR DORICA - 12/500 ML":
        $price = 76.00;
        break;
    case "Basil/Lemongrass CLEAR DORICA - 12/500 ML":
        $price = 77.00;
        break;
    case "Rosemary with Lavender CLEAR DORICA - 12/500 ML":
        $price = 86.00;
        break;
    case "Tuscan Herb CLEAR DORICA - 12/500 ML":
        $price = 83.00;
        break;
    case "Herbs d Provence CLEAR DORICA - 12/500 ML":
        $price = 80.00;
        break;
    case "Citrus Habanero CLEAR DORICA - 12/500 ML":
        $price = 71.00;
        break;
    case "Garlic Roasted Chili CLEAR DORICA - 12/500 ML":
        $price = 75.00;
        break;
    //specialty oils
    case "Almond Oil CLEAR DORICA - 12/500 ML":
        $price = 62.00;
        break;
    case "Apricot Oil CLEAR DORICA - 12/500 ML":
        $price = 65.00;
        break;
    case "Avocado Oil CLEAR DORICA - 12/500 ML":
        $price = 56.00;
        break;
    case "Flaxseed Oil CLEAR DORICA - 12/500 ML":
        $price = 45.00;
        break;
    case "Grapeseed Oil CLEAR DORICA - 12/500 ML":
        $price = 38.00;
        break;
    case "Hempseed Oil CLEAR DORICA - 12/500 ML":
        $price = 103.00;
        break;
    case "Macadamia Nut Oil CLEAR DORICA - 12/500 ML":
        $price = 73.00;
        break;
    case "Toasted Sesame Oil CLEAR DORICA - 12/500 ML":
        $price = 48.00;
        break;
    case "Walnut Oil CLEAR DORICA - 12/500 ML":
        $price = 43.00;
        break;
//organics
    case "Olive Oil, Extra Virgin, Organic , Italy CLEAR DORICA - 12/500 ML":
        $price = 62.00;
        break;
    case "Olive Oil, Extra Virgin, Organic , Country Varies CLEAR DORICA - 12/500 ML":
        $price = 53.00;
        break;
    case "Canola CLEAR DORICA - 12/500 ML":
        $price = 35.00;
        break;
    case "Soybean Oil CLEAR DORICA - 12/500 ML":
        $price = 27.00;
        break;
    case "Sunflower CLEAR DORICA - 12/500 ML":
        $price = 48.00;
        break;
    case "Sesame Toasted CLEAR DORICA - 12/500 ML":
        $price = 46.00;
        break;
    case "Sesame, Virgin CLEAR DORICA - 12/500 ML":
        $price = 46.00;
        break;
    //vinegars
    case "4 Star Balsamic CLEAR DORICA - 12/500 ML":
        $price = 30.00;
        break;
    case "8 Star Balsamic CLEAR DORICA - 12/500 ML":
        $price = 41.00;
        break;
    case "12 Star Balsamic CLEAR DORICA - 12/500 ML":
        $price = 47.00;
        break;
    case "25 Star Dark Balsamic CLEAR DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "25 Star White Balsamic CLEAR DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "6 Star White Balsamic CLEAR DORICA - 12/500 ML":
        $price = 40.00;
        break;
    case "Red Wine Vinegar - Domestic - 50 Grain CLEAR DORICA - 12/500 ML":
        $price = 23.00;
        break;
    case "Organic Premium Quality Balsamic - Ltd Edition CLEAR DORICA - 12/500 ML":
        $price = 79.00;
        break;
    case "Serrano Chile Vinegar CLEAR DORICA - 12/500 ML":
        $price = 82.00;
        break;
    case "Lambrusco Vinegar CLEAR DORICA - 12/500 ML":
        $price = 74.00;
        break;
//flavored balsamic
    case "Almond Creme CLEAR DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Apricot CLEAR DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Bittersweet Chocolate with Orange CLEAR DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Blackberry CLEAR DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Blackberry Ginger CLEAR DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Black Currant CLEAR DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Black Walnut CLEAR DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Blueberry CLEAR DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Bordeaux Cherry CLEAR DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Chili CLEAR DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Chocolate CLEAR DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Chocolate Covered Cherries":
        $price = 74.00;
        break;
    case "Chocolate Jalapeno CLEAR DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Chocolate Marshmallow CLEAR DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Coconut CLEAR DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Cranberry Orange CLEAR DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Cranberry Walnut CLEAR DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Cucumber Melon CLEAR DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Elderberry CLEAR DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Espresso CLEAR DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Fig CLEAR DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Garlic CLEAR DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Garlic Cilantro CLEAR DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Grapefruit CLEAR DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Green Apple CLEAR DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Hickory CLEAR DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Honey CLEAR DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Honey Ginger CLEAR DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Huckleberry CLEAR DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Jalapeno CLEAR DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Jalapeno Fig in 8 Star CLEAR DORICA - 12/500 ML":
        $price = 59.00;
        break;
    case "Jalapeno & Lime CLEAR DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Lavender CLEAR DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Lemongrass Mint CLEAR DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Lemon CLEAR DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Lemon Vanilla CLEAR DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Mandarin Orange CLEAR DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Mango CLEAR DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Mocha Almond Fudge CLEAR DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Orange CLEAR DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Orange/Mango/Passionfruit CLEAR DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Orange Vanilla CLEAR DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Peach CLEAR DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Pear CLEAR DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Pecan Praline CLEAR DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Pineapple CLEAR DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Pomegranate CLEAR DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Pumpkin Spice CLEAR DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Raspberry CLEAR DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Raspberry Ginger CLEAR DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Strawberry CLEAR DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Strawberry Peach CLEAR DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Tangerine CLEAR DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Black Truffle CLEAR DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Vanilla CLEAR DORICA - 12/500 ML":
        $price = 74.00;
        break;
    case "Vanilla Fig CLEAR DORICA - 12/500 ML":
        $price = 74.00;
        break;
//marasca 100ml bottomes
//oils
    case "Olive Oil Extra Virgin MARASCA - 24/100 ml":
        $price = 35.00;
        break;
    case "Australia - Leccino/Frantoio MARASCA - 24/100 ml":
        $price = 41.00;
        break;
    case "Australia - Frantoio MARASCA - 24/100 ml":
        $price = 41.00;
        break;
    case "California Arbequina MARASCA - 24/100 ml":
        $price = 54.00;
        break;
    case "California Arbosana MARASCA - 24/100 ml":
        $price = 52.00;
        break;
    case "California Mission MARASCA - 24/100 ml":
        $price = 52.00;
        break;
    case "California Arbequina/Arbosana/Koroneiki MARASCA - 24/100 ml":
        $price = 41.00;
        break;
    case "Chile - Arbequina MARASCA - 24/100 ml":
        $price = 35.00;
        break;
    case "Chile - Frantoio MARASCA - 24/100 ml":
        $price = 35.00;
        break;
    case "Chile - Leccino MARASCA - 24/100 ml":
        $price = 35.00;
        break;
    case "Chile - Picual MARASCA - 24/100 ml":
        $price = 35.00;
        break;
    case "Greece - Kalamata MARASCA - 24/100 ml":
        $price = 38.00;
        break;
    case "Greece - Koroneiki MARASCA - 24/100 ml":
        $price = 38.00;
        break;
    case "Italy - Umbrian region MARASCA - 24/100 ml":
        $price = 51.00;
        break;
    case "Italy - Calabria region - Carolea MARASCA - 24/100 ml":
        $price = 45.00;
        break;
    case "Italy (Puglia) - Coratina MARASCA - 24/100 ml":
        $price = 45.00;
        break;
    case "Italy (Puglia)- Ogliarola MARASCA - 24/100 ml":
        $price = 45.00;
        break;
    case "Spain - Hojiblanca MARASCA - 24/100 ml":
        $price = 39.00;
        break;
    case "Spain - Picual MARASCA - 24/100 ml":
        $price = 39.00;
        break;
    case "Spain - Signature blend MARASCA - 24/100 ml":
        $price = 39.00;
        break;
    case "Tunisia - Chemlali/Chetoui MARASCA - 24/100 ml":
        $price = 35.00;
        break;
    //infused
    case "Black Pepper MARASCA - 24/100 ml":
        $price = 46.00;
        break;
    case "Garlic Oil MARASCA - 24/100 ml":
        $price = 41.00;
        break;
    case "Roasted Chili MARASCA - 24/100 ml":
        $price = 43.00;
        break;
    case "Oregano MARASCA - 24/100 ml":
        $price = 44.00;
        break;
    case "Lemon Pepper MARASCA - 24/100 ml":
        $price = 49.00;
        break;
    case "Jalapeno oil MARASCA - 24/100 ml":
        $price = 41.00;
        break;
    case "Basil MARASCA - 24/100 ml":
        $price = 42.00;
        break;
    case "Scallion MARASCA - 24/100 ml":
        $price = 44.00;
        break;
    case "Habenero MARASCA - 24/100 ml":
        $price = 51.00;
        break;
    case "Rosemary MARASCA - 24/100 ml":
        $price = 45.00;
        break;
    case "Chipotle MARASCA - 24/100 ml":
        $price = 43.00;
        break;
    //flavored olive oils
    case "Blood Orange MARASCA - 24/100 ml":
        $price = 36.00;
        break;
    case "Lemon oil MARASCA - 24/100 ml":
        $price = 40.00;
        break;
    case "Lime MARASCA - 24/100 ml":
        $price = 40.00;
        break;
    case "Orange oil MARASCA - 24/100 ml":
        $price = 36.00;
        break;
    case "Tangerine oil MARASCA - 24/100 ml":
        $price = 37.00;
        break;
    case "Butter MARASCA - 24/100 ml":
        $price = 42.00;
        break;
    case "Cranberry Walnut oil MARASCA - 24/100 ml":
        $price = 37.00;
        break;
    case "Arbequina Rhubarb MARASCA - 24/100 ml":
        $price = 36.00;
        break;
    case "Sage Onion MARASCA - 24/100 ml":
        $price = 46.00;
        break;
    case "Sundried Tomato MARASCA - 24/100 ml":
        $price = 41.00;
        break;
    case "White Truffle MARASCA - 24/100 ml":
        $price = 75.00;
        break;
    case "Black Truffle oil MARASCA - 24/100 ml":
        $price = 74.00;
        break;
    case "Hickory oil MARASCA - 24/100 ml":
        $price = 41.00;
        break;
    case "Vanilla oil MARASCA - 24/100 ml":
        $price = 41.00;
        break;
    //fused
    case "Garlic Mushroom MARASCA - 24/100 ml":
        $price = 46.00;
        break;
    case "Basil/Lemongrass MARASCA - 24/100 ml":
        $price = 46.00;
        break;
    case "Rosemary with Lavender MARASCA - 24/100 ml":
        $price = 50.00;
        break;
    case "Tuscan Herb MARASCA - 24/100 ml":
        $price = 48.00;
        break;
    case "Herbs d Provence MARASCA - 24/100 ml":
        $price = 48.00;
        break;
    case "Citrus Habanero MARASCA - 24/100 ml":
        $price = 44.00;
        break;
    case "Garlic Roasted Chili MARASCA - 24/100 ml":
        $price = 46.00;
        break;
    //specialty oils
    case "Almond Oil MARASCA - 24/100 ml":
        $price = 39.00;
        break;
    case "Apricot Oil MARASCA - 24/100 ml":
        $price = 40.00;
        break;
    case "Avocado Oil MARASCA - 24/100 ml":
        $price = 37.00;
        break;
    case "Flaxseed Oil MARASCA - 24/100 ml":
        $price = 32.00;
        break;
    case "Grapeseed Oil MARASCA - 24/100 ml":
        $price = 30.00;
        break;
    case "Hempseed Oil MARASCA - 24/100 ml":
        $price = 56.00;
        break;
    case "Macadamia Nut Oil MARASCA - 24/100 ml":
        $price = 44.00;
        break;
    case "Toasted Sesame Oil MARASCA - 24/100 ml":
        $price = 34.00;
        break;
    case "Walnut Oil MARASCA - 24/100 ml":
        $price = 32.00;
        break;
    //organics
    case "Olive Oil, Extra Virgin, Organic , Italy MARASCA - 24/100 ml":
        $price = 39.00;
        break;
    case "Olive Oil, Extra Virgin, Organic , Country Varies MARASCA - 24/100 ml":
        $price = 36.00;
        break;
    case "Canola MARASCA - 24/100 ml":
        $price = 28.00;
        break;
    case "Soybean Oil MARASCA - 24/100 ml":
        $price = 25.00;
        break;
    case "Sunflower MARASCA - 24/100 ml":
        $price = 34.00;
        break;
    case "Sesame Toasted MARASCA - 24/100 ml":
        $price = 33.00;
        break;
    case "Sesame, Virgin MARASCA - 24/100 ml":
        $price = 33.00;
        break;
    //vinegars
    case "4 Star Balsamic MARASCA - 24/100 ml":
        $price = 28.00;
        break;
    case "8 Star Balsamic MARASCA - 24/100 ml":
        $price = 34.00;
        break;
    case "12 Star Balsamic MARASCA - 24/100 ml":
        $price = 36.00;
        break;
    case "25 Star Dark Balsamic MARASCA - 24/100 ml":
        $price = 50.00;
        break;
    case "25 Star White Balsamic MARASCA - 24/100 ml":
        $price = 50.00;
        break;
    case "6 Star White Balsamic MARASCA - 24/100 ml":
        $price = 33.00;
        break;
    case "Red Wine Vinegar - Domestic - 50 Grain MARASCA - 24/100 ml":
        $price = 26.00;
        break;
    case "Organic Premium Quality Balsamic - Ltd Edition MARASCA - 24/100 ml":
        $price = 47.00;
        break;
    case "Serrano Chile Vinegar MARASCA - 24/100 ml":
        $price = 51.00;
        break;
    case "Lambrusco Vinegar MARASCA - 24/100 ml":
        $price = 50.00;
        break;
    //flavored balsamic
    case "Almond Creme MARASCA - 24/100 ml":
        $price = 50.00;
        break;
    case "Apricot MARASCA - 24/100 ml":
        $price = 50.00;
        break;
    case "Bittersweet Chocolate with Orange MARASCA - 24/100 ml":
        $price = 50.00;
        break;
    case "Blackberry MARASCA - 24/100 ml":
        $price = 50.00;
        break;
    case "Blackberry Ginger MARASCA - 24/100 ml":
        $price = 50.00;
        break;
    case "Black Currant MARASCA - 24/100 ml":
        $price = 50.00;
        break;
    case "Black Walnut MARASCA - 24/100 ml":
        $price = 50.00;
        break;
    case "Blueberry MARASCA - 24/100 ml":
        $price = 50.00;
        break;
    case "Bordeaux Cherry MARASCA - 24/100 ml":
        $price = 50.00;
        break;
    case "Chili MARASCA - 24/100 ml":
        $price = 50.00;
        break;
    case "Chocolate MARASCA - 24/100 ml":
        $price = 50.00;
        break;
    case "Chocolate Covered Cherries MARASCA - 24/100 ml":
        $price = 50.00;
        break;
    case "Chocolate Jalapeno MARASCA - 24/100 ml":
        $price = 50.00;
        break;
    case "Chocolate Marshmallow MARASCA - 24/100 ml":
        $price = 50.00;
        break;
    case "Coconut MARASCA - 24/100 ml":
        $price = 50.00;
        break;
    case "Cranberry Orange MARASCA - 24/100 ml":
        $price = 50.00;
        break;
    case "Cranberry Walnut MARASCA - 24/100 ml":
        $price = 50.00;
        break;
    case "Cucumber Melon MARASCA - 24/100 ml":
        $price = 50.00;
        break;
    case "Elderberry MARASCA - 24/100 ml":
        $price = 50.00;
        break;
    case "Espresso MARASCA - 24/100 ml":
        $price = 50.00;
        break;
    case "Fig MARASCA - 24/100 ml":
        $price = 50.00;
        break;
    case "Garlic MARASCA - 24/100 ml":
        $price = 50.00;
        break;
    case "Garlic Cilantro MARASCA - 24/100 ml":
        $price = 50.00;
        break;
    case "Grapefruit MARASCA - 24/100 ml":
        $price = 50.00;
        break;
    case "Green Apple MARASCA - 24/100 ml":
        $price = 50.00;
        break;
    case "Hickory MARASCA - 24/100 ml":
        $price = 50.00;
        break;
    case "Honey MARASCA - 24/100 ml":
        $price = 50.00;
        break;
    case "Honey Ginger MARASCA - 24/100 ml":
        $price = 50.00;
        break;
    case "Huckleberry MARASCA - 24/100 ml":
        $price = 50.00;
        break;
    case "Jalapeno MARASCA - 24/100 ml":
        $price = 50.00;
        break;
    case "Jalapeno Fig in 8 Star MARASCA - 24/100 ml":
        $price = 44.00;
        break;
    case "Jalapeno & Lime MARASCA - 24/100 ml":
        $price = 50.00;
        break;
    case "Lavender MARASCA - 24/100 ml":
        $price = 50.00;
        break;
    case "Lemongrass Mint MARASCA - 24/100 ml":
        $price = 50.00;
        break;
    case "Lemon MARASCA - 24/100 ml":
        $price = 50.00;
        break;
    case "Lemon Vanilla MARASCA - 24/100 ml":
        $price = 50.00;
        break;
    case "Mandarin Orange MARASCA - 24/100 ml":
        $price = 50.00;
        break;
    case "Mango MARASCA - 24/100 ml":
        $price = 50.00;
        break;
    case "Mocha Almond Fudge MARASCA - 24/100 ml":
        $price = 50.00;
        break;
    case "Orange MARASCA - 24/100 ml":
        $price = 50.00;
        break;
    case "Orange/Mango/Passionfruit MARASCA - 24/100 ml":
        $price = 50.00;
        break;
    case "Orange Vanilla MARASCA - 24/100 ml":
        $price = 50.00;
        break;
    case "Peach MARASCA - 24/100 ml":
        $price = 50.00;
        break;
    case "Pear MARASCA - 24/100 ml":
        $price = 50.00;
        break;
    case "Pecan Praline MARASCA - 24/100 ml":
        $price = 50.00;
        break;
    case "Pineapple MARASCA - 24/100 ml":
        $price = 50.00;
        break;
    case "Pomegranate MARASCA - 24/100 ml":
        $price = 50.00;
        break;
    case "Pumpkin Spice MARASCA - 24/100 ml":
        $price = 50.00;
        break;
    case "Raspberry MARASCA - 24/100 ml":
        $price = 50.00;
        break;
    case "Raspberry Ginger MARASCA - 24/100 ml":
        $price = 50.00;
        break;
    case "Strawberry MARASCA - 24/100 ml":
        $price = 50.00;
        break;
    case "Strawberry Peach MARASCA - 24/100 ml":
        $price = 50.00;
        break;
    case "Tangerine MARASCA - 24/100 ml":
        $price = 50.00;
        break;
    case "Black Truffle MARASCA - 24/100 ml":
        $price = 50.00;
        break;
    case "Vanilla MARASCA - 24/100 ml":
        $price = 50.00;
        break;
    case "Vanilla Fig MARASCA - 24/100 ml":
        $price = 50.00;
        break;
    //marasca 250ml bottles
    //oils
    case "Olive Oil Extra Virgin MARASCA - 12/250 ml":
        $price = 29.00;
        break;
    case "Australia - Leccino/Frantoio MARASCA - 12/250 ml":
        $price = 36.00;
        break;
    case "Australia - Frantoio MARASCA - 12/250 ml":
        $price = 36.00;
        break;
    case "California Arbequina MARASCA - 12/250 ml":
        $price = 54.00;
        break;
    case "California Arbosana MARASCA - 12/250 ml":
        $price = 51.00;
        break;
    case "California Mission MARASCA - 12/250 ml":
        $price = 51.00;
        break;
    case "California Arbequina/Arbosana/Koroneiki MARASCA - 12/250 ml":
        $price = 37.00;
        break;
    case "Chile - Arbequina MARASCA - 12/250 ml":
        $price = 29.00;
        break;
    case "Chile - Frantoio MARASCA - 12/250 ml":
        $price = 29.00;
        break;
    case "Chile - Leccino MARASCA - 12/250 ml":
        $price = 29.00;
        break;
    case "Chile - Picual MARASCA - 12/250 ml":
        $price = 29.00;
        break;
    case "Greece - Kalamata MARASCA - 12/250 ml":
        $price = 33.00;
        break;
    case "Greece - Koroneiki MARASCA - 12/250 ml":
        $price = 33.00;
        break;
    case "Italy - Umbrian region MARASCA - 12/250 ml":
        $price = 49.00;
        break;
    case "Italy - Calabria region - Carolea MARASCA - 12/250 ml":
        $price = 42.00;
        break;
    case "Italy (Puglia) - Coratina MARASCA - 12/250 ml":
        $price = 42.00;
        break;
    case "Italy (Puglia)- Ogliarola MARASCA - 12/250 ml":
        $price = 42.00;
        break;
    case "Spain - Hojiblanca MARASCA - 12/250 ml":
        $price = 34.00;
        break;
    case "Spain - Picual MARASCA - 12/250 ml":
        $price = 34.00;
        break;
    case "Spain - Signature blend MARASCA - 12/250 ml":
        $price = 34.00;
        break;
    case "Tunisia - Chemlali/Chetoui MARASCA - 12/250 ml":
        $price = 29.00;
        break;
    //infused
    case "Black Pepper MARASCA - 12/250 ml":
        $price = 45.00;
        break;
    case "Garlic Oil MARASCA - 12/250 ml":
        $price = 39.00;
        break;
    case "Roasted Chili MARASCA - 12/250 ml":
        $price = 40.00;
        break;
    case "Oregano MARASCA - 12/250 ml":
        $price = 42.00;
        break;
    case "Lemon Pepper MARASCA - 12/250 ml":
        $price = 48.00;
        break;
    case "Jalapeno oil MARASCA - 12/250 ml":
        $price = 39.00;
        break;
    case "Basil MARASCA - 12/250 ml":
        $price = 39.00;
        break;
    case "Scallion MARASCA - 12/250 ml":
        $price = 42.00;
        break;
    case "Habenero MARASCA - 12/250 ml":
        $price = 51.00;
        break;
    case "Rosemary MARASCA - 12/250 ml":
        $price = 43.00;
        break;
    case "Chipotle MARASCA - 12/250 ml":
        $price = 40.00;
        break;
    //flavored
    case "Blood Orange MARASCA - 12/250 ml":
        $price = 31.00;
        break;
    case "Lemon oil MARASCA - 12/250 ml":
        $price = 35.00;
        break;
    case "Lime MARASCA - 12/250 ml":
        $price = 36.00;
        break;
    case "Orange oil MARASCA - 12/250 ml":
        $price = 31.00;
        break;
    case "Tangerine oil MARASCA - 12/250 ml":
        $price = 32.00;
        break;
    case "Butter MARASCA - 12/250 ml":
        $price = 38.00;
        break;
    case "Cranberry Walnut oil MARASCA - 12/250 ml":
        $price = 31.00;
        break;
    case "Arbequina Rhubarb MARASCA - 12/250 ml":
        $price = 31.00;
        break;
    case "Sage Onion MARASCA - 12/250 ml":
        $price = 43.00;
        break;
    case "Sundried Tomato MARASCA - 12/250 ml":
        $price = 37.00;
        break;
    case "White Truffle MARASCA - 12/250 ml":
        $price = 78.00;
        break;
    case "Black Truffle oil MARASCA - 12/250 ml":
        $price = 77.00;
        break;
    case "Hickory oil MARASCA - 12/250 ml":
        $price = 37.00;
        break;
    case "Vanilla oil MARASCA - 12/250 ml":
        $price = 37.00;
        break;
    //fused
    case "Garlic Mushroom MARASCA - 12/250 ml":
        $price = 43.00;
        break;
    case "Basil/Lemongrass MARASCA - 12/250 ml":
        $price = 44.00;
        break;
    case "Rosemary with Lavender MARASCA - 12/250 ml":
        $price = 49.00;
        break;
    case "Tuscan Herb MARASCA - 12/250 ml":
        $price = 47.00;
        break;
    case "Herbs d Provence MARASCA - 12/250 ml":
        $price = 46.00;
        break;
    case "Citrus Habanero MARASCA - 12/250 ml":
        $price = 41.00;
        break;
    case "Garlic Roasted Chili MARASCA - 12/250 ml":
        $price = 43.00;
        break;
    //specialty oils
    case "Almond Oil MARASCA - 12/250 ml":
        $price = 36.00;
        break;
    case "Apricot Oil MARASCA - 12/250 ml":
        $price = 37.00;
        break;
    case "Avocado Oil MARASCA - 12/250 ml":
        $price = 33.00;
        break;
    case "Flaxseed Oil MARASCA - 12/250 ml":
        $price = 27.00;
        break;
    case "Grapeseed Oil MARASCA - 12/250 ml":
        $price = 23.00;
        break;
    case "Hempseed Oil MARASCA - 12/250 ml":
        $price = 58.00;
        break;
    case "Macadamia Nut Oil MARASCA - 12/250 ml":
        $price = 41.00;
        break;
    case "Toasted Sesame Oil MARASCA - 12/250 ml":
        $price = 28.00;
        break;
    case "Walnut Oil MARASCA - 12/250 ml":
        $price = 26.00;
        break;
    //organics
    case "Olive Oil, Extra Virgin, Organic , Italy MARASCA - 12/250 ml":
        $price = 36.00;
        break;
    case "Olive Oil, Extra Virgin, Organic , Country Varies MARASCA - 12/250 ml":
        $price = 31.00;
        break;
    case "Canola MARASCA - 12/250 ml":
        $price = 22.00;
        break;
    case "Soybean Oil MARASCA - 12/250 ml":
        $price = 18.00;
        break;
    case "Sunflower MARASCA - 12/250 ml":
        $price = 29.00;
        break;
    case "Sesame Toasted MARASCA - 12/250 ml":
        $price = 27.00;
        break;
    case "Sesame, Virgin MARASCA - 12/250 ml":
        $price = 27.00;
        break;
    //vinegars
    case "4 Star Balsamic MARASCA - 12/250 ml":
        $price = 19.00;
        break;
    case "8 Star Balsamic MARASCA - 12/250 ml":
        $price = 26.00;
        break;
    case "12 Star Balsamic MARASCA - 12/250 ml":
        $price = 30.00;
        break;
    case "25 Star Dark Balsamic MARASCA - 12/250 ml":
        $price = 43.00;
        break;
    case "25 Star White Balsamic MARASCA - 12/250 ml":
        $price = 43.00;
        break;
    case "6 Star White Balsamic MARASCA - 12/250 ml":
        $price = 25.00;
        break;
    case "Red Wine Vinegar - Domestic - 50 Grain MARASCA - 12/250 ml":
        $price = 16.00;
        break;
    case "Organic Premium Quality Balsamic - Ltd Edition MARASCA - 12/250 ml":
        $price = 45.00;
        break;
    case "Serrano Chile Vinegar MARASCA - 12/250 ml":
        $price = 47.00;
        break;
    case "Lambrusco Vinegar MARASCA - 12/250 ml":
        $price = 43.00;
        break;
//flavored balsamic
    case "Almond Creme MARASCA - 12/250 ml":
        $price = 43.00;
        break;
    case "Apricot MARASCA - 12/250 ml":
        $price = 43.00;
        break;
    case "Bittersweet Chocolate with Orange MARASCA - 12/250 ml":
        $price = 43.00;
        break;
    case "Blackberry MARASCA - 12/250 ml":
        $price = 43.00;
        break;
    case "Blackberry Ginger MARASCA - 12/250 ml":
        $price = 43.00;
        break;
    case "Black Currant MARASCA - 12/250 ml":
        $price = 43.00;
        break;
    case "Black Walnut MARASCA - 12/250 ml":
        $price = 43.00;
        break;
    case "Blueberry MARASCA - 12/250 ml":
        $price = 43.00;
        break;
    case "Bordeaux Cherry MARASCA - 12/250 ml":
        $price = 43.00;
        break;
    case "Chili MARASCA - 12/250 ml":
        $price = 43.00;
        break;
    case "Chocolate MARASCA - 12/250 ml":
        $price = 43.00;
        break;
    case "Chocolate Covered Cherries MARASCA - 12/250 ml":
        $price = 43.00;
        break;
    case "Chocolate Jalapeno MARASCA - 12/250 ml":
        $price = 43.00;
        break;
    case "Chocolate Marshmallow MARASCA - 12/250 ml":
        $price = 43.00;
        break;
    case "Coconut MARASCA - 12/250 ml":
        $price = 43.00;
        break;
    case "Cranberry Orange MARASCA - 12/250 ml":
        $price = 43.00;
        break;
    case "Cranberry Walnut MARASCA - 12/250 ml":
        $price = 43.00;
        break;
    case "Cucumber Melon MARASCA - 12/250 ml":
        $price = 43.00;
        break;
    case "Elderberry MARASCA - 12/250 ml":
        $price = 43.00;
        break;
    case "Espresso MARASCA - 12/250 ml":
        $price = 43.00;
        break;
    case "Fig MARASCA - 12/250 ml":
        $price = 43.00;
        break;
    case "Garlic MARASCA - 12/250 ml":
        $price = 43.00;
        break;
    case "Garlic Oil Cilantro MARASCA - 12/250 ml":
        $price = 43.00;
        break;
    case "Grapefruit MARASCA - 12/250 ml":
        $price = 43.00;
        break;
    case "Green Apple MARASCA - 12/250 ml":
        $price = 43.00;
        break;
    case "Hickory MARASCA - 12/250 ml":
        $price = 43.00;
        break;
    case "Honey MARASCA - 12/250 ml":
        $price = 43.00;
        break;
    case "Honey Ginger MARASCA - 12/250 ml":
        $price = 43.00;
        break;
    case "Huckleberry MARASCA - 12/250 ml":
        $price = 43.00;
        break;
    case "Jalapeno MARASCA - 12/250 ml":
        $price = 43.00;
        break;
    case "Jalapeno Fig in 8 Star MARASCA - 12/250 ml":
        $price = 36.00;
        break;
    case "Jalapeno & Lime MARASCA - 12/250 ml":
        $price = 43.00;
        break;
    case "Lavender MARASCA - 12/250 ml":
        $price = 43.00;
        break;
    case "Lemongrass Mint MARASCA - 12/250 ml":
        $price = 43.00;
        break;
    case "Lemon MARASCA - 12/250 ml":
        $price = 43.00;
        break;
    case "Lemon Vanilla MARASCA - 12/250 ml":
        $price = 43.00;
        break;
    case "Mandarin Orange MARASCA - 12/250 ml":
        $price = 43.00;
        break;
    case "Mango MARASCA - 12/250 ml":
        $price = 43.00;
        break;
    case "Mocha Almond Fudge MARASCA - 12/250 ml":
        $price = 43.00;
        break;
    case "Orange MARASCA - 12/250 ml":
        $price = 43.00;
        break;
    case "Orange/Mango/Passionfruit MARASCA - 12/250 ml":
        $price = 43.00;
        break;
    case "Orange Vanilla MARASCA - 12/250 ml":
        $price = 43.00;
        break;
    case "Peach MARASCA - 12/250 ml":
        $price = 43.00;
        break;
    case "Pear MARASCA - 12/250 ml":
        $price = 43.00;
        break;
    case "Pecan Praline MARASCA - 12/250 ml":
        $price = 43.00;
        break;
    case "Pineapple MARASCA - 12/250 ml":
        $price = 43.00;
        break;
    case "Pomegranate MARASCA - 12/250 ml":
        $price = 43.00;
        break;
    case "Pumpkin Spice MARASCA - 12/250 ml":
        $price = 43.00;
        break;
    case "Raspberry MARASCA - 12/250 ml":
        $price = 43.00;
        break;
    case "Raspberry Ginger MARASCA - 12/250 ml":
        $price = 43.00;
        break;
    case "Strawberry MARASCA - 12/250 ml":
        $price = 43.00;
        break;
    case "Strawberry Peach MARASCA - 12/250 ml":
        $price = 43.00;
        break;
    case "Tangerine MARASCA - 12/250 ml":
        $price = 43.00;
        break;
    case "Black Truffle MARASCA - 12/250 ml":
        $price = 43.00;
        break;
    case "Vanilla MARASCA - 12/250 ml":
        $price = 43.00;
        break;
    case "Vanilla Fig MARASCA - 12/250 ml":
        $price = 43.00;
        break;
//marasca 500ml
//oils
    case "Olive Oil Extra Virgin MARASCA - 12/500 ml":
        $price = 48.00;
        break;
    case "Australia - Leccino/Frantoio MARASCA - 12/500 ml":
        $price = 63.00;
        break;
    case "Australia - Frantoio MARASCA - 12/500 ml":
        $price = 63.00;
        break;
    case "California Arbequina MARASCA - 12/500 ml":
        $price = 95.00;
        break;
    case "California Arbosana MARASCA - 12/500 ml":
        $price = 90.00;
        break;
    case "California Mission MARASCA - 12/500 ml":
        $price = 90.00;
        break;
    case "California Arbequina/Arbosana/Koroneiki MARASCA - 12/500 ml":
        $price = 63.00;
        break;
    case "Chile - Arbequina MARASCA - 12/500 ml":
        $price = 49.00;
        break;
    case "Chile - Frantoio MARASCA - 12/500 ml":
        $price = 49.00;
        break;
    case "Chile - Leccino MARASCA - 12/500 ml":
        $price = 49.00;
        break;
    case "Chile - Picual MARASCA - 12/500 ml":
        $price = 49.00;
        break;
    case "Greece - Kalamata MARASCA - 12/500 ml":
        $price = 56.00;
        break;
    case "Greece - Koroneiki MARASCA - 12/500 ml":
        $price = 56.00;
        break;
    case "Italy - Umbrian region MARASCA - 12/500 ml":
        $price = 87.00;
        break;
    case "Italy - Calabria region - Carolea MARASCA - 12/500 ml":
        $price = 73.00;
        break;
    case "Italy (Puglia) - Coratina MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Italy (Puglia)- Ogliarola MARASCA - 12/500 ml":
        $price = 73.00;
        break;
    case "Spain - Hojiblanca MARASCA - 12/500 ml":
        $price = 58.00;
        break;
    case "Spain - Picual MARASCA - 12/500 ml":
        $price = 58.00;
        break;
    case "Spain - Signature blend MARASCA - 12/500 ml":
        $price = 58.00;
        break;
    case "Tunisia - Chemlali/Chetoui MARASCA - 12/500 ml":
        $price = 48.00;
        break;
//infused
    case "Black Pepper MARASCA - 12/500 ml":
        $price = 80.00;
        break;
    case "Garlic Oil MARASCA - 12/500 ml":
        $price = 67.00;
        break;
    case "Roasted Chili MARASCA - 12/500 ml":
        $price = 71.00;
        break;
    case "Oregano MARASCA - 12/500 ml":
        $price = 73.00;
        break;
    case "Lemon Pepper MARASCA - 12/500 ml":
        $price = 86.00;
        break;
    case "Jalapeno oil MARASCA - 12/500 ml":
        $price = 67.00;
        break;
    case "Basil MARASCA - 12/500 ml":
        $price = 68.00;
        break;
    case "Scallion MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Habenero MARASCA - 12/500 ml":
        $price = 91.00;
        break;
    case "Rosemary MARASCA - 12/500 ml":
        $price = 76.00;
        break;
    case "Chipotle MARASCA - 12/500 ml":
        $price = 71.00;
        break;
//flavored olive oils
    case "Blood Orange MARASCA - 12/500 ml":
        $price = 52.00;
        break;
    case "Lemon oil MARASCA - 12/500 ml":
        $price = 60.00;
        break;
    case "Lime MARASCA - 12/500 ml":
        $price = 62.00;
        break;
    case "Orange oil MARASCA - 12/500 ml":
        $price = 52.00;
        break;
    case "Tangerine oil MARASCA - 12/500 ml":
        $price = 54.00;
        break;
    case "Butter MARASCA - 12/500 ml":
        $price = 67.00;
        break;
    case "Cranberry Walnut oil MARASCA - 12/500 ml":
        $price = 53.00;
        break;
    case "Arbequina Rhubarb MARASCA - 12/500 ml":
        $price = 52.00;
        break;
    case "Sage Onion MARASCA - 12/500 ml":
        $price = 76.00;
        break;
    case "Sundried Tomato MARASCA - 12/500 ml":
        $price = 63.00;
        break;
    case "White Truffle MARASCA - 12/500 ml":
        $price = 141.00;
        break;
    case "Black Truffle oil MARASCA - 12/500 ml":
        $price = 138.00;
        break;
    case "Hickory oil MARASCA - 12/500 ml":
        $price = 63.00;
        break;
    case "Vanilla oil MARASCA - 12/500 ml":
        $price = 63.00;
        break;
    //fused
    case "Garlic Mushroom MARASCA - 12/500 ml":
        $price = 76.00;
        break;
    case "Basil/Lemongrass MARASCA - 12/500 ml":
        $price = 77.00;
        break;
    case "Rosemary with Lavender MARASCA - 12/500 ml":
        $price = 86.00;
        break;
    case "Tuscan Herb MARASCA - 12/500 ml":
        $price = 83.00;
        break;
    case "Herbs d Provence MARASCA - 12/500 ml":
        $price = 80.00;
        break;
    case "Citrus Habanero MARASCA - 12/500 ml":
        $price = 71.00;
        break;
    case "Garlic Roasted Chili MARASCA - 12/500 ml":
        $price = 75.00;
        break;
//specialty oils
    case "Almond Oil MARASCA - 12/500 ml":
        $price = 62.00;
        break;
    case "Apricot Oil MARASCA - 12/500 ml":
        $price = 65.00;
        break;
    case "Avocado Oil MARASCA - 12/500 ml":
        $price = 56.00;
        break;
    case "Flaxseed Oil MARASCA - 12/500 ml":
        $price = 45.00;
        break;
    case "Grapeseed Oil MARASCA - 12/500 ml":
        $price = 38.00;
        break;
    case "Hempseed Oil MARASCA - 12/500 ml":
        $price = 103.00;
        break;
    case "Macadamia Nut Oil MARASCA - 12/500 ml":
        $price = 73.00;
        break;
    case "Toasted Sesame Oil MARASCA - 12/500 ml":
        $price = 48.00;
        break;
    case "Walnut Oil MARASCA - 12/500 ml":
        $price = 43.00;
        break;
    //organics
    case "Olive Oil, Extra Virgin, Organic , Italy MARASCA - 12/500 ml":
        $price = 63.00;
        break;
    case "Olive Oil, Extra Virgin, Organic , Country Varies MARASCA - 12/500 ml":
        $price = 53.00;
        break;
    case "Canola MARASCA - 12/500 ml":
        $price = 35.00;
        break;
    case "Soybean Oil MARASCA - 12/500 ml":
        $price = 27.00;
        break;
    case "Sunflower MARASCA - 12/500 ml":
        $price = 49.00;
        break;
    case "Sesame Toasted MARASCA - 12/500 ml":
        $price = 46.00;
        break;
    case "Sesame, Virgin MARASCA - 12/500 ml":
        $price = 46.00;
        break;
//vinegars
    case "4 Star Balsamic MARASCA - 12/500 ml":
        $price = 30.00;
        break;
    case "8 Star Balsamic MARASCA - 12/500 ml":
        $price = 41.00;
        break;
    case "12 Star Balsamic MARASCA - 12/500 ml":
        $price = 47.00;
        break;
    case "25 Star Dark Balsamic MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "25 Star White Balsamic MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "6 Star White Balsamic MARASCA - 12/500 ml":
        $price = 40.00;
        break;
    case "Red Wine Vinegar - Domestic - 50 Grain MARASCA - 12/500 ml":
        $price = 23.00;
        break;
    case "Organic Premium Quality Balsamic - Ltd Edition MARASCA - 12/500 ml":
        $price = 79.00;
        break;
    case "Serrano Chile Vinegar MARASCA - 12/500 ml":
        $price = 82.00;
        break;
    case "Lambrusco Vinegar MARASCA - 12/500 ml":
        $price = 74.00;
        break;
//flavored balsamic
    case "Almond Creme MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Apricot MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Bittersweet Chocolate with Orange MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Blackberry MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Blackberry Ginger MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Black Currant MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Black Walnut MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Blueberry MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Bordeaux Cherry MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Chili MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Chocolate MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Chocolate Covered Cherries MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Chocolate Jalapeno MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Chocolate Marshmallow MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Coconut MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Cranberry Orange MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Cranberry Walnut MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Cucumber Melon MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Elderberry MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Espresso MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Fig MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Garlic MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Garlic Cilantro MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Grapefruit MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Green Apple MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Hickory MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Honey MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Honey Ginger MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Huckleberry MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Jalapeno MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Jalapeno Fig in 8 Star MARASCA - 12/500 ml":
        $price = 59.00;
        break;
    case "Jalapeno & Lime MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Lavender MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Lemongrass Mint MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Lemon MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Lemon Vanilla MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Mandarin Orange MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Mango MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Mocha Almond Fudge MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Orange MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Orange/Mango/Passionfruit MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Orange Vanilla MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Peach MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Pear MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Pecan Praline MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Pineapple MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Pomegranate MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Pumpkin Spice MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Raspberry MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Raspberry Ginger MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Strawberry MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Strawberry Peach MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Tangerine MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Black Truffle MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Vanilla MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Vanilla Fig MARASCA - 12/500 ml":
        $price = 74.00;
        break;
//GREEN MARASCA 500ml
//oils
    case "Olive Oil Extra Virgin GREEN MARASCA - 12/500 ml":
        $price = 48.00;
        break;
    case "Australia - Leccino/Frantoio GREEN MARASCA - 12/500 ml":
        $price = 63.00;
        break;
    case "Australia - Frantoio GREEN MARASCA - 12/500 ml":
        $price = 63.00;
        break;
    case "California Arbequina GREEN MARASCA - 12/500 ml":
        $price = 95.00;
        break;
    case "California Arbosana GREEN MARASCA - 12/500 ml":
        $price = 90.00;
        break;
    case "California Mission GREEN MARASCA - 12/500 ml":
        $price = 90.00;
        break;
    case "California Arbequina/Arbosana/Koroneiki GREEN MARASCA - 12/500 ml":
        $price = 63.00;
        break;
    case "Chile - Arbequina GREEN MARASCA - 12/500 ml":
        $price = 49.00;
        break;
    case "Chile - Frantoio GREEN MARASCA - 12/500 ml":
        $price = 49.00;
        break;
    case "Chile - Leccino GREEN MARASCA - 12/500 ml":
        $price = 49.00;
        break;
    case "Chile - Picual GREEN MARASCA - 12/500 ml":
        $price = 49.00;
        break;
    case "Greece - Kalamata GREEN MARASCA - 12/500 ml":
        $price = 56.00;
        break;
    case "Greece - Koroneiki GREEN MARASCA - 12/500 ml":
        $price = 56.00;
        break;
    case "Italy - Umbrian region GREEN MARASCA - 12/500 ml":
        $price = 87.00;
        break;
    case "Italy - Calabria region - Carolea GREEN MARASCA - 12/500 ml":
        $price = 73.00;
        break;
    case "Italy (Puglia) - Coratina GREEN MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Italy (Puglia)- Ogliarola GREEN MARASCA - 12/500 ml":
        $price = 73.00;
        break;
    case "Spain - Hojiblanca GREEN MARASCA - 12/500 ml":
        $price = 58.00;
        break;
    case "Spain - Picual GREEN MARASCA - 12/500 ml":
        $price = 58.00;
        break;
    case "Spain - Signature blend GREEN MARASCA - 12/500 ml":
        $price = 58.00;
        break;
    case "Tunisia - Chemlali/Chetoui GREEN MARASCA - 12/500 ml":
        $price = 48.00;
        break;
//infused
    case "Black Pepper GREEN MARASCA - 12/500 ml":
        $price = 80.00;
        break;
    case "Garlic Oil GREEN MARASCA - 12/500 ml":
        $price = 67.00;
        break;
    case "Roasted Chili GREEN MARASCA - 12/500 ml":
        $price = 71.00;
        break;
    case "Oregano GREEN MARASCA - 12/500 ml":
        $price = 73.00;
        break;
    case "Lemon Pepper GREEN MARASCA - 12/500 ml":
        $price = 86.00;
        break;
    case "Jalapeno oil GREEN MARASCA - 12/500 ml":
        $price = 67.00;
        break;
    case "Basil GREEN MARASCA - 12/500 ml":
        $price = 68.00;
        break;
    case "Scallion GREEN MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Habenero GREEN MARASCA - 12/500 ml":
        $price = 91.00;
        break;
    case "Rosemary GREEN MARASCA - 12/500 ml":
        $price = 76.00;
        break;
    case "Chipotle GREEN MARASCA - 12/500 ml":
        $price = 71.00;
        break;
//flavored olive oils
    case "Blood Orange GREEN MARASCA - 12/500 ml":
        $price = 52.00;
        break;
    case "Lemon oil GREEN MARASCA - 12/500 ml":
        $price = 60.00;
        break;
    case "Lime GREEN MARASCA - 12/500 ml":
        $price = 62.00;
        break;
    case "Orange oil GREEN MARASCA - 12/500 ml":
        $price = 52.00;
        break;
    case "Tangerine oil GREEN MARASCA - 12/500 ml":
        $price = 54.00;
        break;
    case "Butter GREEN MARASCA - 12/500 ml":
        $price = 67.00;
        break;
    case "Cranberry Walnut oil GREEN MARASCA - 12/500 ml":
        $price = 53.00;
        break;
    case "Arbequina Rhubarb GREEN MARASCA - 12/500 ml":
        $price = 52.00;
        break;
    case "Sage Onion GREEN MARASCA - 12/500 ml":
        $price = 76.00;
        break;
    case "Sundried Tomato GREEN MARASCA - 12/500 ml":
        $price = 63.00;
        break;
    case "White Truffle GREEN MARASCA - 12/500 ml":
        $price = 141.00;
        break;
    case "Black Truffle oil GREEN MARASCA - 12/500 ml":
        $price = 138.00;
        break;
    case "Hickory oil GREEN MARASCA - 12/500 ml":
        $price = 63.00;
        break;
    case "Vanilla oil GREEN MARASCA - 12/500 ml":
        $price = 63.00;
        break;
    //fused
    case "Garlic Mushroom GREEN MARASCA - 12/500 ml":
        $price = 76.00;
        break;
    case "Basil/Lemongrass GREEN MARASCA - 12/500 ml":
        $price = 77.00;
        break;
    case "Rosemary with Lavender GREEN MARASCA - 12/500 ml":
        $price = 86.00;
        break;
    case "Tuscan Herb GREEN MARASCA - 12/500 ml":
        $price = 83.00;
        break;
    case "Herbs d Provence GREEN MARASCA - 12/500 ml":
        $price = 80.00;
        break;
    case "Citrus Habanero GREEN MARASCA - 12/500 ml":
        $price = 71.00;
        break;
    case "Garlic Roasted Chili GREEN MARASCA - 12/500 ml":
        $price = 75.00;
        break;
//specialty oils
    case "Almond Oil GREEN MARASCA - 12/500 ml":
        $price = 62.00;
        break;
    case "Apricot Oil GREEN MARASCA - 12/500 ml":
        $price = 65.00;
        break;
    case "Avocado Oil GREEN MARASCA - 12/500 ml":
        $price = 56.00;
        break;
    case "Flaxseed Oil GREEN MARASCA - 12/500 ml":
        $price = 45.00;
        break;
    case "Grapeseed Oil GREEN MARASCA - 12/500 ml":
        $price = 38.00;
        break;
    case "Hempseed Oil GREEN MARASCA - 12/500 ml":
        $price = 103.00;
        break;
    case "Macadamia Nut Oil GREEN MARASCA - 12/500 ml":
        $price = 73.00;
        break;
    case "Toasted Sesame Oil GREEN MARASCA - 12/500 ml":
        $price = 48.00;
        break;
    case "Walnut Oil GREEN MARASCA - 12/500 ml":
        $price = 43.00;
        break;
    //organics
    case "Olive Oil, Extra Virgin, Organic , Italy GREEN MARASCA - 12/500 ml":
        $price = 63.00;
        break;
    case "Olive Oil, Extra Virgin, Organic , Country Varies GREEN MARASCA - 12/500 ml":
        $price = 53.00;
        break;
    case "Canola GREEN MARASCA - 12/500 ml":
        $price = 35.00;
        break;
    case "Soybean Oil GREEN MARASCA - 12/500 ml":
        $price = 27.00;
        break;
    case "Sunflower GREEN MARASCA - 12/500 ml":
        $price = 49.00;
        break;
    case "Sesame Toasted GREEN MARASCA - 12/500 ml":
        $price = 46.00;
        break;
    case "Sesame, Virgin GREEN MARASCA - 12/500 ml":
        $price = 46.00;
        break;
//vinegars
    case "4 Star Balsamic GREEN MARASCA - 12/500 ml":
        $price = 30.00;
        break;
    case "8 Star Balsamic GREEN MARASCA - 12/500 ml":
        $price = 41.00;
        break;
    case "12 Star Balsamic GREEN MARASCA - 12/500 ml":
        $price = 47.00;
        break;
    case "25 Star Dark Balsamic GREEN MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "25 Star White Balsamic GREEN MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "6 Star White Balsamic GREEN MARASCA - 12/500 ml":
        $price = 40.00;
        break;
    case "Red Wine Vinegar - Domestic - 50 Grain GREEN MARASCA - 12/500 ml":
        $price = 23.00;
        break;
    case "Organic Premium Quality Balsamic - Ltd Edition GREEN MARASCA - 12/500 ml":
        $price = 79.00;
        break;
    case "Serrano Chile Vinegar GREEN MARASCA - 12/500 ml":
        $price = 82.00;
        break;
    case "Lambrusco Vinegar GREEN MARASCA - 12/500 ml":
        $price = 74.00;
        break;
//flavored balsamic
    case "Almond Creme GREEN MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Apricot GREEN MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Bittersweet Chocolate with Orange GREEN MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Blackberry GREEN MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Blackberry Ginger GREEN MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Black Currant GREEN MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Black Walnut GREEN MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Blueberry GREEN MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Bordeaux Cherry GREEN MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Chili GREEN MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Chocolate GREEN MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Chocolate Covered Cherries GREEN MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Chocolate Jalapeno GREEN MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Chocolate Marshmallow GREEN MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Coconut GREEN MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Cranberry Orange GREEN MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Cranberry Walnut GREEN MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Cucumber Melon GREEN MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Elderberry GREEN MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Espresso GREEN MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Fig GREEN MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Garlic GREEN MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Garlic Cilantro GREEN MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Grapefruit GREEN MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Green Apple GREEN MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Hickory GREEN MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Honey GREEN MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Honey Ginger GREEN MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Huckleberry GREEN MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Jalapeno GREEN MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Jalapeno Fig in 8 Star GREEN MARASCA - 12/500 ml":
        $price = 59.00;
        break;
    case "Jalapeno & Lime GREEN MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Lavender GREEN MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Lemongrass Mint GREEN MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Lemon GREEN MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Lemon Vanilla GREEN MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Mandarin Orange GREEN MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Mango GREEN MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Mocha Almond Fudge GREEN MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Orange GREEN MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Orange/Mango/Passionfruit GREEN MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Orange Vanilla GREEN MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Peach GREEN MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Pear GREEN MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Pecan Praline GREEN MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Pineapple GREEN MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Pomegranate GREEN MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Pumpkin Spice GREEN MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Raspberry GREEN MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Raspberry Ginger GREEN MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Strawberry GREEN MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Strawberry Peach GREEN MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Tangerine GREEN MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Black Truffle GREEN MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Vanilla GREEN MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Vanilla Fig GREEN MARASCA - 12/500 ml":
        $price = 74.00;
        break;
//green marasca 750ml
//oils
    case "Olive Oil Extra Virgin GREEN MARASCA - 12/750 ml":
        $price = 69.00;
        break;
    case "Australia - Leccino/Frantoio GREEN MARASCA - 12/750 ml":
        $price = 92.00;
        break;
    case "Australia - Frantoio GREEN MARASCA - 12/750 ml":
        $price = 92.00;
        break;
    case "California Arbequina GREEN MARASCA - 12/750 ml":
        $price = 143.00;
        break;
    case "California Arbosana GREEN MARASCA - 12/750 ml":
        $price = 1360.00;
        break;
    case "California Mission GREEN MARASCA - 12/750 ml":
        $price = 136.00;
        break;
    case "California Arbequina/Arbosana/Koroneiki GREEN MARASCA - 12/750 ml":
        $price = 92.00;
        break;
    case "Chile - Arbequina GREEN MARASCA - 12/750 ml":
        $price = 71.00;
        break;
    case "Chile - Frantoio GREEN MARASCA - 12/750 ml":
        $price = 71.00;
        break;
    case "Chile - Leccino GREEN MARASCA - 12/750 ml":
        $price = 71.00;
        break;
    case "Chile - Picual GREEN MARASCA - 12/750 ml":
        $price = 71.00;
        break;
    case "Greece - Kalamata GREEN MARASCA - 12/750 ml":
        $price = 81.00;
        break;
    case "Greece - Koroneiki GREEN MARASCA - 12/750 ml":
        $price = 81.00;
        break;
    case "Italy - Umbrian region GREEN MARASCA - 12/750 ml":
        $price = 130.00;
        break;
    case "Italy - Calabria region - Carolea GREEN MARASCA - 12/750 ml":
        $price = 109.00;
        break;
    case "Italy (Puglia) - Coratina GREEN MARASCA - 12/750 ml":
        $price = 109.00;
        break;
    case "Italy (Puglia)- Ogliarola GREEN MARASCA - 12/750 ml":
        $price = 109.00;
        break;
    case "Spain - Hojiblanca GREEN MARASCA - 12/750 ml":
        $price = 85.00;
        break;
    case "Spain - Picual GREEN MARASCA - 12/750 ml":
        $price = 85.00;
        break;
    case "Spain - Signature blend GREEN MARASCA - 12/750 ml":
        $price = 85.00;
        break;
    case "Tunisia - Chemlali/Chetoui GREEN MARASCA - 12/750 ml":
        $price = 69.00;
        break;
    //infused
    case "Black Pepper GREEN MARASCA - 12/750 ml":
        $price = 120.00;
        break;
    case "Garlic Oil GREEN MARASCA - 12/750 ml":
        $price = 100.00;
        break;
    case "Roasted Chili GREEN MARASCA - 12/750 ml":
        $price = 105.00;
        break;
    case "Oregano GREEN MARASCA - 12/750 ml":
        $price = 109.00;
        break;
    case "Lemon Pepper GREEN MARASCA - 12/750 ml":
        $price = 129.00;
        break;
    case "Jalapeno oil GREEN MARASCA - 12/750 ml":
        $price = 100.00;
        break;
    case "Basil GREEN MARASCA - 12/750 ml":
        $price = 102.00;
        break;
    case "Scallion GREEN MARASCA - 12/750 ml":
        $price = 111.00;
        break;
    case "Habenero GREEN MARASCA - 12/750 ml":
        $price = 136.00;
        break;
    case "Rosemary GREEN MARASCA - 12/750 ml":
        $price = 113.00;
        break;
    case "Chipotle GREEN MARASCA - 12/750 ml":
        $price = 105.00;
        break;
//flavored olive oils
    case "Blood Orange GREEN MARASCA - 12/750 ml":
        $price = 76.00;
        break;
    case "Lemon oil GREEN MARASCA - 12/750 ml":
        $price = 88.00;
        break;
    case "Lime GREEN MARASCA - 12/750 ml":
        $price = 91.00;
        break;
    case "Orange oil GREEN MARASCA - 12/750 ml":
        $price = 76.00;
        break;
    case "Tangerine oil GREEN MARASCA - 12/750 ml":
        $price = 78.00;
        break;
    case "Butter GREEN MARASCA - 12/750 ml":
        $price = 98.00;
        break;
    case "Cranberry Walnut oil GREEN MARASCA - 12/750 ml":
        $price = 77.00;
        break;
    case "Arbequina Rhubarb GREEN MARASCA - 12/750 ml":
        $price = 76.00;
        break;
    case "Sage Onion GREEN MARASCA - 12/750 ml":
        $price = 113.00;
        break;
    case "Sundried Tomato GREEN MARASCA - 12/750 ml":
        $price = 93.00;
        break;
    case "White Truffle GREEN MARASCA - 12/750 ml":
        $price = 213.00;
        break;
    case "Black Truffle oil GREEN MARASCA - 12/750 ml":
        $price = 209.00;
        break;
    case "Hickory oil GREEN MARASCA - 12/750 ml":
        $price = 93.00;
        break;
    case "Vanilla oil GREEN MARASCA - 12/750 ml":
        $price = 93.00;
        break;
//fused
    case "Garlic Mushroom GREEN MARASCA - 12/750 ml":
        $price = 113.00;
        break;
    case "Basil/Lemongrass GREEN MARASCA - 12/750 ml":
        $price = 114.00;
        break;
    case "Rosemary with Lavender GREEN MARASCA - 12/750 ml":
        $price = 129.00;
        break;
    case "Tuscan Herb GREEN MARASCA - 12/750 ml":
        $price = 125.00;
        break;
    case "Herbs d Provence GREEN MARASCA - 12/750 ml":
        $price = 120.00;
        break;
    case "Citrus Habanero GREEN MARASCA - 12/750 ml":
        $price = 105.00;
        break;
    case "Garlic Roasted Chili GREEN MARASCA - 12/750 ml":
        $price = 112.00;
        break;
//specialty oils
    case "Almond Oil GREEN MARASCA - 12/750 ml":
        $price = 92.00;
        break;
    case "Apricot Oil GREEN MARASCA - 12/750 ml":
        $price = 96.00;
        break;
    case "Avocado Oil GREEN MARASCA - 12/750 ml":
        $price = 83.00;
        break;
    case "Flaxseed Oil GREEN MARASCA - 12/750 ml":
        $price = 65.00;
        break;
    case "Grapeseed Oil GREEN MARASCA - 12/750 ml":
        $price = 54.00;
        break;
    case "Hempseed Oil GREEN MARASCA - 12/750 ml":
        $price = 156.00;
        break;
    case "Macadamia Nut Oil GREEN MARASCA - 12/750 ml":
        $price = 108.00;
        break;
    case "Toasted Sesame Oil GREEN MARASCA - 12/750 ml":
        $price = 69.00;
        break;
    case "Walnut Oil GREEN MARASCA - 12/750 ml":
        $price = 62.00;
        break;
//organics
    case "Olive Oil, Extra Virgin, Organic , Italy GREEN MARASCA - 12/750 ml":
        $price = 93.00;
        break;
    case "Olive Oil, Extra Virgin, Organic , Country Varies GREEN MARASCA - 12/750 ml":
        $price = 78.00;
        break;
    case "Canola GREEN MARASCA - 12/750 ml":
        $price = 50.00;
        break;
    case "Soybean Oil GREEN MARASCA - 12/750 ml":
        $price = 37.00;
        break;
    case "Sunflower GREEN MARASCA - 12/750 ml":
        $price = 71.00;
        break;
    case "Sesame Toasted GREEN MARASCA - 12/750 ml":
        $price = 66.00;
        break;
    case "Sesame, Virgin GREEN MARASCA - 12/750 ml":
        $price = 66.00;
        break;
//vinegars
    case "4 Star Balsamic GREEN MARASCA - 12/750 ml":
        $price = 40.00;
        break;
    case "8 Star Balsamic GREEN MARASCA - 12/750 ml":
        $price = 59.00;
        break;
    case "12 Star Balsamic GREEN MARASCA - 12/750 ml":
        $price = 68.00;
        break;
    case "25 Star Dark Balsamic GREEN MARASCA - 12/750 ml":
        $price = 109.00;
        break;
    case "25 Star White Balsamic GREEN MARASCA - 12/750 ml":
        $price = 109.00;
        break;
    case "6 Star White Balsamic GREEN MARASCA - 12/750 ml":
        $price = 56.00;
        break;
    case "Red Wine Vinegar - Domestic - 50 Grain GREEN MARASCA - 12/750 ml":
        $price = 29.00;
        break;
    case "Organic Premium Quality Balsamic - Ltd Edition GREEN MARASCA - 12/750 ml":
        $price = 118.00;
        break;
    case "Serrano Chile Vinegar GREEN MARASCA - 12/750 ml":
        $price = 121.00;
        break;
    case "Lambrusco Vinegar GREEN MARASCA - 12/750 ml":
        $price = 109.00;
        break;
    //flavored balsamics

    case "Almond Creme GREEN MARASCA - 12/750 ml":
        $price = 109.00;
        break;
    case "Apricot GREEN MARASCA - 12/750 ml":
        $price = 109.00;
        break;
    case "Bittersweet Chocolate with Orange GREEN MARASCA - 12/750 ml":
        $price = 109.00;
        break;
    case "Blackberry GREEN MARASCA - 12/750 ml":
        $price = 109.00;
        break;
    case "Blackberry Ginger GREEN MARASCA - 12/750 ml":
        $price = 109.00;
        break;
    case "Black Currant GREEN MARASCA - 12/750 ml":
        $price = 109.00;
        break;
    case "Black Walnut GREEN MARASCA - 12/750 ml":
        $price = 109.00;
        break;
    case "Blueberry GREEN MARASCA - 12/750 ml":
        $price = 109.00;
        break;
    case "Bordeaux Cherry GREEN MARASCA - 12/750 ml":
        $price = 109.00;
        break;
    case "Chili GREEN MARASCA - 12/750 ml":
        $price = 109.00;
        break;
    case "Chocolate GREEN MARASCA - 12/750 ml":
        $price = 109.00;
        break;
    case "Chocolate Covered Cherries GREEN MARASCA - 12/750 ml":
        $price = 109.00;
        break;
    case "Chocolate Jalapeno GREEN MARASCA - 12/750 ml":
        $price = 109.00;
        break;
    case "Chocolate Marshmallow GREEN MARASCA - 12/750 ml":
        $price = 109.00;
        break;
    case "Coconut GREEN MARASCA - 12/750 ml":
        $price = 109.00;
        break;
    case "Cranberry Orange GREEN MARASCA - 12/750 ml":
        $price = 109.00;
        break;
    case "Cranberry Walnut GREEN MARASCA - 12/750 ml":
        $price = 109.00;
        break;
    case "Cucumber Melon GREEN MARASCA - 12/750 ml":
        $price = 109.00;
        break;
    case "Elderberry GREEN MARASCA - 12/750 ml":
        $price = 109.00;
        break;
    case "Espresso GREEN MARASCA - 12/750 ml":
        $price = 109.00;
        break;
    case "Fig GREEN MARASCA - 12/750 ml":
        $price = 109.00;
        break;
    case "Garlic GREEN MARASCA - 12/750 ml":
        $price = 109.00;
        break;
    case "Garlic Cilantro GREEN MARASCA - 12/750 ml":
        $price = 109.00;
        break;
    case "Grapefruit GREEN MARASCA - 12/750 ml":
        $price = 109.00;
        break;
    case "Green Apple GREEN MARASCA - 12/750 ml":
        $price = 109.00;
        break;
    case "Hickory GREEN MARASCA - 12/750 ml":
        $price = 109.00;
        break;
    case "Honey GREEN MARASCA - 12/750 ml":
        $price = 109.00;
        break;
    case "Honey Ginger GREEN MARASCA - 12/750 ml":
        $price = 109.00;
        break;
    case "Huckleberry GREEN MARASCA - 12/750 ml":
        $price = 109.00;
        break;
    case "Jalapeno GREEN MARASCA - 12/750 ml":
        $price = 109.00;
        break;
    case "Jalapeno Fig in 8 Star GREEN MARASCA - 12/750 ml":
        $price = 85.00;
        break;
    case "Jalapeno & Lime GREEN MARASCA - 12/750 ml":
        $price = 109.00;
        break;
    case "Lavender GREEN MARASCA - 12/750 ml":
        $price = 109.00;
        break;
    case "Lemongrass Mint GREEN MARASCA - 12/750 ml":
        $price = 109.00;
        break;
    case "Lemon GREEN MARASCA - 12/750 ml":
        $price = 109.00;
        break;
    case "Lemon Vanilla GREEN MARASCA - 12/750 ml":
        $price = 109.00;
        break;
    case "Mandarin Orange GREEN MARASCA - 12/750 ml":
        $price = 109.00;
        break;
    case "Mango GREEN MARASCA - 12/750 ml":
        $price = 109.00;
        break;
    case "Mocha Almond Fudge GREEN MARASCA - 12/750 ml":
        $price = 109.00;
        break;
    case "Orange GREEN MARASCA - 12/750 ml":
        $price = 109.00;
        break;
    case "Orange/Mango/Passionfruit GREEN MARASCA - 12/750 ml":
        $price = 109.00;
        break;
    case "Orange Vanilla GREEN MARASCA - 12/750 ml":
        $price = 109.00;
        break;
    case "Peach GREEN MARASCA - 12/750 ml":
        $price = 109.00;
        break;
    case "Pear GREEN MARASCA - 12/750 ml":
        $price = 109.00;
        break;
    case "Pecan Praline GREEN MARASCA - 12/750 ml":
        $price = 109.00;
        break;
    case "Pineapple GREEN MARASCA - 12/750 ml":
        $price = 109.00;
        break;
    case "Pomegranate GREEN MARASCA - 12/750 ml":
        $price = 109.00;
        break;
    case "Pumpkin Spice GREEN MARASCA - 12/750 ml":
        $price = 109.00;
        break;
    case "Raspberry GREEN MARASCA - 12/750 ml":
        $price = 109.00;
        break;
    case "Raspberry Ginger GREEN MARASCA - 12/750 ml":
        $price = 109.00;
        break;
    case "Strawberry GREEN MARASCA - 12/750 ml":
        $price = 109.00;
        break;
    case "Strawberry Peach GREEN MARASCA - 12/750 ml":
        $price = 109.00;
        break;
    case "Tangerine GREEN MARASCA - 12/750 ml":
        $price = 109.00;
        break;
    case "Black Truffle GREEN MARASCA - 12/750 ml":
        $price = 109.00;
        break;
    case "Vanilla GREEN MARASCA - 12/750 ml":
        $price = 109.00;
        break;
    case "Vanilla Fig GREEN MARASCA - 12/750 ml":
        $price = 109.00;
        break;
//UVAG MARASCA - 12/500 ml
//oils
    case "Olive Oil Extra Virgin UVAG MARASCA - 12/500 ml":
        $price = 48.00;
        break;
    case "Australia - Leccino/Frantoio UVAG MARASCA - 12/500 ml":
        $price = 63.00;
        break;
    case "Australia - Frantoio UVAG MARASCA - 12/500 ml":
        $price = 63.00;
        break;
    case "California Arbequina UVAG MARASCA - 12/500 ml":
        $price = 95.00;
        break;
    case "California Arbosana UVAG MARASCA - 12/500 ml":
        $price = 90.00;
        break;
    case "California Mission UVAG MARASCA - 12/500 ml":
        $price = 90.00;
        break;
    case "California Arbequina/Arbosana/Koroneiki UVAG MARASCA - 12/500 ml":
        $price = 63.00;
        break;
    case "Chile - Arbequina UVAG MARASCA - 12/500 ml":
        $price = 49.00;
        break;
    case "Chile - Frantoio UVAG MARASCA - 12/500 ml":
        $price = 49.00;
        break;
    case "Chile - Leccino UVAG MARASCA - 12/500 ml":
        $price = 49.00;
        break;
    case "Chile - Picual UVAG MARASCA - 12/500 ml":
        $price = 49.00;
        break;
    case "Greece - Kalamata UVAG MARASCA - 12/500 ml":
        $price = 56.00;
        break;
    case "Greece - Koroneiki UVAG MARASCA - 12/500 ml":
        $price = 56.00;
        break;
    case "Italy - Umbrian region UVAG MARASCA - 12/500 ml":
        $price = 87.00;
        break;
    case "Italy - Calabria region - Carolea UVAG MARASCA - 12/500 ml":
        $price = 73.00;
        break;
    case "Italy (Puglia) - Coratina UVAG MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Italy (Puglia)- Ogliarola UVAG MARASCA - 12/500 ml":
        $price = 73.00;
        break;
    case "Spain - Hojiblanca UVAG MARASCA - 12/500 ml":
        $price = 58.00;
        break;
    case "Spain - Picual UVAG MARASCA - 12/500 ml":
        $price = 58.00;
        break;
    case "Spain - Signature blend UVAG MARASCA - 12/500 ml ":
        $price = 58.00;
        break;
    case "Tunisia - Chemlali/Chetoui UVAG MARASCA - 12/500 ml":
        $price = 48.00;
        break;
    //infused
    case "Black Pepper UVAG MARASCA - 12/500 m":
        $price = 80.00;
        break;
    case "Garlic Oil UVAG MARASCA - 12/500 m":
        $price = 67.00;
        break;
    case "Roasted Chili UVAG MARASCA - 12/500 m":
        $price = 71.00;
        break;
    case "Oregano UVAG MARASCA - 12/500 m":
        $price = 73.00;
        break;
    case "Lemon Pepper UVAG MARASCA - 12/500 m":
        $price = 86.00;
        break;
    case "Jalapeno oil UVAG MARASCA - 12/500 m":
        $price = 67.00;
        break;
    case "Basil UVAG MARASCA - 12/500 m":
        $price = 68.00;
        break;
    case "Scallion UVAG MARASCA - 12/500 m":
        $price = 74.00;
        break;
    case "Habenero UVAG MARASCA - 12/500 m":
        $price = 91.00;
        break;
    case "Rosemary UVAG MARASCA - 12/500 m":
        $price = 76.00;
        break;
    case "Chipotle UVAG MARASCA - 12/500 m":
        $price = 71.00;
        break;
//flavored olive oils
    case "Blood Orange UVAG MARASCA - 12/500 ml":
        $price = 52.00;
        break;
    case "Lemon oil UVAG MARASCA - 12/500 ml":
        $price = 60.00;
        break;
    case "Lime UVAG MARASCA - 12/500 ml":
        $price = 62.00;
        break;
    case "Orange oil UVAG MARASCA - 12/500 ml":
        $price = 52.00;
        break;
    case "Tangerine oil UVAG MARASCA - 12/500 ml":
        $price = 54.00;
        break;
    case "Butter UVAG MARASCA - 12/500 ml":
        $price = 67.00;
        break;
    case "Cranberry Walnut oil UVAG MARASCA - 12/500 ml":
        $price = 53.00;
        break;
    case "Arbequina Rhubarb UVAG MARASCA - 12/500 ml":
        $price = 52.00;
        break;
    case "Sage Onion UVAG MARASCA - 12/500 ml":
        $price = 76.00;
        break;
    case "Sundried Tomato UVAG MARASCA - 12/500 ml":
        $price = 63.00;
        break;
    case "White Truffle UVAG MARASCA - 12/500 ml":
        $price = 141.00;
        break;
    case "Black Truffle oil UVAG MARASCA - 12/500 ml":
        $price = 138.00;
        break;
    case "Hickory oil UVAG MARASCA - 12/500 ml":
        $price = 63.00;
        break;
    case "Vanilla oil UVAG MARASCA - 12/500 ml":
        $price = 63.00;
        break;
//fused
    case "Garlic Mushroom UVAG MARASCA - 12/500 ml":
        $price = 76.00;
        break;
    case "Basil/Lemongrass UVAG MARASCA - 12/500 ml":
        $price = 77.00;
        break;
    case "Rosemary with Lavender UVAG MARASCA - 12/500 ml":
        $price = 86.00;
        break;
    case "Tuscan Herb UVAG MARASCA - 12/500 ml":
        $price = 83.00;
        break;
    case "Herbs d Provence UVAG MARASCA - 12/500 ml":
        $price = 80.00;
        break;
    case "Citrus Habanero UVAG MARASCA - 12/500 ml":
        $price = 71.00;
        break;
    case "Garlic Roasted Chili UVAG MARASCA - 12/500 ml":
        $price = 75.00;
        break;
//specialty oils
    case "Almond Oil UVAG MARASCA - 12/500 ml":
        $price = 62.00;
        break;
    case "Apricot Oil UVAG MARASCA - 12/500 ml":
        $price = 65.00;
        break;
    case "Avocado Oil UVAG MARASCA - 12/500 ml":
        $price = 56.00;
        break;
    case "Flaxseed Oil UVAG MARASCA - 12/500 ml":
        $price = 45.00;
        break;
    case "Grapeseed Oil UVAG MARASCA - 12/500 ml":
        $price = 38.00;
        break;
    case "Hempseed Oil UVAG MARASCA - 12/500 ml":
        $price = 103.00;
        break;
    case "Macadamia Nut Oil UVAG MARASCA - 12/500 ml":
        $price = 73.00;
        break;
    case "Toasted Sesame Oil UVAG MARASCA - 12/500 ml":
        $price = 48.00;
        break;
    case "Walnut Oil UVAG MARASCA - 12/500 ml":
        $price = 43.00;
        break;
    //organics
    case "Olive Oil, Extra Virgin, Organic , Italy UVAG MARASCA - 12/500 ml":
        $price = 63.00;
        break;
    case "Olive Oil, Extra Virgin, Organic , Country Varies UVAG MARASCA - 12/500 ml":
        $price = 53.00;
        break;
    case "Canola UVAG MARASCA - 12/500 ml":
        $price = 35.00;
        break;
    case "Soybean Oil UVAG MARASCA - 12/500 ml":
        $price = 27.00;
        break;
    case "Sunflower UVAG MARASCA - 12/500 ml":
        $price = 49.00;
        break;
    case "Sesame Toasted UVAG MARASCA - 12/500 ml":
        $price = 46.00;
        break;
    case "Sesame, Virgin UVAG MARASCA - 12/500 ml":
        $price = 46.00;
        break;
//vinegars
    case "4 Star Balsamic UVAG MARASCA - 12/500 ml":
        $price = 30.00;
        break;
    case "8 Star Balsamic UVAG MARASCA - 12/500 ml":
        $price = 41.00;
        break;
    case "12 Star Balsamic UVAG MARASCA - 12/500 ml":
        $price = 47.00;
        break;
    case "25 Star Dark Balsamic UVAG MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "25 Star White Balsamic UVAG MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "6 Star White Balsamic UVAG MARASCA - 12/500 ml":
        $price = 40.00;
        break;
    case "Red Wine Vinegar - Domestic - 50 Grain UVAG MARASCA - 12/500 ml":
        $price = 23.00;
        break;
    case "Organic Premium Quality Balsamic - Ltd Edition UVAG MARASCA - 12/500 ml":
        $price = 79.00;
        break;
    case "Serrano Chile Vinegar UVAG MARASCA - 12/500 ml":
        $price = 82.00;
        break;
    case "Lambrusco Vinegar UVAG MARASCA - 12/500 ml":
        $price = 74.00;
        break;
//flavored balsamic
    case "Almond Creme UVAG MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Apricot UVAG MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Bittersweet Chocolate with Orange UVAG MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Blackberry UVAG MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Blackberry Ginger UVAG MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Black Currant UVAG MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Black Walnut UVAG MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Blueberry UVAG MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Bordeaux Cherry UVAG MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Chili UVAG MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Chocolate UVAG MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Chocolate Covered Cherries UVAG MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Chocolate Jalapeno UVAG MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Chocolate Marshmallow UVAG MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Coconut UVAG MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Cranberry Orange UVAG MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Cranberry Walnut UVAG MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Cucumber Melon UVAG MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Elderberry UVAG MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Espresso UVAG MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Fig UVAG MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Garlic UVAG MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Garlic Cilantro UVAG MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Grapefruit UVAG MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Green Apple UVAG MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Hickory UVAG MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Honey UVAG MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Honey Ginger UVAG MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Huckleberry UVAG MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Jalapeno UVAG MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Jalapeno Fig in 8 Star UVAG MARASCA - 12/500 ml":
        $price = 59.00;
        break;
    case "Jalapeno & Lime UVAG MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Lavender UVAG MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Lemongrass Mint UVAG MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Lemon UVAG MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Lemon Vanilla UVAG MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Mandarin Orange UVAG MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Mango UVAG MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Mocha Almond Fudge UVAG MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Orange UVAG MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Orange/Mango/Passionfruit UVAG MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Orange Vanilla UVAG MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Peach UVAG MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Pear UVAG MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Pecan Praline UVAG MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Pineapple UVAG MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Pomegranate UVAG MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Pumpkin Spice UVAG MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Raspberry UVAG MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Raspberry Ginger UVAG MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Strawberry UVAG MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Strawberry Peach UVAG MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Tangerine UVAG MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Black Truffle UVAG MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Vanilla UVAG MARASCA - 12/500 ml":
        $price = 74.00;
        break;
    case "Vanilla Fig UVAG MARASCA - 12/500 ml":
        $price = 74.00;
        break;
//SERENADE - 24/100 ml
//oils
    case "Olive Oil Extra Virgin SERENADE - 24/100 ml":
        $price = 43.00;
        break;
    case "Australia - Leccino/Frantoio SERENADE - 24/100 ml":
        $price = 49.00;
        break;
    case "Australia - Frantoio SERENADE - 24/100 ml":
        $price = 49.00;
        break;
    case "California Arbequina SERENADE - 24/100 ml":
        $price = 62.00;
        break;
    case "California Arbosana SERENADE - 24/100 ml":
        $price = 60.00;
        break;
    case "California Mission SERENADE - 24/100 ml":
        $price = 60.00;
        break;
    case "California Arbequina/Arbosana/Koroneiki SERENADE - 24/100 ml":
        $price = 49.00;
        break;
    case "Chile - Arbequina SERENADE - 24/100 ml":
        $price = 44.00;
        break;
    case "Chile - Frantoio SERENADE - 24/100 ml":
        $price = 44.00;
        break;
    case "Chile - Leccino SERENADE - 24/100 ml":
        $price = 44.00;
        break;
    case "Chile - Picual SERENADE - 24/100 ml":
        $price = 44.00;
        break;
    case "Greece - Kalamata SERENADE - 24/100 ml":
        $price = 46.00;
        break;
    case "Greece - Koroneiki SERENADE - 24/100 ml":
        $price = 46.00;
        break;
    case "Italy - Umbrian region SERENADE - 24/100 ml":
        $price = 59.00;
        break;
    case "Italy - Calabria region - Carolea SERENADE - 24/100 ml":
        $price = 53.00;
        break;
    case "Italy (Puglia) - Coratina SERENADE - 24/100 ml":
        $price = 54.00;
        break;
    case "Italy (Puglia)- Ogliarola SERENADE - 24/100 ml":
        $price = 53.00;
        break;
    case "Spain - Hojiblanca SERENADE - 24/100 ml":
        $price = 47.00;
        break;
    case "Spain - Picual SERENADE - 24/100 ml":
        $price = 47.00;
        break;
    case "Spain - Signature blend SERENADE - 24/100 ml":
        $price = 47.00;
        break;
    case "Tunisia - Chemlali/Chetoui SERENADE - 24/100 ml":
        $price = 43.00;
        break;
    //infused
    case "Black Pepper SERENADE - 24/100 ml":
        $price = 54.00;
        break;
    case "Garlic Oil SERENADE - 24/100 ml":
        $price = 49.00;
        break;
    case "Roasted Chili SERENADE - 24/100 ml":
        $price = 50.00;
        break;
    case "Oregano SERENADE - 24/100 ml":
        $price = 51.00;
        break;
    case "Lemon Pepper SERENADE - 24/100 ml":
        $price = 56.00;
        break;
    case "Jalapeno oil SERENADE - 24/100 ml":
        $price = 49.00;
        break;
    case "Basil SERENADE - 24/100 ml":
        $price = 49.00;
        break;
    case "Scallion SERENADE - 24/100 ml":
        $price = 52.00;
        break;
    case "Habenero SERENADE - 24/100 ml":
        $price = 58.00;
        break;
    case "Rosemary SERENADE - 24/100 ml":
        $price = 52.00;
        break;
    case "Chipotle SERENADE - 24/100 ml":
        $price = 50.00;
        break;
//flavored oils
    case "Blood Orange SERENADE - 24/100 ml":
        $price = 45.00;
        break;
    case "Lemon oil SERENADE - 24/100 ml":
        $price = 48.00;
        break;
    case "Lime SERENADE - 24/100 ml":
        $price = 49.00;
        break;
    case "Orange oil SERENADE - 24/100 ml":
        $price = 45.00;
        break;
    case "Tangerine oil SERENADE - 24/100 ml":
        $price = 45.00;
        break;
    case "Butter SERENADE - 24/100 ml":
        $price = 51.00;
        break;
    case "Cranberry Walnut oil SERENADE - 24/100 ml":
        $price = 45.00;
        break;
    case "Arbequina Rhubarb SERENADE - 24/100 ml":
        $price = 45.00;
        break;
    case "Sage Onion SERENADE - 24/100 ml":
        $price = 54.00;
        break;
    case "Sundried Tomato SERENADE - 24/100 ml":
        $price = 49.00;
        break;
    case "White Truffle SERENADE - 24/100 ml":
        $price = 85.00;
        break;
    case "Black Truffle oil SERENADE - 24/100 ml":
        $price = 83.00;
        break;
    case "Hickory oil SERENADE - 24/100 ml":
        $price = 49.00;
        break;
    case "Vanilla oil SERENADE - 24/100 ml":
        $price = 49.00;
        break;
//fused
    case "Garlic Mushroom SERENADE - 24/100 ml":
        $price = 54.00;
        break;
    case "Basil/Lemongrass SERENADE - 24/100 ml":
        $price = 55.00;
        break;
    case "Rosemary with Lavender SERENADE - 24/100 ml":
        $price = 58.00;
        break;
    case "Tuscan Herb SERENADE - 24/100 ml":
        $price = 57.00;
        break;
    case "Herbs d Provence SERENADE - 24/100 ml":
        $price = 56.00;
        break;
    case "Citrus Habanero SERENADE - 24/100 ml":
        $price = 52.00;
        break;
    case "Garlic Roasted Chili SERENADE - 24/100 ml":
        $price = 54.00;
        break;
//specialty oils
    case "Almond Oil SERENADE - 24/100 ml":
        $price = 47.00;
        break;
    case "Apricot Oil SERENADE - 24/100 ml":
        $price = 48.00;
        break;
    case "Avocado Oil SERENADE - 24/100 ml":
        $price = 45.00;
        break;
    case "Flaxseed Oil SERENADE - 24/100 ml":
        $price = 40.00;
        break;
    case "Grapeseed Oil SERENADE - 24/100 ml":
        $price = 38.00;
        break;
    case "Hempseed Oil SERENADE - 24/100 ml":
        $price = 64.00;
        break;
    case "Macadamia Nut Oil SERENADE - 24/100 ml":
        $price = 51.00;
        break;
    case "Toasted Sesame Oil SERENADE - 24/100 ml":
        $price = 41.00;
        break;
    case "Walnut Oil SERENADE - 24/100 ml":
        $price = 39.00;
        break;
//organics
    case "Olive Oil, Extra Virgin, Organic , Italy SERENADE - 24/100 ml":
        $price = 47.00;
        break;
    case "Olive Oil, Extra Virgin, Organic , Country Varies SERENADE - 24/100 ml":
        $price = 44.00;
        break;
    case "Canola SERENADE - 24/100 ml":
        $price = 36.00;
        break;
    case "Soybean Oil SERENADE - 24/100 ml":
        $price = 33.00;
        break;
    case "Sunflower SERENADE - 24/100 ml":
        $price = 41.00;
        break;
    case "Sesame Toasted SERENADE - 24/100 ml":
        $price = 40.00;
        break;
    case "Sesame, Virgin SERENADE - 24/100 ml":
        $price = 40.00;
        break;
//vinegars
    case "4 Star Balsamic SERENADE - 24/100 ml":
        $price = 37.00;
        break;
    case "8 Star Balsamic SERENADE - 24/100 ml":
        $price = 43.00;
        break;
    case "12 Star Balsamic SERENADE - 24/100 ml":
        $price = 47.00;
        break;
    case "25 Star Dark Balsamic SERENADE - 24/100 ml":
        $price = 61.00;
        break;
    case "25 Star White Balsamic SERENADE - 24/100 ml":
        $price = 61.00;
        break;
    case "6 Star White Balsamic SERENADE - 24/100 ml":
        $price = 42.00;
        break;
    case "Red Wine Vinegar - Domestic - 50 Grain SERENADE - 24/100 ml":
        $price = 36.00;
        break;
    case "Organic Premium Quality Balsamic - Ltd Edition SERENADE - 24/100 ml":
        $price = 55.00;
        break;
    case "Serrano Chile Vinegar SERENADE - 24/100 ml":
        $price = 61.00;
        break;
    case "Lambrusco Vinegar SERENADE - 24/100 ml":
        $price = 61.00;
        break;
//flavored balsmics
    case "Almond Creme SERENADE - 24/100 ml":
        $price = 61.00;
        break;
    case "Apricot SERENADE - 24/100 ml":
        $price = 61.00;
        break;
    case "Bittersweet Chocolate with Orange SERENADE - 24/100 ml":
        $price = 61.00;
        break;
    case "Blackberry SERENADE - 24/100 ml":
        $price = 61.00;
        break;
    case "Blackberry Ginger SERENADE - 24/100 ml":
        $price = 61.00;
        break;
    case "Black Currant SERENADE - 24/100 ml":
        $price = 61.00;
        break;
    case "Black Walnut SERENADE - 24/100 ml":
        $price = 61.00;
        break;
    case "Blueberry SERENADE - 24/100 ml":
        $price = 61.00;
        break;
    case "Bordeaux Cherry SERENADE - 24/100 ml":
        $price = 61.00;
        break;
    case "Chili SERENADE - 24/100 ml":
        $price = 61.00;
        break;
    case "Chocolate SERENADE - 24/100 ml":
        $price = 61.00;
        break;
    case "Chocolate Covered Cherries SERENADE - 24/100 ml":
        $price = 61.00;
        break;
    case "Chocolate Jalapeno SERENADE - 24/100 ml":
        $price = 61.00;
        break;
    case "Chocolate Marshmallow SERENADE - 24/100 ml":
        $price = 61.00;
        break;
    case "Coconut SERENADE - 24/100 ml":
        $price = 61.00;
        break;
    case "Cranberry Orange SERENADE - 24/100 ml":
        $price = 61.00;
        break;
    case "Cranberry Walnut SERENADE - 24/100 ml":
        $price = 61.00;
        break;
    case "Cucumber Melon SERENADE - 24/100 ml":
        $price = 61.00;
        break;
    case "Elderberry SERENADE - 24/100 ml":
        $price = 61.00;
        break;
    case "Espresso SERENADE - 24/100 ml":
        $price = 61.00;
        break;
    case "Fig SERENADE - 24/100 ml":
        $price = 61.00;
        break;
    case "Garlic SERENADE - 24/100 ml":
        $price = 61.00;
        break;
    case "Garlic Cilantro SERENADE - 24/100 ml":
        $price = 61.00;
        break;
    case "Grapefruit SERENADE - 24/100 ml":
        $price = 61.00;
        break;
    case "Green Apple SERENADE - 24/100 ml":
        $price = 61.00;
        break;
    case "Hickory SERENADE - 24/100 ml":
        $price = 61.00;
        break;
    case "Honey SERENADE - 24/100 ml":
        $price = 61.00;
        break;
    case "Honey Ginger SERENADE - 24/100 ml":
        $price = 61.00;
        break;
    case "Huckleberry SERENADE - 24/100 ml":
        $price = 61.00;
        break;
    case "Jalapeno SERENADE - 24/100 ml":
        $price = 61.00;
        break;
    case "Jalapeno Fig in 8 Star SERENADE - 24/100 ml":
        $price = 55.00;
        break;
    case "Jalapeno & Lime SERENADE - 24/100 ml":
        $price = 61.00;
        break;
    case "Lavender SERENADE - 24/100 ml":
        $price = 61.00;
        break;
    case "Lemongrass Mint SERENADE - 24/100 ml":
        $price = 61.00;
        break;
    case "Lemon SERENADE - 24/100 ml":
        $price = 61.00;
        break;
    case "Lemon Vanilla SERENADE - 24/100 ml":
        $price = 61.00;
        break;
    case "Mandarin Orange SERENADE - 24/100 ml":
        $price = 61.00;
        break;
    case "Mango SERENADE - 24/100 ml":
        $price = 61.00;
        break;
    case "Mocha Almond Fudge SERENADE - 24/100 ml":
        $price = 61.00;
        break;
    case "Orange SERENADE - 24/100 ml":
        $price = 61.00;
        break;
    case "Orange/Mango/Passionfruit SERENADE - 24/100 ml":
        $price = 61.00;
        break;
    case "Orange Vanilla SERENADE - 24/100 ml":
        $price = 61.00;
        break;
    case "Peach SERENADE - 24/100 ml":
        $price = 61.00;
        break;
    case "Pear SERENADE - 24/100 ml":
        $price = 61.00;
        break;
    case "Pecan Praline SERENADE - 24/100 ml":
        $price = 61.00;
        break;
    case "Pineapple SERENADE - 24/100 ml":
        $price = 61.00;
        break;
    case "Pomegranate SERENADE - 24/100 ml":
        $price = 61.00;
        break;
    case "Pumpkin Spice SERENADE - 24/100 ml":
        $price = 61.00;
        break;
    case "Raspberry SERENADE - 24/100 ml":
        $price = 61.00;
        break;
    case "Raspberry Ginger SERENADE - 24/100 ml":
        $price = 61.00;
        break;
    case "Strawberry SERENADE - 24/100 ml":
        $price = 61.00;
        break;
    case "Strawberry Peach SERENADE - 24/100 ml":
        $price = 61.00;
        break;
    case "Tangerine SERENADE - 24/100 ml":
        $price = 61.00;
        break;
    case "Black Truffle SERENADE - 24/100 ml":
        $price = 61.00;
        break;
    case "Vanilla SERENADE - 24/100 ml":
        $price = 61.00;
        break;
    case "Vanilla Fig SERENADE - 24/100 ml":
        $price = 61.00;
        break;
    //serenade 200ml
    //oils
    case "Olive Oil Extra Virgin SERENADE - 12/200 ml":
        $price = 32.00;
        break;
    case "Australia - Leccino/Frantoio SERENADE - 12/200 ml":
        $price = 38.00;
        break;
    case "Australia - Frantoio SERENADE - 12/200 ml":
        $price = 38.00;
        break;
    case "California Arbequina SERENADE - 12/200 ml":
        $price = 51.00;
        break;
    case "California Arbosana SERENADE - 12/200 ml":
        $price = 49.00;
        break;
    case "California Mission SERENADE - 12/200 ml":
        $price = 49.00;
        break;
    case "California Arbequina/Arbosana/Koroneiki SERENADE - 12/200 ml":
        $price = 38.00;
        break;
    case "Chile - Arbequina SERENADE - 12/200 ml":
        $price = 33.00;
        break;
    case "Chile - Frantoio SERENADE - 12/200 ml":
        $price = 33.00;
        break;
    case "Chile - Leccino SERENADE - 12/200 ml":
        $price = 33.00;
        break;
    case "Chile - Picual SERENADE - 12/200 ml":
        $price = 33.00;
        break;
    case "Greece - Kalamata SERENADE - 12/200 ml":
        $price = 36.00;
        break;
    case "Greece - Koroneiki SERENADE - 12/200 ml":
        $price = 36.00;
        break;
    case "Italy - Umbrian region SERENADE - 12/200 ml":
        $price = 48.00;
        break;
    case "Italy - Calabria region - Carolea SERENADE - 12/200 ml":
        $price = 43.00;
        break;
    case "Italy (Puglia) - Coratina SERENADE - 12/200 ml":
        $price = 43.00;
        break;
    case "Italy (Puglia)- Ogliarola SERENADE - 12/200 ml":
        $price = 43.00;
        break;
    case "Spain - Hojiblanca SERENADE - 12/200 ml":
        $price = 37.00;
        break;
    case "Spain - Picual SERENADE - 12/200 ml":
        $price = 37.00;
        break;
    case "Spain - Signature blend SERENADE - 12/200 ml":
        $price = 37.00;
        break;
    case "Tunisia - Chemlali/Chetoui SERENADE - 12/200 ml":
        $price = 32.00;
        break;
//infused
    case "Black Pepper SERENADE - 12/200 ml":
        $price = 44.00;
        break;
    case "Garlic Oil SERENADE - 12/200 ml":
        $price = 39.00;
        break;
    case "Roasted Chili SERENADE - 12/200 ml":
        $price = 40.00;
        break;
    case "Oregano SERENADE - 12/200 ml":
        $price = 41.00;
        break;
    case "Lemon Pepper SERENADE - 12/200 ml":
        $price = 46.00;
        break;
    case "Jalapeno oil SERENADE - 12/200 ml":
        $price = 39.00;
        break;
    case "Basil SERENADE - 12/200 ml":
        $price = 39.00;
        break;
    case "Scallion SERENADE - 12/200 ml":
        $price = 42.00;
        break;
    case "Habenero SERENADE - 12/200 ml":
        $price = 48.00;
        break;
    case "Rosemary SERENADE - 12/200 ml":
        $price = 42.00;
        break;
    case "Chipotle SERENADE - 12/200 ml":
        $price = 40.00;
        break;
//flavored oils
    case "Blood Orange SERENADE - 12/200 ml":
        $price = 34.00;
        break;
    case "Lemon oil SERENADE - 12/200 ml":
        $price = 37.00;
        break;
    case "Lime SERENADE - 12/200 ml":
        $price = 38.00;
        break;
    case "Orange oil SERENADE - 12/200 ml":
        $price = 34.00;
        break;
    case "Tangerine oil SERENADE - 12/200 ml":
        $price = 35.00;
        break;
    case "Butter SERENADE - 12/200 ml":
        $price = 40.00;
        break;
    case "Cranberry Walnut oil SERENADE - 12/200 ml":
        $price = 34.00;
        break;
    case "Arbequina Rhubarb SERENADE - 12/200 ml":
        $price = 34.00;
        break;
    case "Sage Onion SERENADE - 12/200 ml":
        $price = 44.00;
        break;
    case "Sundried Tomato SERENADE - 12/200 ml":
        $price = 38.00;
        break;
    case "White Truffle SERENADE - 12/200 ml":
        $price = 72.00;
        break;
    case "Black Truffle oil SERENADE - 12/200 ml":
        $price = 71.00;
        break;
    case "Hickory oil SERENADE - 12/200 ml":
        $price = 38.00;
        break;
    case "Vanilla oil SERENADE - 12/200 ml":
        $price = 38.00;
        break;
//fused
    case "Garlic Mushroom SERENADE - 12/200 ml":
        $price = 43.00;
        break;
    case "Basil/Lemongrass SERENADE - 12/200 ml":
        $price = 44.00;
        break;
    case "Rosemary with Lavender SERENADE - 12/200 ml":
        $price = 47.00;
        break;
    case "Tuscan Herb SERENADE - 12/200 ml":
        $price = 46.00;
        break;
    case "Herbs d Provence SERENADE - 12/200 ml":
        $price = 45.00;
        break;
    case "Citrus Habanero SERENADE - 12/200 ml":
        $price = 42.00;
        break;
    case "Garlic Roasted Chili SERENADE - 12/200 ml":
        $price = 43.00;
        break;
//specialty oils
    case "Almond Oil SERENADE - 12/200 ml":
        $price = 37.00;
        break;
    case "Apricot Oil SERENADE - 12/200 ml":
        $price = 38.00;
        break;
    case "Avocado Oil SERENADE - 12/200 ml":
        $price = 35.00;
        break;
    case "Flaxseed Oil SERENADE - 12/200 ml":
        $price = 30.00;
        break;
    case "Grapeseed Oil SERENADE - 12/200 ml":
        $price = 28.00;
        break;
    case "Hempseed Oil SERENADE - 12/200 ml":
        $price = 54.00;
        break;
    case "Macadamia Nut Oil SERENADE - 12/200 ml":
        $price = 41.00;
        break;
    case "Toasted Sesame Oil SERENADE - 12/200 ml":
        $price = 31.00;
        break;
    case "Walnut Oil SERENADE - 12/200 ml":
        $price = 29.00;
        break;
//organics
    case "Olive Oil, Extra Virgin, Organic , Italy SERENADE - 12/200 ml":
        $price = 37.00;
        break;
    case "Olive Oil, Extra Virgin, Organic , Country Varies SERENADE - 12/200 ml":
        $price = 34.00;
        break;
    case "Canola SERENADE - 12/200 ml":
        $price = 26.00;
        break;
    case "Soybean Oil SERENADE - 12/200 ml":
        $price = 23.00;
        break;
    case "Sunflower SERENADE - 12/200 ml":
        $price = 32.00;
        break;
    case "Sesame Toasted SERENADE - 12/200 ml":
        $price = 30.00;
        break;
    case "Sesame, Virgin SERENADE - 12/200 ml":
        $price = 30.00;
        break;
//vinegars
    case "4 Star Balsamic SERENADE - 12/200 ml":
        $price = 25.00;
        break;
    case "8 Star Balsamic SERENADE - 12/200 ml":
        $price = 31.00;
        break;
    case "12 Star Balsamic SERENADE - 12/200 ml":
        $price = 34.00;
        break;
    case "25 Star Dark Balsamic SERENADE - 12/200 ml":
        $price = 47.00;
        break;
    case "25 Star White Balsamic SERENADE - 12/200 ml":
        $price = 47.00;
        break;
    case "6 Star White Balsamic SERENADE - 12/200 ml":
        $price = 31.00;
        break;
    case "Red Wine Vinegar - Domestic - 50 Grain SERENADE - 12/200 ml":
        $price = 24.00;
        break;
    case "Organic Premium Quality Balsamic - Ltd Edition SERENADE - 12/200 ml":
        $price = 44.00;
        break;
    case "Serrano Chile Vinegar SERENADE - 12/200 ml":
        $price = 49.00;
        break;
    case "Lambrusco Vinegar SERENADE - 12/200 ml":
        $price = 47.00;
        break;
//flavored balsamic
    case "Almond Creme SERENADE - 12/200 ml":
        $price = 47.00;
        break;
    case "Apricot SERENADE - 12/200 ml":
        $price = 47.00;
        break;
    case "Bittersweet Chocolate with Orange SERENADE - 12/200 ml":
        $price = 47.00;
        break;
    case "Blackberry SERENADE - 12/200 ml":
        $price = 47.00;
        break;
    case "Blackberry Ginger SERENADE - 12/200 ml":
        $price = 47.00;
        break;
    case "Black Currant SERENADE - 12/200 ml":
        $price = 47.00;
        break;
    case "Black Walnut SERENADE - 12/200 ml":
        $price = 47.00;
        break;
    case "Blueberry SERENADE - 12/200 ml":
        $price = 47.00;
        break;
    case "Bordeaux Cherry SERENADE - 12/200 ml":
        $price = 47.00;
        break;
    case "Chili SERENADE - 12/200 ml":
        $price = 47.00;
        break;
    case "Chocolate SERENADE - 12/200 ml":
        $price = 47.00;
        break;
    case "Chocolate Covered Cherries SERENADE - 12/200 ml":
        $price = 47.00;
        break;
    case "Chocolate Jalapeno SERENADE - 12/200 ml":
        $price = 47.00;
        break;
    case "Chocolate Marshmallow SERENADE - 12/200 ml":
        $price = 47.00;
        break;
    case "Coconut SERENADE - 12/200 ml":
        $price = 47.00;
        break;
    case "Cranberry Orange SERENADE - 12/200 ml":
        $price = 47.00;
        break;
    case "Cranberry Walnut SERENADE - 12/200 ml":
        $price = 47.00;
        break;
    case "Cucumber Melon SERENADE - 12/200 ml":
        $price = 47.00;
        break;
    case "Elderberry SERENADE - 12/200 ml":
        $price = 47.00;
        break;
    case "Espresso SERENADE - 12/200 ml":
        $price = 47.00;
        break;
    case "Fig SERENADE - 12/200 ml":
        $price = 47.00;
        break;
    case "Garlic SERENADE - 12/200 ml":
        $price = 47.00;
        break;
    case "Garlic Cilantro SERENADE - 12/200 ml":
        $price = 47.00;
        break;
    case "Grapefruit SERENADE - 12/200 ml":
        $price = 47.00;
        break;
    case "Green Apple SERENADE - 12/200 ml":
        $price = 47.00;
        break;
    case "Hickory SERENADE - 12/200 ml":
        $price = 47.00;
        break;
    case "Honey SERENADE - 12/200 ml":
        $price = 47.00;
        break;
    case "Honey Ginger SERENADE - 12/200 ml":
        $price = 47.00;
        break;
    case "Huckleberry SERENADE - 12/200 ml":
        $price = 47.00;
        break;
    case "Jalapeno SERENADE - 12/200 ml":
        $price = 47.00;
        break;
    case "Jalapeno Fig in 8 Star SERENADE - 12/200 ml":
        $price = 41.00;
        break;
    case "Jalapeno & Lime SERENADE - 12/200 ml":
        $price = 47.00;
        break;
    case "Lavender SERENADE - 12/200 ml":
        $price = 47.00;
        break;
    case "Lemongrass Mint SERENADE - 12/200 ml":
        $price = 47.00;
        break;
    case "Lemon SERENADE - 12/200 ml":
        $price = 47.00;
        break;
    case "Lemon Vanilla SERENADE - 12/200 ml":
        $price = 47.00;
        break;
    case "Mandarin Orange SERENADE - 12/200 ml":
        $price = 47.00;
        break;
    case "Mango SERENADE - 12/200 ml":
        $price = 47.00;
        break;
    case "Mocha Almond Fudge SERENADE - 12/200 ml":
        $price = 47.00;
        break;
    case "Orange SERENADE - 12/200 ml":
        $price = 47.00;
        break;
    case "Orange/Mango/Passionfruit SERENADE - 12/200 ml":
        $price = 47.00;
        break;
    case "Orange Vanilla SERENADE - 12/200 ml":
        $price = 47.00;
        break;
    case "Peach SERENADE - 12/200 ml":
        $price = 47.00;
        break;
    case "Pear SERENADE - 12/200 ml":
        $price = 47.00;
        break;
    case "Pecan Praline SERENADE - 12/200 ml":
        $price = 47.00;
        break;
    case "Pineapple SERENADE - 12/200 ml":
        $price = 47.00;
        break;
    case "Pomegranate SERENADE - 12/200 ml":
        $price = 47.00;
        break;
    case "Pumpkin Spice SERENADE - 12/200 ml":
        $price = 47.00;
        break;
    case "Raspberry SERENADE - 12/200 ml":
        $price = 47.00;
        break;
    case "Raspberry Ginger SERENADE - 12/200 ml":
        $price = 47.00;
        break;
    case "Strawberry SERENADE - 12/200 ml":
        $price = 47.00;
        break;
    case "Strawberry Peach SERENADE - 12/200 ml":
        $price = 47.00;
        break;
    case "Tangerine SERENADE - 12/200 ml":
        $price = 47.00;
        break;
    case "Black Truffle SERENADE - 12/200 ml":
        $price = 47.00;
        break;
    case "Vanilla SERENADE - 12/200 ml":
        $price = 47.00;
        break;
    case "Vanilla Fig SERENADE - 12/200 ml":
        $price = 47.00;
        break;
//SERENADE - 12/375 ml
//oil
    case "Olive Oil Extra Virgin SERENADE - 12/375 ml":
        $price = 50.00;
        break;
    case "Australia - Leccino/Frantoio SERENADE - 12/375 ml":
        $price = 61.00;
        break;
    case "Australia - Frantoio SERENADE - 12/375 ml":
        $price = 61.00;
        break;
    case "California Arbequina SERENADE - 12/375 ml":
        $price = 86.00;
        break;
    case "California Arbosana SERENADE - 12/375 ml":
        $price = 82.00;
        break;
    case "California Mission SERENADE - 12/375 ml":
        $price = 82.00;
        break;
    case "California Arbequina/Arbosana/Koroneiki SERENADE - 12/375 ml":
        $price = 61.00;
        break;
    case "Chile - Arbequina SERENADE - 12/375 ml":
        $price = 51.00;
        break;
    case "Chile - Frantoio SERENADE - 12/375 ml":
        $price = 51.00;
        break;
    case "Chile - Leccino SERENADE - 12/375 ml":
        $price = 51.00;
        break;
    case "Chile - Picual SERENADE - 12/375 ml":
        $price = 51.00;
        break;
    case "Greece - Kalamata SERENADE - 12/375 ml":
        $price = 56.00;
        break;
    case "Greece - Koroneiki SERENADE - 12/375 ml":
        $price = 56.00;
        break;
    case "Italy - Umbrian region SERENADE - 12/375 ml":
        $price = 80.00;
        break;
    case "Italy - Calabria region - Carolea SERENADE - 12/375 ml":
        $price = 69.00;
        break;
    case "Italy (Puglia) - Coratina SERENADE - 12/375 ml":
        $price = 70.00;
        break;
    case "Italy (Puglia)- Ogliarola SERENADE - 12/375 ml":
        $price = 69.00;
        break;
    case "Spain - Hojiblanca SERENADE - 12/375 ml":
        $price = 58.00;
        break;
    case "Spain - Picual SERENADE - 12/375 ml":
        $price = 58.00;
        break;
    case "Spain - Signature blend SERENADE - 12/375 ml":
        $price = 58.00;
        break;
    case "Tunisia - Chemlali/Chetoui SERENADE - 12/375 ml":
        $price = 50.00;
        break;
//infused
    case "Black Pepper SERENADE - 12/375 ml":
        $price = 73.00;
        break;
    case "Garlic Oil SERENADE - 12/375 ml":
        $price = 64.00;
        break;
    case "Roasted Chili SERENADE - 12/375 ml":
        $price = 66.00;
        break;
    case "Oregano SERENADE - 12/375 ml":
        $price = 68.00;
        break;
    case "Lemon Pepper SERENADE - 12/375 ml":
        $price = 78.00;
        break;
    case "Jalapeno oil SERENADE - 12/375 ml":
        $price = 64.00;
        break;
    case "Basil SERENADE - 12/375 ml":
        $price = 64.00;
        break;
    case "Scallion SERENADE - 12/375 ml":
        $price = 69.00;
        break;
    case "Habenero SERENADE - 12/375 ml":
        $price = 81.00;
        break;
    case "Rosemary SERENADE - 12/375 ml":
        $price = 70.00;
        break;
    case "Chipotle SERENADE - 12/375 ml":
        $price = 66.00;
        break;
//flavored oils
    case "Blood Orange SERENADE - 12/375 ml":
        $price = 53.00;
        break;
    case "Lemon oil SERENADE - 12/375 ml":
        $price = 59.00;
        break;
    case "Lime SERENADE - 12/375 ml":
        $price = 61.00;
        break;
    case "Orange oil SERENADE - 12/375 ml":
        $price = 53.00;
        break;
    case "Tangerine oil SERENADE - 12/375 ml":
        $price = 55.00;
        break;
    case "Butter SERENADE - 12/375 ml":
        $price = 64.00;
        break;
    case "Cranberry Walnut oil SERENADE - 12/375 ml":
        $price = 54.00;
        break;
    case "Arbequina Rhubarb SERENADE - 12/375 ml":
        $price = 54.00;
        break;
    case "Sage Onion SERENADE - 12/375 ml":
        $price = 71.00;
        break;
    case "Sundried Tomato SERENADE - 12/375 ml":
        $price = 62.00;
        break;
    case "White Truffle SERENADE - 12/375 ml":
        $price = 122.00;
        break;
    case "Black Truffle oil SERENADE - 12/375 ml":
        $price = 120.00;
        break;
    case "Hickory oil SERENADE - 12/375 ml":
        $price = 62.00;
        break;
    case "Vanilla oil SERENADE - 12/375 ml":
        $price = 62.00;
        break;
//fused
    case "Garlic Mushroom SERENADE - 12/375 ml":
        $price = 71.00;
        break;
    case "Basil/Lemongrass SERENADE - 12/375 ml":
        $price = 72.00;
        break;
    case "Rosemary with Lavender SERENADE - 12/375 ml":
        $price = 78.00;
        break;
    case "Tuscan Herb SERENADE - 12/375 ml":
        $price = 76.00;
        break;
    case "Herbs d Provence SERENADE - 12/375 ml":
        $price = 74.00;
        break;
    case "Citrus Habanero SERENADE - 12/375 ml":
        $price = 67.00;
        break;
    case "Garlic Roasted Chili SERENADE - 12/375 ml":
        $price = 71.00;
        break;
//specialty oils
    case "Almond Oil SERENADE - 12/375 ml":
        $price = 60.00;
        break;
    case "Apricot Oil SERENADE - 12/375 ml":
        $price = 62.00;
        break;
    case "Avocado Oil SERENADE - 12/375 ml":
        $price = 55.00;
        break;
    case "Flaxseed Oil SERENADE - 12/375 ml":
        $price = 47.00;
        break;
    case "Grapeseed Oil SERENADE - 12/375 ml":
        $price = 42.00;
        break;
    case "Hempseed Oil SERENADE - 12/375 ml":
        $price = 91.00;
        break;
    case "Macadamia Nut Oil SERENADE - 12/375 ml":
        $price = 68.00;
        break;
    case "Toasted Sesame Oil SERENADE - 12/375 ml":
        $price = 49.00;
        break;
    case "Walnut Oil SERENADE - 12/375 ml":
        $price = 45.00;
        break;
//organics
    case "Olive Oil, Extra Virgin, Organic , Italy SERENADE - 12/375 ml":
        $price = 60.00;
        break;
    case "Olive Oil, Extra Virgin, Organic , Country Varies SERENADE - 12/375 ml":
        $price = 53.00;
        break;
    case "Canola SERENADE - 12/375 ml":
        $price = 39.00;
        break;
    case "Soybean Oil SERENADE - 12/375 ml":
        $price = 34.00;
        break;
    case "Sunflower SERENADE - 12/375 ml":
        $price = 50.00;
        break;
    case "Sesame Toasted SERENADE - 12/375 ml":
        $price = 47.00;
        break;
    case "Sesame, Virgin SERENADE - 12/375 ml":
        $price = 47.00;
        break;
//vinegars
    case "4 Star Balsamic SERENADE - 12/375 ml":
        $price = 37.00;
        break;
    case "8 Star Balsamic SERENADE - 12/375 ml":
        $price = 47.00;
        break;
    case "12 Star Balsamic SERENADE - 12/375 ml":
        $price = 51.00;
        break;
    case "25 Star Dark Balsamic SERENADE - 12/375 ml":
        $price = 74.00;
        break;
    case "25 Star White Balsamic SERENADE - 12/375 ml":
        $price = 74.00;
        break;
    case "6 Star White Balsamic SERENADE - 12/375 ml":
        $price = 46.00;
        break;
    case "Red Wine Vinegar - Domestic - 50 Grain SERENADE - 12/375 ml":
        $price = 33.00;
        break;
    case "Organic Premium Quality Balsamic - Ltd Edition SERENADE - 12/375 ml":
        $price = 73.00;
        break;
    case "Serrano Chile Vinegar SERENADE - 12/375 ml":
        $price = 78.00;
        break;
    case "Lambrusco Vinegar SERENADE - 12/375 ml":
        $price = 74.00;
        break;
//flavored balsamics
    case "Almond Creme SERENADE - 12/375 ml":
        $price = 74.00;
        break;
    case "Apricot SERENADE - 12/375 ml":
        $price = 74.00;
        break;
    case "Bittersweet Chocolate with Orange SERENADE - 12/375 ml":
        $price = 74.00;
        break;
    case "Blackberry SERENADE - 12/375 ml":
        $price = 74.00;
        break;
    case "Blackberry Ginger SERENADE - 12/375 ml":
        $price = 74.00;
        break;
    case "Black Currant SERENADE - 12/375 ml":
        $price = 74.00;
        break;
    case "Black Walnut SERENADE - 12/375 ml":
        $price = 74.00;
        break;
    case "Blueberry SERENADE - 12/375 ml":
        $price = 74.00;
        break;
    case "Bordeaux Cherry SERENADE - 12/375 ml":
        $price = 74.00;
        break;
    case "Chili SERENADE - 12/375 ml":
        $price = 74.00;
        break;
    case "Chocolate SERENADE - 12/375 ml":
        $price = 74.00;
        break;
    case "Chocolate Covered Cherries SERENADE - 12/375 ml":
        $price = 74.00;
        break;
    case "Chocolate Jalapeno SERENADE - 12/375 ml":
        $price = 74.00;
        break;
    case "Chocolate Marshmallow SERENADE - 12/375 ml":
        $price = 74.00;
        break;
    case "Coconut SERENADE - 12/375 ml":
        $price = 74.00;
        break;
    case "Cranberry Orange SERENADE - 12/375 ml":
        $price = 74.00;
        break;
    case "Cranberry Walnut SERENADE - 12/375 ml":
        $price = 74.00;
        break;
    case "Cucumber Melon SERENADE - 12/375 ml":
        $price = 74.00;
        break;
    case "Elderberry SERENADE - 12/375 ml":
        $price = 74.00;
        break;
    case "Espresso SERENADE - 12/375 ml":
        $price = 74.00;
        break;
    case "Fig SERENADE - 12/375 ml":
        $price = 74.00;
        break;
    case "Garlic SERENADE - 12/375 ml":
        $price = 74.00;
        break;
    case "Garlic Cilantro SERENADE - 12/375 ml":
        $price = 74.00;
        break;
    case "Grapefruit SERENADE - 12/375 ml":
        $price = 74.00;
        break;
    case "Green Apple SERENADE - 12/375 ml":
        $price = 74.00;
        break;
    case "Hickory SERENADE - 12/375 ml":
        $price = 74.00;
        break;
    case "Honey SERENADE - 12/375 ml":
        $price = 74.00;
        break;
    case "Honey Ginger SERENADE - 12/375 ml":
        $price = 74.00;
        break;
    case "Huckleberry SERENADE - 12/375 ml":
        $price = 74.00;
        break;
    case "Jalapeno SERENADE - 12/375 ml":
        $price = 74.00;
        break;
    case "Jalapeno Fig in 8 Star SERENADE - 12/375 ml":
        $price = 63.00;
        break;
    case "Jalapeno & Lime SERENADE - 12/375 ml":
        $price = 74.00;
        break;
    case "Lavender SERENADE - 12/375 ml":
        $price = 74.00;
        break;
    case "Lemongrass Mint SERENADE - 12/375 ml":
        $price = 74.00;
        break;
    case "Lemon SERENADE - 12/375 ml":
        $price = 74.00;
        break;
    case "Lemon Vanilla SERENADE - 12/375 ml":
        $price = 74.00;
        break;
    case "Mandarin Orange SERENADE - 12/375 ml":
        $price = 74.00;
        break;
    case "Mango SERENADE - 12/375 ml":
        $price = 74.00;
        break;
    case "Mocha Almond Fudge SERENADE - 12/375 ml":
        $price = 74.00;
        break;
    case "Orange SERENADE - 12/375 ml":
        $price = 74.00;
        break;
    case "Orange/Mango/Passionfruit SERENADE - 12/375 ml":
        $price = 74.00;
        break;
    case "Orange Vanilla SERENADE - 12/375 ml":
        $price = 74.00;
        break;
    case "Peach SERENADE - 12/375 ml":
        $price = 74.00;
        break;
    case "Pear SERENADE - 12/375 ml":
        $price = 74.00;
        break;
    case "Pecan Praline SERENADE - 12/375 ml":
        $price = 74.00;
        break;
    case "Pineapple SERENADE - 12/375 ml":
        $price = 74.00;
        break;
    case "Pomegranate SERENADE - 12/375 ml":
        $price = 74.00;
        break;
    case "Pumpkin Spice SERENADE - 12/375 ml":
        $price = 74.00;
        break;
    case "Raspberry SERENADE - 12/375 ml":
        $price = 74.00;
        break;
    case "Raspberry Ginger SERENADE - 12/375 ml":
        $price = 74.00;
        break;
    case "Strawberry SERENADE - 12/375 ml":
        $price = 74.00;
        break;
    case "Strawberry Peach SERENADE - 12/375 ml":
        $price = 74.00;
        break;
    case "Tangerine SERENADE - 12/375 ml":
        $price = 74.00;
        break;
    case "Black Truffle SERENADE - 12/375 ml":
        $price = 74.00;
        break;
    case "Vanilla SERENADE - 12/375 ml":
        $price = 74.00;
        break;
    case "Vanilla Fig SERENADE - 12/375 ml":
        $price = 74.00;
        break;

    default: echo "Product Name: " . $product_name . " Product Bottle " . $bottle;
}
