<?php include('functions.php'); ?>

<!--show bottles & caps-->
<script type="text/javascript" >

//caps
//@return input & value

<?php include('caps.php'); ?>

    function check_value(fieldvalue)
    {
        switch(fieldvalue)
        {
            case "bordolese":
                document.getElementById("bottle").innerHTML = "<span class='borderlese'></span>"
                document.getElementById("cap").innerHTML = bordolese;
                break;
            case "bordeaux_375":
                document.getElementById("bottle").innerHTML = "<span class='bordeaux_375'></span>";
                document.getElementById("cap").innerHTML = bordeaux;
                break;
            case "bordeaux_500":
                document.getElementById("bottle").innerHTML = "<span class='bordeaux_500'></span>";
                document.getElementById("cap").innerHTML = bordeaux;
                break;
            case "bordeaux_750":
                document.getElementById("bottle").innerHTML = "<span class='bordeaux_750'></span>";
                document.getElementById("cap").innerHTML = bordeaux;
                break;
            case "dorica_antique_100":
                document.getElementById("bottle").innerHTML = "<span class='dorica_antique_100'></span>";
                document.getElementById("cap").innerHTML = dorica_100;
                break;
            case "dorica_antique_250":
                document.getElementById("bottle").innerHTML = "<span class='dorica_antique_250'></span>";
                document.getElementById("cap").innerHTML = antique_dorica;
                break;
            case "dorica_antique_500":
                document.getElementById("bottle").innerHTML = "<span class='dorica_antique_500'></span>";
                document.getElementById("cap").innerHTML = antique_dorica;
                break;
            case "dorica_clear_250":
                document.getElementById("bottle").innerHTML = "<span class='dorica_clear_250'></span>";
                document.getElementById("cap").innerHTML = antique_dorica;
                break;
            case "dorica_clear_500":
                document.getElementById("bottle").innerHTML = "<span class='dorica_clear_500'></span>";
                document.getElementById("cap").innerHTML = antique_dorica;
                break;
//            clear marasca
            case "marasca_clear_100":
                document.getElementById("bottle").innerHTML = "<span class='marasca_clear_100'></span>";
                document.getElementById("cap").innerHTML = marasca_100;
                break;
            case "marasca_clear_250":
                document.getElementById("bottle").innerHTML = "<span class='marasca_clear_250'></span>";
                document.getElementById("cap").innerHTML = antique_dorica;
                break;
            case "marasca_clear_500":
                document.getElementById("bottle").innerHTML = "<span class='marasca_clear_500'></span>";
                document.getElementById("cap").innerHTML = antique_dorica;
                break;
            case "marasca_clear_750":
                document.getElementById("bottle").innerHTML = "<span class='marasca_clear_750'></span>";
                break;
            case "marasca_clear_liter":
                document.getElementById("bottle").innerHTML = "<span class='marasca_clear_liter'></span>";
                break;
//            green marasca
            case "marasca_green_100":
                document.getElementById("bottle").innerHTML = "<span class='marasca_green_100'></span>";
                document.getElementById("cap").innerHTML = marasca_green;
                break;
            case "marasca_green_250":
                document.getElementById("bottle").innerHTML = "<span class='marasca_green_250'></span>";
                document.getElementById("cap").innerHTML = marasca_green;
                break;
            case "marasca_green_500":
                document.getElementById("bottle").innerHTML = "<span class='marasca_green_500'></span>";
                document.getElementById("cap").innerHTML = antique_dorica;
                break;
            case "marasca_green_750":
                document.getElementById("bottle").innerHTML = "<span class='marasca_green_750'></span>";
                document.getElementById("cap").innerHTML = antique_dorica;
                break;
            case "marasca_green_liter":
                document.getElementById("bottle").innerHTML = "<span class='marasca_green_liter'></span>";
                break;
//            uvag marasca
            case "marasca_uvag_100":
                document.getElementById("bottle").innerHTML = "<span class='marasca_uvag_100'></span>";
                document.getElementById("cap").innerHTML = marasca_uvag;
                break;
            case "marasca_uvag_250":
                document.getElementById("bottle").innerHTML = "<span class='marasca_uvag_250'></span>";
                document.getElementById("cap").innerHTML = marasca_uvag;
                break;
            case "marasca_uvag_500":
                document.getElementById("bottle").innerHTML = "<span class='marasca_uvag_500'></span>";
                document.getElementById("cap").innerHTML = antique_dorica;
                break;
            case "marasca_uvag_750":
                document.getElementById("bottle").innerHTML = "<span class='marasca_uvag_750'></span>";
                document.getElementById("cap").innerHTML = marasca_uvag;
                break;
            case "marasca_uvag_liter":
                document.getElementById("bottle").innerHTML = "<span class='marasca_uvag_liter'></span>";
                break;
//            serenade
            case "serenade_100":
                document.getElementById("bottle").innerHTML = "<span class='serenade_100'></span>";
                document.getElementById("cap").innerHTML = serenade_100;
                break;
            case "serenade_200":
                document.getElementById("bottle").innerHTML = "<span class='serenade_200'></span>";
                document.getElementById("cap").innerHTML = serenade_200_375;
                break;
            case "serenade_375":
                document.getElementById("bottle").innerHTML = "<span class='serenade_375'></span>";
                document.getElementById("cap").innerHTML = serenade_200_375;
                break;
//            stephanie
            case "stephanie_100":
                document.getElementById("bottle").innerHTML = "<span class='stephanie_100'></span>";
                document.getElementById("cap").innerHTML = stephanie_100;
                break;
            case "stephanie_200":
                document.getElementById("bottle").innerHTML = "<span class='stephanie_200'></span>";
                document.getElementById("cap").innerHTML = stephanie_200;
                break;
        }

        //capsules
        <?php include('capsules.php'); ?>

//end
    }

//validation

$().ready(function() {
    // validate signup form on keyup and submit
    $("#orderForm").validate({
        rules: {
            billing_name: "required",
            billing_street_address: "required",
            billing_city: "required",
            billing_state: "required",
            billing_zip: "required",
            billing_telephone: "required",
            billing_email: "required",
            shipping_name: "required",
            shipping_street_address: "required",
            shipping_city: "required",
            shipping_state: "required",
            shipping_zip: "required",
            shipping_telephone: "required",
            shipping_email: "required",

            terms: "required"
        },
        messages: {
            billing_name: "Please Enter Your Billing Name",
            billing_street_address: "Please Enter Your Street Address",
            billing_city: "Please Enter Your City",
            billing_state: "Please Enter Your State",
            billing_zip: "Please Enter Your Zipcode",
            billing_telephone: "Please Enter Your Telephone Number",
            billing_email: "Please Enter Your Email Address",
            shipping_name: "Please Enter The Name",
            shipping_street_address: "Please Add Your Street Address",
            shipping_city: "Please Enter Your City",
            shipping_state: "Please Enter Your State",
            shipping_zip: "Please Enter Your Zipcode",
            shipping_telephone: "Please Enter Your Telephone",
            shipping_email: "Please Enter Your Email Address",
            terms: "Please accept our policy"
        }
    });
});



</script>