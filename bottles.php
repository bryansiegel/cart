<?php
// setup the autoloading
require_once 'vendor/autoload.php';

// setup Propel
require_once 'generated-conf/config.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php include('includes/header.php'); ?>
</head>

<body>

<div class="container">

    <?php include('includes/nav.php'); ?>


    <table class="table-bordered">
        <tr>
            <td colspan="5">
                <h1 style="text-align: center;">Marasca Bottles</h1>
            </td>
        </tr>
        <tr>
            <td>

                <img src="assets/img/bottles/100mmaricasal.jpg" alt=""/>
             <br />
                <p style="text-align: center;">100ml Marasca Clear</p>
            </td>
            <td>
                <img src="assets/img/bottles/250-clear-marasca.jpg" alt=""/>
                <br />
                <p style="text-align: center;">250ml Marasca Clear</p>
                
            </td>
            <td>
                <img src="assets/img/bottles/500ml-marasca-clear.jpg" alt=""/>
                <p style="text-align: center;">500ml Marasca Clear</p>
            </td>
            <td>
                <img src="assets/img/bottles/500ml.jpg" alt=""/>
                <p style="text-align: center">500ml Marasca UVAG</p>
            </td>

            <td>
                <img src="assets/img/bottles/750_half-green-marasca.jpg" alt=""/>
                <p style="text-align: center;">750ml Marasca Half Green</p>
            </td>
        </tr>
        <tr>
            <td colspan="5">
                <h1 style="text-align: center">Bordelese Bottles</h1>
            </td>
        </tr>
        <tr>
            <td>
                <img src="assets/img/bottles/bottle_bordelese_375ml_1.jpg" alt=""/>
                <p style="text-align: center">375ml Bordelese</p>
            </td>
        </tr>
        <tr>
            <td colspan="5">
                <h1 style="text-align: center;">Bordeaux Bottles</h1>
            </td>
        </tr>
        <tr>
            <td>
                <img src="assets/img/bottles/bordeaux_family.jpg" alt=""/>
                <p style="text-align: center;">Bordeaux</p>
            </td>
        </tr>
        <tr>
            <td colspan="5">
                <h1 style="text-align: center;">Dorica Green Bottles</h1>
            </td>
        </tr>
        <tr>
            <td>
                <img src="assets/img/bottles/dorica-green_1.jpg" alt=""/>
                <p style="text-align: center;">Dorica Green Bottles</p>
            </td>
        </tr>
        <tr>
            <td colspan="5">
                <h1 style="text-align: center;">Dorica Clear Bottles</h1>
            </td>
        </tr>
        <tr>
            <td>
                <img src="assets/img/bottles/dorica_clear.jpg" alt=""/>
                <p style="text-align: center;">Dorica Clear Bottles</p>
            </td>
        </tr>
        <tr>
            <td colspan="5">
                <h1 style="text-align: center;">Serenade Bottles</h1>
            </td>
        </tr>
        <tr>
            <td>
                <img src="assets/img/bottles/100_serenade.jpg" alt=""/>
                <p style="text-align: center;">100ml Serenade</p>

            </td>
            <td>
                <img src="assets/img/bottles/200ml-serenade.jpg" alt=""/>
                <p style="text-align: center;">200ml Serenade</p>
            </td>
            <td>
                <img src="assets/img/bottles/375_serenade.jpg" alt=""/>
                <p style="text-align: center;">375ml Serenade</p>
            </td>

        </tr>
    </table>
    
    </div>

    <?php include('includes/footer.php'); ?>

</body>
</html>
